webpackJsonp([2,13],{

/***/ 782:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__team_component__ = __webpack_require__(801);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__prismic_prismic_service__ = __webpack_require__(137);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamModule", function() { return TeamModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: "",
        component: __WEBPACK_IMPORTED_MODULE_3__team_component__["a" /* TeamComponent */]
    }
];
var TeamModule = (function () {
    function TeamModule() {
    }
    return TeamModule;
}());
TeamModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* RouterModule */].forChild(routes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__team_component__["a" /* TeamComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_4__prismic_prismic_service__["a" /* PrismicService */]],
        exports: [__WEBPACK_IMPORTED_MODULE_3__team_component__["a" /* TeamComponent */]]
    })
], TeamModule);

//# sourceMappingURL=team.module.js.map

/***/ }),

/***/ 784:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, "/* Efficiency */\n.efficiency{\n    background-position: right 50px;\n    background-repeat: no-repeat;\n\tbackground-size: 40%;\n    background-color: #0c3445;\n        padding-top: 50px;\n    padding-bottom: 50px;\n}\n.efficiencyText{\n    font-size: 30px;\n    color: white;\n\tfont-weight: bold;\n\tline-height: 1.2;\n}\n.efficiencyStatement{\n\tfont-size: 18px;\n  \tfont-weight: 500;\n\tcolor: #4a4a4a;\n\tline-height: 1.5;\n}\n.sectionTop-efficiency{\n    margin-top:70px;\n}\n.bottomMargin{\n    margin-bottom: 100px;\n}\n.btn-custom {\n\tborder-radius: 30px;\n\tbackground-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n\tcolor: white;\n\ttext-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n\n.btn-custom:active, .btn-custom:focus {\n    outline: none;\n}\n/* Efficiency CSS */\n@media screen and (max-width: 767px){\n\t.efficiency{\n\t\tbackground-position: 50px 20px;\n\t\tbackground-size: 100%;\n\t}\n\t.efficiencyText{\n        font-size: 18px;\n\t\tpadding: 0 15px;\n\t\tmargin-top: 50px;\n    }\n    .efficiencyStatement{\n\t\tfont-size: 14px;\n\t\tmargin-top: 50px;\n        padding: 0 15px;\n\t}\n\t.bottomMargin{\n\t\tmargin-bottom: 50px;\n\t}\n\t.sectionTop-efficiency{\n\t\tmargin-top:50px;\n\t}\n\t.efficiency .btn-custom {\n\t\tfont-size: 14px;\n\t}\n}\n", ""]);

// exports


/***/ }),

/***/ 801:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_dom__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismic_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_wow__ = __webpack_require__(302);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeamComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var TeamComponent = (function () {
    function TeamComponent(prismic, route, titleService, _headerService, meta, wowService, sanitizer) {
        this.prismic = prismic;
        this.route = route;
        this.titleService = titleService;
        this._headerService = _headerService;
        this.meta = meta;
        this.wowService = wowService;
        this.sanitizer = sanitizer;
        this.PrismicDOM = __WEBPACK_IMPORTED_MODULE_5_prismic_dom___default.a;
        this.toolbar = false;
        this.contactUrl = "";
        this.wowService.init();
    }
    TeamComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeStream = __WEBPACK_IMPORTED_MODULE_4_rxjs__["Observable"].fromPromise(this.prismic.buildContext())
            .subscribe(function (ctx) {
            _this.ctx = ctx;
            _this.fetchPage();
        });
        this.subscription = this._headerService.currentSection.subscribe(function (info) { return _this.sectionInfo = info; });
    };
    TeamComponent.prototype.ngOnDestroy = function () {
        this.routeStream.unsubscribe();
        this.subscription.unsubscribe();
    };
    TeamComponent.prototype.ngAfterViewChecked = function () {
        if (this.aboutUs && this.aboutUs.nativeElement && (this.sectionInfo.section !== '')) {
            this.scrollToSection(this.sectionInfo);
            this.sectionInfo = { section: '' };
        }
        else if (this.ctx && !this.toolbar) {
            // this.prismic.toolbar(this.ctx.api);
            this.toolbar = true;
            window.scrollTo(0, 0);
        }
    };
    TeamComponent.prototype.scrollOffsetForFixedHeader = function () {
        var scrolledY = window.scrollY;
        if (scrolledY) {
            window.scroll(0, scrolledY - 160);
        }
    };
    TeamComponent.prototype.scrollToSection = function (obj) {
        var _this = this;
        setImmediate(function () {
            if (obj.section === 'aboutus') {
                _this.aboutUs.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'milestones') {
                _this.milestones.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'culture') {
                _this.culture.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'leadership') {
                _this.leadership.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'meet_team') {
                _this.meetTeam.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'joinus') {
                _this.joinus.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'contact') {
                _this.contactus.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            _this.scrollOffsetForFixedHeader();
        });
    };
    TeamComponent.prototype.fetchPage = function () {
        var _this = this;
        this.ctx.api.query([
            __WEBPACK_IMPORTED_MODULE_6_prismic_javascript___default.a.Predicates.any('document.type', ['leadership', 'milestones', 'team', 'aboutus', 'culture', 'joinus', 'contactus', 'efficiency', 'teambanner']),
        ])
            .then(function (response) {
            _this.toolbar = false;
            _this.pageContent = response.results;
            var metaDescription = "";
            var metaKeyWords = [];
            var that = _this;
            _this.pageContent.map(function (prop) {
                if (prop.uid === 'leadership') {
                    that.leadershipData = prop.data;
                    metaDescription = that.leadershipData['meta-description'];
                    metaKeyWords.push(that.leadershipData['meta-title']);
                }
                else if (prop.uid === 'aboutus') {
                    that.aboutData = prop.data;
                    metaKeyWords.push(that.aboutData['meta-title']);
                }
                else if (prop.uid === 'milestones') {
                    that.milestoneData = prop.data;
                    metaKeyWords.push(that.milestoneData['meta-title']);
                }
                else if (prop.uid === 'efficiency') {
                    that.efficiencyData = prop.data;
                    metaKeyWords.push(that.efficiencyData['meta-title']);
                }
                else if (prop.uid === 'culture') {
                    that.cultureData = prop.data;
                    metaKeyWords.push(that.cultureData['meta-title']);
                }
                else if (prop.uid === 'team') {
                    that.teamData = prop.data;
                    metaKeyWords.push(that.teamData['meta-title']);
                }
                else if (prop.uid === 'joinus') {
                    that.joinUsData = prop.data;
                    metaKeyWords.push(that.joinUsData['meta-title']);
                }
                else if (prop.uid === 'teambanner') {
                    that.bannerData = prop.data;
                    metaKeyWords.push(that.bannerData['meta-title']);
                }
                else if (prop.uid === 'contactus') {
                    that.contactUsData = prop.data;
                    if (that.contactUsData.locations[0].embedurl.length === 1) {
                        that.contactUrl = that.sanitizer.bypassSecurityTrustResourceUrl(_this.contactUsData.locations[0].embedurl[0].text);
                    }
                    metaKeyWords.push(that.contactUsData['meta-title']);
                }
            });
            _this.meta.updateTag({ name: 'description', content: metaDescription });
            _this.meta.updateTag({ name: 'author', content: 'Xpms' });
            _this.meta.updateTag({ name: 'keywords', content: metaKeyWords.toString() });
        })
            .catch(function (e) { return console.log(e); });
    };
    return TeamComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('aboutUs'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], TeamComponent.prototype, "aboutUs", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('milestones'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], TeamComponent.prototype, "milestones", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('culture'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object)
], TeamComponent.prototype, "culture", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('leadership'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _d || Object)
], TeamComponent.prototype, "leadership", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('meetTeam'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _e || Object)
], TeamComponent.prototype, "meetTeam", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('joinus'),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _f || Object)
], TeamComponent.prototype, "joinus", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('contactus'),
    __metadata("design:type", typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _g || Object)
], TeamComponent.prototype, "contactus", void 0);
TeamComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-team',
        template: __webpack_require__(818),
        styles: [__webpack_require__(810)]
    }),
    __metadata("design:paramtypes", [typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_2__prismic_prismic_service__["a" /* PrismicService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__prismic_prismic_service__["a" /* PrismicService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* ActivatedRoute */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* Title */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* Meta */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* Meta */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["e" /* DomSanitizer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["e" /* DomSanitizer */]) === "function" && _p || Object])
], TeamComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
//# sourceMappingURL=team.component.js.map

/***/ }),

/***/ 810:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports
exports.i(__webpack_require__(784), "");

// module
exports.push([module.i, "/*dwcwdcew*/\n.contentMargin{\n  margin-top:100px;\n}\n.team-banner{\n  background-image: url(" + __webpack_require__(822) + ");\n  background-size: 100% 100%;\n  background-repeat: no-repeat;\n  height: 45vh;\n;\n}\n.banner-inner{\n  text-align: center;\n}\n.banner-inner h2{\n  font-size: 50px;\n  font-weight: bold;\n  font-style: normal;\n  color: #ffffff;\n  margin-left: 50px;\n  text-align: left;\n  margin-top: 0;\n  margin-bottom: 0;\n}\n.solutionHeading{\n  font-weight: 500;\n}\n.testimonialTop{\n\tmargin-top:30px;\n}\nh1.testimonialTop{\n\tmargin-top: 40px;\n}\n.customLine{\n\tborder-top: 6px solid #f7d761;\n\tmargin-top: 0px;\n}\n.card {\n  transition: 0.3s;\n  width: 70%;\n  margin-left: 15%;\n\tborder-radius: 5px;\n}\n.btn-enso img, .btn-custom img {\n\tmargin-top: -3px;\n\tmargin-left: 3px;\n\theight: 12px;\n}\n\n.Teamdescription{\n\tbackground-color: #f7d761;\n\tcolor: #4a4a4a;\n\tfont-weight: 500;\n}\n.Teamdescription h5 {\n  margin-bottom: 5px;\n  margin-top: 0;\n}\n.teamDesignation {\n  margin: 0;\n  font-size: 12px;\n}\n.sectionTop2{\n  margin-top:30px;\n  margin-bottom: 40px;\n}\n\n.row-eq-height{\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  -webkit-box-align:center;\n      -ms-flex-align:center;\n          align-items:center;\n}\n\n/* leadership cards */\n.leadership{\n  background-color: #f7d761;\n  padding: 20px 50px;\n  background-size:50% 100%;\n  background-repeat:no-repeat;\n}\n.linkedin-section {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  padding: 12px 0;\n  background: #007bb5;\n}\n.leardership-name-section {\n  /* display: flex; */\n  padding: 3px 0 0 8px;\n}\n\n.material-card {\n  position: relative;\n  height: 0;\n  padding-bottom: calc(100% - 16px);\n  margin-top: 30px;\n}\n.material-card label {\n   position: absolute;\n    top: calc(75% - 10px);\n    right: -16px;\n    width: 95%;\n    color: #fff;\n    margin: 0;\n    z-index: 10;\n    font-size:14px;\n  transition: all 0.3s;\n}\n.material-card label span {\n  display: block;\n}\n.material-card label strong {\n  font-weight: 400;\n  display: block;\n  font-size: .8em;\n}\n.material-card label:before,\n.material-card label:after {\n  content: ' ';\n  position: absolute;\n  right: 0;\n  top: -16px;\n  width: 0;\n  border: 8px solid;\n  transition: all 0.3s;\n}\n.material-card label:after {\n  top: auto;\n  bottom: 0;\n}\n.material-card.mc-active label {\n  top: 0;\n  padding: 10px 16px 10px 90px;\n}\n.material-card.mc-active label:before {\n  top: 0;\n}\n.material-card.mc-active label:after {\n  bottom: -16px;\n}\n.material-card .mc-content {\n  position: absolute;\n  right: 0;\n  top: 0;\n  bottom: 16px;\n  left: 16px;\n  transition: all 0.3s;\n}\n\n.material-card .mc-description {\n  position: absolute;\n  top: 100%;\n  right: 30px;\n  left: 30px;\n  bottom: 54px;\n  overflow: hidden;\n  opacity: 0;\n  filter: alpha(opacity=0);\n  transition: all 1.2s;\n}\n\n.material-card .img-container {\n  overflow: hidden;\n  position: absolute;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 3;\n  transition: all 0.3s;\n}\n.material-card.mc-active .img-container {\n  border-radius: 50%;\n  left: 0;\n  top: 12px;\n  width: 60px;\n  height: 60px;\n  z-index: 20;\n}\n.img-responsive{\n    max-width: 100%;\n}\n.material-card.mc-active .mc-content {\n  padding-top: 5.6em;\n}\n.material-card.mc-active .mc-description {\n  top: 50px;\n  padding-top: 5.6em;\n  opacity: 1;\n  filter: alpha(opacity=100);\n}\n.material-card.Red label {\n    background-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n}\n.material-card.Red label:after {\n  border-top-color: transparent;\n  border-right-color: transparent;\n  border-bottom-color: transparent;\n  border-left-color: transparent;\n\n}\n.material-card.Red label:before {\n  border-top-color: transparent;\n    border-right-color: transparent;\n    border-bottom-color: #af9226;\n    border-left-color: #af9226;\n}\n.material-card.Red.mc-active label:before {\n  border-top-color: transparent;\n    border-right-color: transparent;\n    border-bottom-color: #af9226;\n    border-left-color: #af9226;\n}\n.fulllengthCOntainer{\n  padding-left:-15px;\n  padding-right:-15px;\n}\n\n.milestoneYear{\n  color:#9b9b9b;\n  font-family: 'Helvetica';\n}\n.milestoneDate{\n  color: #9b9b9b;\n  font-family: 'Helvetica';\n  border-bottom:6px solid #f7d761;\n  padding-bottom:10px;\n}\n.milestoneSummary{\n  margin-top:25px;\n}\n.leadership-info {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.linkdinImage{\n          height: 45px;\n    background-size: 60%;\n    background-repeat: no-repeat;\n}\n\n.heading{\n  text-transform:uppercase;\n  color:#4a4a4a;\n}\n.contact-us-wrapper{\n  background-color: rgba(216, 216, 216,0.2)\n}\n.poly{\n  -webkit-clip-path: polygon(38% 0, 100% 0, 100% 100%, 0 100%);\n  clip-path: polygon(38% 0, 100% 0, 100% 100%, 0 100%);\n  background-color: rgba(0,0,0,0.5); \n  height: 100%;\n  \n}\n.poly span{\n  font-size: 20px;\n  font-weight: normal;\n  font-style: normal;\n  color: #ffffff;\n  margin: 0px 10% 0px 30%;\n}\n.h-100{\n  height: 100%;\n}\n.h-50{\n  height: 50%;\n}\n.h-70{\n  height: 70%;\n}\n.h-30{\n  height: 30%;\n}\n.flex-div{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n}\n.flex-div-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.flex-end{\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n.bottom h5{\n  font-size: 24px;\n  margin-left: 50px;\n  font-weight: 300;\n  font-style: normal;\n  color: #ffffff;\n  margin-bottom: 5%;\n}\n.right-bottom{\n  position: relative;\n}\n.right-bottom p{\n  margin-right: 50px;\n  font-size: 14px;\n  font-weight: bold;\n  font-style: normal;\n  color: #ffffff;\n  right: 0;\n  position: absolute;\n}\n.tag-0{\n  bottom: 20%;\n}\n.tag-1{\n  bottom: 0;\n}\n.caseStudyRequest{\n  margin-bottom: 100px;\n}\n.btn-custom {\n  border-radius: 30px;\n  background-color: #4a4a4a;\n  color: white;\n  background-image: none;\n  text-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n.team-blocks {\n  padding: 50px 0 15px 0;\n}\n\n@media screen and (max-width: 600px){\n  .row-eq-height{\n    display : block;\n    -webkit-box-align:top;\n        -ms-flex-align:top;\n            align-items:top;\n  }\n}\n/* culture */\n\n.team-events-content{\n  background-color: #ffffff;\n  padding: 40px 40px 150px 40px;\n}\n.team-events{\n  text-align: left;\n  background-size: cover;\n  background-repeat: no-repeat;\n  width: 100%;\n  /* height: 780px; */\n  padding-top: 10%;\n  padding-bottom: 5%;\n}\n.team-events-content-mobile{\n  background-color: #ffffff;\n  padding: 30px 0;\n}\n.team-events-content-mobile h4{\n  margin: 0 0 20px 0;\n  text-transform: uppercase;\n}\n.team-events-mobile{\n  background-size: cover;\n  background-repeat: no-repeat;\n  width: 100%;\n  height: 350px;\n}\n\n.row.no-gutters {\n  margin-right:0;\n  margin-left:0;\n}\n.row.no-gutters > [class*='col-'] {\n  padding-right:0;\n  padding-left:0;\n}\n\n.row{\n\tmargin-left:0px;\n\tmargin-right:0px;\n}\n.team-award-section img, .team-work-section img{\n  height: 100%;\n  width: 100%;\n}\n.team-video-section {\n  padding: 60px 0px 0px 40px;\n}\n.flex-row{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-color: rgba(216, 216, 216,0.2);\n}\n.culture-inner{\n  margin: 0px 80px 0px 65px;\n}\n.culture-inner h3{\n  font-size: 40px;\n  font-weight: 500;\n  font-style: normal;\n  color: #4a4a4a;\n  margin-bottom: 50px;\n}\n.culture-inner p{\n  font-size: 16px;\n  font-weight: normal;\n  font-style: normal;\n  color: #4a4a4a;\n}\n.join-us-description {\n  padding-left: 10%;\n  padding-right: 10%;\n}\n.join-us-wrapper, .contact-us-wrapper {\n  margin-top: 50px;\n  margin-bottom: 50px;\n}\n.contact-us-wrapper .country {\n  color: #333333;\n  font-weight: bold;\n  font-size: 16px;\n}\n.contact-us-wrapper .city {\n  font-weight: bold;\n  margin-top: 20px;\n  font-size: 16px;\n}\n.contact-us-wrapper .address {\n  margin-top: 10px;\n}\n.maps-section {\n\n}\n@media screen and (min-width: 768px) {\n  .solutionHeading{\n    color: #333333;\n    font-size: 40px;\n  }\n  .Teamdescription{\n    padding: 10px 0px 10px 10px;\n  }\n  .team-award-section, .team-work-section {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n  }\n  .team-awards-content{\n    margin: auto;\n    padding: 40px 10px 40px 80px !important;\n  }\n  .team-works-content{\n    margin: auto;\n    padding: 40px 80px 40px 0 !important;\n  }\n  .contact-title {\n    margin-top: 0;\n  }\n  .contact-info {\n    margin: 30px 0;\n  }\n}\n@media screen and (min-width: 768px) and (max-width: 992px) {\n  .team-awards-content {\n    margin: auto;\n    padding: 20px 10px 20px 15px !important;\n  }\n  .team-works-contentm{\n    margin: auto;\n    padding: 20px 15px 20px 0 !important;\n  }\n}\n@media screen and (min-width: 768px) and (max-width: 1200px) {\n  .solutionHeading, .join-us-title, .milestoneYear, .heading {\n    font-size: 26px;\n  }\n}\n@media screen and (max-width: 767px) {\n  .solutionHeading{\n    text-align: center;\n    font-size: 20px;\n    margin-top: 0;\n    text-transform: uppercase;\n  }\n  .Teamdescription{\n    padding: 15px 0px 15px 10px;\n  }\n  .milestoneYear {\n    font-size: 20px;\n  }\n  .team-award-section, .team-work-section {\n    padding-bottom: 30px;\n  }\n  .team-award-section h4, .team-work-section h4{\n    margin: 30px 0 20px 0;\n    text-transform: uppercase;\n  }\n  .img-about-mobile {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n  }\n  .img-about-mobile img {\n    width: 50%;\n    height: 50%;\n  }\n  .heading-mobile {\n    font-size: 40px;\n    color: #25aae1;\n  }\n  .contentMargin{\n    margin-top: 20px;\n  }\n  .leadership .heading {\n    font-size: 20px;\n    font-weight: bold;\n  }\n\n  .btn-custom {\n    font-size: 14px;\n  }\n  .material-card.mc-active .mc-description {\n    position: relative;\n    top: auto;\n    right: auto;\n    left: auto;\n    padding: 50px 30px 70px 30px;\n    bottom: 0;\n  }\n  .material-card.mc-active .mc-content {\n    position: relative;\n    margin-right: 16px;\n  }\n  .material-card.mc-active {\n    padding-bottom: 0;\n    height: auto;\n  }\n  .join-us-title {\n    font-size: 20px;\n    text-transform: uppercase;\n  }\n  .contact-info {\n    margin: 30px 0;\n  }\n  .maps-section {\n    margin-top: 20px;\n  }\n}\n@media screen and (max-width: 480px) {\n  .Teamdescription h5{\n    font-size: 12px;\n  }\n  .teamDesignation {\n    font-size: 10px;\n  }\n}\n@media screen and (max-width: 320px) {\n  .Teamdescription {\n    min-height: 100px;\n  }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 818:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!pageContent\" class=\"container content-spinner\">\n\t<div class=\"col-md-12 loader\">\n\t\t<img class=\"\" src=\"../../../assets/images/content_spinner.gif\"/>\n\t</div>\n</div>\n<div *ngIf=\"pageContent\" [attr.data-wio-id]=\"pageContent.id\">\n    <div class=\"\">\n        <section class=\"team-banner\" [ngStyle]=\"{'background-image': 'url(' + bannerData.backgroundimage.url + ')'}\" #aboutUs>\n            <div class=\"container-fluid no-padding h-100\" style=\"margin-top: 65px;\" >\n                <div class=\"row banner-inner h-100\">\n                    <div class=\"col-lg-6 col-md-6 col-sm-6 h-100 \">\n                        <div class=\"flex-div-center h-70\">\n                            <h2>{{bannerData.leftheading[0].text}}</h2>\n                        </div>\n                        <div class=\"bottom flex-div h-30\">\n                            <h5>{{bannerData.lefttagline[0].text}}</h5>\n                        </div>\n                    </div>\n                    <div class=\"col-lg-6 col-md-6 col-sm-6 no-padding h-100\">\n                        <div class=\"poly\">\n                            <div class=\"flex-div h-70\">\n                                <span>\n                                        {{bannerData.righttext[0].text}}\n                                </span>\n                            </div>\n                            <div class=\"right-bottom h-30 flex-div flex-end\">\n                                <p class=\"tag-{{j}}\" *ngFor = \"let i of bannerData.righttagline;let j =index\">{{i.text}} </p>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </section>\n        <section class=\"caseStudyRequest sectionTop2\" >\n            <div class=\"row\">\n                <div class=\"col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-12 no-padding wow fadeInLeft\">\n                    <div class=\"row\" >\n                        <div class=\"col-md-10 col-md-offset-1\">\n                            <!-- <h1 class=\"solutionHeading hidden-xs\">{{aboutData.emtitle[0].text}}</h1> -->\n                            <div *ngFor=\"let item of aboutData.emdata\">\n                                <p class=\"testimonialTop\">{{item.text}}</p>\n                            </div>\n                        </div>\n                        <div class=\"col-md-10 col-md-offset-1 team-video-section\">\n                            <img width=\"100%\" src=\"../../../assets/images/team_video.png\"/>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </section>\n        <div class=\"testimonialTop\">\n           <!--  <hr class=\"customLine \" />\n            <section class=\"milestones sectionTop2\" #milestones>\n                <div class=\"row\">\n                    <div class=\"col-lg-10 col-sm-12 col-xs-12 col-md-10 no-padding\">\n                        <h1 class=\"solutionHeading\">{{milestoneData.title[0].text}}</h1>\n                        <h1 class=\"milestoneYear\">{{milestoneData.milestonetitle[0].text}}</h1>\n                        <div [ngClass]=\"k !== 0 ? 'testimonialTop' : ''\" class=\"\" *ngFor=\"let ms of milestoneData.milestonedata; let k = index;\">\n                            <span class=\"milestoneDate\">{{ms.milestonedate[0].text}}</span>\n                            <br>\n                            <label class=\"milestoneSummary\">{{ms.milestonesummary[0].text}}</label>\n                        </div>\n                    </div>\n                </div>\n            </section>\n            <hr class=\"customLine testimonialTop\" /> -->\n           <!--  <section class=\"culture sectionTop2 \" #culture>\n                <div class=\"testimonialTop\">\n                    <h1 class=\"solutionHeading testimonialTop\">{{cultureData.title[0].text}}</h1>\n                    <div class=\"row no-gutters testimonialTop\">\n                        <div class=\"col-lg-12 col-md-12 col-sm-12 hidden-xs team-events wow fadeIn\" [ngStyle]=\"{'background-image': 'url(' + cultureData.culturedata[0].events.url + ')'}\">\n                            <img src={{cultureData.culturedata[0].events.url}} class=\"img-responsive\" style=\"height:780px!important\">\n                            <div class=\"row\" class=\"wow fadeIn\" data-wow-delay=\"0.2s\">\n                                <div class=\"col-lg-5 col-md-5 col-sm-5 col-xs-12 col-lg-offset-6 col-md-offset-6 col-sm-offset-6 team-events-content \">\n                                    <div>\n                                        <h4>{{cultureData.culturedata[0].title[0].text}}</h4>\n                                        <span>{{cultureData.culturedata[0].summary[0].text}}</span>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-xs-12 hidden-sm hidden-md hidden-lg team-events-mobile\" [ngStyle]=\"{'background-image': 'url(' + cultureData.culturedata[0].events.url + ')'}\">\n                        </div>\n                        <div class=\"col-xs-12 hidden-sm hidden-md hidden-lg team-events-content-mobile\">\n                            <div class=\"\">\n                                <h4>{{cultureData.culturedata[0].title[0].text}}</h4>\n                                <span>{{cultureData.culturedata[0].summary[0].text}}</span>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row no-gutters team-award-section\">\n                        <div class=\"col-md-6 col-lg-6 col-sm-6 col-xs-12 wow fadeInRight\">\n                            <img alt={{cultureData.culturedata[1].events.alt}} src={{cultureData.culturedata[1].events.url}} class=\"img-responsive\">\n                        </div>\n                        <div class=\"col-md-6 col-lg-6 col-sm-6 col-xs-12 team-awards-content wow fadeInLeft\">\n                            <h4>{{cultureData.culturedata[1].title[0].text}}</h4>\n                            <span>{{cultureData.culturedata[1].summary[0].text}}</span>\n                        </div>\n                    </div>\n                    <div class=\"row no-gutters team-work-section\">\n                        <div class=\"col-xs-12 hidden-md hidden-sm hidden-lg  wow fadeInRight\">\n                            <img alt={{cultureData.culturedata[2].events.alt}} src={{cultureData.culturedata[2].events.url}} class=\"img-responsive\">\n                        </div>\n                        <div class=\"col-md-6 col-lg-6 col-sm-6 col-xs-12 team-works-content  wow fadeInRight\">\n                            <h4>{{cultureData.culturedata[2].title[0].text}}</h4>\n                            <span>{{cultureData.culturedata[2].summary[0].text}}</span>\n                        </div>\n                        <div class=\"col-md-6 col-lg-6 col-sm-6 hidden-xs  wow fadeInLeft\">\n                            <img alt={{cultureData.culturedata[2].events.alt}} src={{cultureData.culturedata[2].events.url}} class=\"img-responsive\">\n                        </div>\n                    </div>\n                </div>\n            </section> -->\n            <section class=\"culture\" #culture>\n                <div class=\"row flex-row\">\n                    <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding\">\n                        <img src=\"{{cultureData.culturedata[0].events.url}}\" alt=\"\" class=\"img-responsive\">\n                    </div>\n                    <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding\">\n                        <div class=\"culture-inner\">\n                            <h3>{{cultureData.culturedata[0].title[0].text}}</h3>\n                            <p *ngFor = \"let i of cultureData.culturedata[0].summary\">{{i.text}}</p>\n                        </div>\n                    </div>\n                </div>\n            </section>\n        </div>\n\n        <section class=\"Leadership\" #leadership>\n            <div class=\"leadership\" [ngStyle]=\"{'background-image': 'url(' + leadershipData.leadershipbg.url + ')'}\">\n                <div class=\"row row-eq-height\">\n                    <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12 background\">\n                        <h1 class=\"text-center heading\">{{leadershipData.title[0].text}}</h1>\n                        <p class=\"text-center hidden-sm hidden-xs\">{{leadershipData.subtitle[0].text}}</p>\n                    </div>\n                    <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12\">\n                        <div class=\"row\">\n                            <div class=\"col-lg-4 col-sm-6 col-xs-12\" *ngFor=\"let item of leadershipData.leaders\">\n                                <article class=\"material-card Red\">\n                                    <label class=\"wow fadeInRight\">\n                                        <div class=\"row leadership-info\">\n                                            <div class=\"col-lg-2 col-sm-2 col-md-2 col-xs-2 linkedin-section\">\n                                                <img alt={{leadershipData.linkedinlogo.alt}} src={{leadershipData.linkedinlogo.url}} style=\"width:16px;height: 16px;\" />\n                                            </div>\n                                            <div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-10 leardership-name-section\">\n                                                <span>{{item.name[0].text}}</span>\n                                                <strong>\n\n                                                    {{item.desgination[0].text}}\n                                                </strong>\n                                            </div>\n                                        </div>\n                                    </label>\n                                    <div class=\"mc-content\">\n                                        <div class=\"img-container\">\n                                            <img class=\"img-responsive wow fadeInRight\" alt={{item.profileimage.alt}} src={{item.profileimage.url}} style=\"width:247px;\">\n                                        </div>\n                                    </div>\n                                </article>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </section>\n       <!--  <section class=\"team sectionTop2 container\" #meetTeam>\n            <h1 class=\"solutionHeading features\">\n                {{teamData.teamtitle[0].text}}\n            </h1>\n            <div class=\"row\">\n                <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-6 no-padding team-blocks\" *ngFor=\"let item of teamData.team\">\n                    <div class=\"card wow fadeInUp\">\n                        <img src={{item.profileimage.url}} alt={{item.profileimage.alt}} style=\"width:100%;\">\n                        <div class=\"Teamdescription\">\n                            <h5 class=\"zeroMargin\">\n                                <b>{{item.name[0].text}}</b>\n                            </h5>\n                            <p class=\"teamDesignation\">{{item.designation[0].text}}</p>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </section> -->           \n        <section class=\"team join-us-wrapper\" #joinus>\n            <div class=\"container\">\n                <h1 class=\"join-us-title text-center\">\n                    {{joinUsData.maintitle[0].text}}\n                </h1>\n                <div class=\"sectionTop2 join-us-description text-center\">\n                    {{joinUsData.description[0].text}}\n                </div>\n                <div class=\"sectionTop2 text-center\">\n                    <button class=\"btn btn-custom\" routerLink=\"/openpositions\" >\n                        {{joinUsData.openpositions[0].text}}\n                        <img src=\"../../../assets/images/right-arrow.png\" />\n                    </button>\n                </div>\n            </div>\n        </section>\n        <section class=\"team contact-us-wrapper\" #contactus>\n            <div class=\"\">\n                <div class=\"row hidden-xs row-eq-height\">\n                    <div class=\"col-md-3 col-md-offset-1 col-sm-offset-1 col-sm-3\">\n                        <h1 class=\"solutionHeading contact-title\">\n                            {{contactUsData.maintitle[0].text}}\n                        </h1>\n                        <div class=\"row\">\n                            <div class=\"col-md-12 col-sm-12 contact-info no-padding\" *ngFor=\"let item of contactUsData.locations\">\n                                <div class=\"country\">\n                                    {{item.country[0].text}}\n                                </div>\n                                <div class=\"city\">\n                                    {{item.city[0].text}}\n                                </div>\n                                <div class=\"address\">\n                                    {{item.address[0].text}}\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-md-7 col-sm-7 col-md-offset-1 col-sm-offset-1 no-padding\">\n                        <div class=\"maps-section\">\n                            <iframe width=\"100%\" height=\"450px\" id=\"gmap_canvas\"\n                                [src]=\"contactUrl\"\n                                frameborder=\"0\"\n                                scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row hidden-md hidden-lg hidden-sm\">\n                    <div class=\"col-xs-12 no-padding\">\n                        <h3 class=\"solutionHeading \">\n                            {{contactUsData.maintitle[0].text}}\n                        </h3>\n                    </div>\n                    <div class=\"col-xs-12 no-padding\">\n                        <div class=\"row\">\n                            <div class=\"col-xs-12 contact-info no-padding\" *ngFor=\"let item of contactUsData.locations\">\n                                <div class=\"country\">\n                                    {{item.country[0].text}}\n                                </div>\n                                <div class=\"city\">\n                                    {{item.city[0].text}}\n                                </div>\n                                <div class=\"address\">\n                                    {{item.address[0].text}}\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-xs-12 no-padding\">\n                        <div class=\"maps-section\" >\n                            <iframe  width=\"100%\" height=\"350px\" id=\"gmap_canvas\"\n                                [src]=\"contactUrl\"\n                                frameborder=\"0\"\n                                scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </section>\n        <section class=\"efficiency\" [ngStyle]=\"{'background-image': 'url(' + efficiencyData.background.url + ')'}\">\n            <div class=\"container\">\n                <div class=\"row wow fadeInUp\">\n                    <div class=\"col-md-8 col-md-offset-2 no-padding text-center \">\n                        <h4 class=\"efficiencyText\">{{efficiencyData.maintitle[0].text}}</h4>\n                        <div class=\"efficiencyStatement sectionTop-efficiency\">{{efficiencyData.subtitle[0].text}}</div>\n                        <button class=\"btn btn-custom bottomMargin sectionTop-efficiency\" routerLink=\"/getstarted\">{{efficiencyData.getstartedlabel[0].text}}</button>\n                    </div>\n                </div>\n            </div>\n        </section>\n    </div>\n</div>\n"

/***/ }),

/***/ 822:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "team-banner.db1b5d6a39fc81d7dc18.png";

/***/ })

});
//# sourceMappingURL=2.chunk.js.map