webpackJsonp([4,13],{

/***/ 778:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__future_component__ = __webpack_require__(797);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(50);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FutureModule", function() { return FutureModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: "",
        component: __WEBPACK_IMPORTED_MODULE_2__future_component__["a" /* FutureComponent */]
    }
];
var FutureModule = (function () {
    function FutureModule() {
    }
    return FutureModule;
}());
FutureModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* RouterModule */].forChild(routes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__future_component__["a" /* FutureComponent */]]
    })
], FutureModule);

//# sourceMappingURL=future.module.js.map

/***/ }),

/***/ 784:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, "/* Efficiency */\n.efficiency{\n    background-position: right 50px;\n    background-repeat: no-repeat;\n\tbackground-size: 40%;\n    background-color: #0c3445;\n        padding-top: 50px;\n    padding-bottom: 50px;\n}\n.efficiencyText{\n    font-size: 30px;\n    color: white;\n\tfont-weight: bold;\n\tline-height: 1.2;\n}\n.efficiencyStatement{\n\tfont-size: 18px;\n  \tfont-weight: 500;\n\tcolor: #4a4a4a;\n\tline-height: 1.5;\n}\n.sectionTop-efficiency{\n    margin-top:70px;\n}\n.bottomMargin{\n    margin-bottom: 100px;\n}\n.btn-custom {\n\tborder-radius: 30px;\n\tbackground-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n\tcolor: white;\n\ttext-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n\n.btn-custom:active, .btn-custom:focus {\n    outline: none;\n}\n/* Efficiency CSS */\n@media screen and (max-width: 767px){\n\t.efficiency{\n\t\tbackground-position: 50px 20px;\n\t\tbackground-size: 100%;\n\t}\n\t.efficiencyText{\n        font-size: 18px;\n\t\tpadding: 0 15px;\n\t\tmargin-top: 50px;\n    }\n    .efficiencyStatement{\n\t\tfont-size: 14px;\n\t\tmargin-top: 50px;\n        padding: 0 15px;\n\t}\n\t.bottomMargin{\n\t\tmargin-bottom: 50px;\n\t}\n\t.sectionTop-efficiency{\n\t\tmargin-top:50px;\n\t}\n\t.efficiency .btn-custom {\n\t\tfont-size: 14px;\n\t}\n}\n", ""]);

// exports


/***/ }),

/***/ 787:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, ".main-title {\n    color: #25aae1;\n    font-weight: 600;\n    font-size: 40px;\n}\n.main-title-margin {\n    margin-top: 100px;\n}\n.sectionBorder6{\n\tborder-top: 6px solid #f7d761;\n\tmargin-top: 0px;\n}\n.margintop50 {\n    margin-top: 50px;\n}\n.marginbottom50 {\n    margin-bottom: 50px;\n}\n.margintop30 {\n    margin-top: 30px;\n}\n.marginbottom30 {\n    margin-bottom: 30px;\n}\n@media screen and (max-width: 767px) {\n    .main-title {\n        font-size: 20px;\n        text-transform: uppercase;\n    }\n    .main-title-margin {\n        margin-top: 50px;\n    }\n}", ""]);

// exports


/***/ }),

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prismic_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_wow__ = __webpack_require__(302);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FutureComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var FutureComponent = (function () {
    function FutureComponent(prismic, route, titleService, _headerService, meta, wowService) {
        this.prismic = prismic;
        this.route = route;
        this.titleService = titleService;
        this._headerService = _headerService;
        this.meta = meta;
        this.wowService = wowService;
        this.PrismicDOM = __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default.a;
        this.toolbar = false;
        this.wowService.init();
    }
    FutureComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeStream = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].fromPromise(this.prismic.buildContext())
            .subscribe(function (ctx) {
            _this.ctx = ctx;
            _this.fetchPage();
        });
        this.subscription = this._headerService.currentSection.subscribe(function (info) { return _this.sectionInfo = info; });
    };
    FutureComponent.prototype.ngOnDestroy = function () {
        this.routeStream.unsubscribe();
        this.subscription.unsubscribe();
    };
    FutureComponent.prototype.ngAfterViewChecked = function () {
        if (this.blogSection && this.blogSection.nativeElement && (this.sectionInfo.section !== '')) {
            this.scrollToSection(this.sectionInfo);
            this.sectionInfo = { section: '' };
        }
        else if (this.ctx && !this.toolbar) {
            // this.prismic.toolbar(this.ctx.api);
            this.toolbar = true;
            window.scrollTo(0, 0);
        }
    };
    FutureComponent.prototype.scrollOffsetForFixedHeader = function () {
        var scrolledY = window.scrollY;
        if (scrolledY) {
            window.scroll(0, scrolledY - 160);
        }
    };
    FutureComponent.prototype.scrollToSection = function (obj) {
        var _this = this;
        setImmediate(function () {
            if (obj.section === 'blog') {
                _this.blogSection.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'lab') {
                _this.labSection.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            _this.scrollOffsetForFixedHeader();
        });
    };
    FutureComponent.prototype.fetchPage = function () {
        var _this = this;
        this.ctx.api.query([
            __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default.a.Predicates.any('document.type', ['blog', 'lab', 'efficiency']),
        ])
            .then(function (response) {
            _this.toolbar = false;
            _this.pageContent = response.results;
            var metaDescription = "";
            var metaKeyWords = [];
            _this.pageContent.map(function (prop) {
                if (prop.uid === 'blog') {
                    _this.blogData = prop.data;
                    metaDescription = _this.blogData['meta-description'];
                    metaKeyWords.push(_this.blogData['meta-title']);
                }
                else if (prop.uid === 'inlab') {
                    _this.labData = prop.data;
                    metaKeyWords.push(_this.labData['meta-title']);
                }
                else if (prop.uid === 'efficiency') {
                    _this.efficiencyData = prop.data;
                    metaKeyWords.push(_this.efficiencyData['meta-title']);
                }
            });
            _this.meta.updateTag({ name: 'description', content: metaDescription });
            _this.meta.updateTag({ name: 'author', content: 'Xpms' });
            _this.meta.updateTag({ name: 'keywords', content: metaKeyWords.toString() });
        })
            .catch(function (e) { return console.log(e); });
    };
    return FutureComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('blogSection'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], FutureComponent.prototype, "blogSection", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('labSection'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], FutureComponent.prototype, "labSection", void 0);
FutureComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-future',
        template: __webpack_require__(814),
        styles: [__webpack_require__(806)]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */]) === "function" && _h || Object])
], FutureComponent);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=future.component.js.map

/***/ }),

/***/ 806:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports
exports.i(__webpack_require__(784), "");
exports.i(__webpack_require__(787), "");

// module
exports.push([module.i, ".sectionTop2{\n  margin-top:70px;\n}\n.btn-custom {\n  border-radius: 30px;\n  background-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n  color: white;\n  text-transform: uppercase;\n}\n.contentMargin {\n  margin-top:100px;\n}\n\n.sectionHeading {\n  color: #25aae1;\n  font-weight: 600;\n  font-size: 40px;\n}\n.sectionHeading-future {\n  color: #25aae1;\n  font-weight: 600;\n  padding-left: 30px;\n}\n\n.customLine{\n  border-top: 6px solid #f7d761;\n\tmargin-bottom: 0px;\n}\n.info-section {\n  padding-top: 20px;\n  font-size: 12px;  \n  font-family: 'Helvetica';\n}\n.info-section .divider {\n  margin: 0 10px;\n}\n.blog-title {\n  margin: 20px 0;\n  font-weight: bold;\n}\n.article-content {\n  padding: 20px 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: left;\n      -ms-flex-pack: left;\n          justify-content: left;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n}\n.article-content .pdf-icon {\n  margin-right: 10px;\n  width: 15px;\n  margin-top: 3px;\n  color: #25aae1;\n}\n.sectionHeading-mobile {\n  font-size: 20px;\n  text-transform: uppercase;\n  font-weight: bold;\n}\n@media screen and (min-width: 768px) {\n  .blog-content-section, .in-lab-section {\n    padding: 15px 0px 15px 50px;\n  }\n  .blog-cards, .lab-cards {\n    padding-right: 45px;\n    padding-left: 0;\n  }\n  .blog-title {\n    font-size: 22px;\n  }\n}\n@media screen and (max-width: 767px) {\n  .blog-content-section {\n    padding: 15px 0;\n  }\n  .blog-title {\n    font-size: 18px;    \n  }\n  .img-future-mobile {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    padding-bottom: 20px;\n  }\n  .img-future-mobile img {\n    width: 50%;\n    height: 50%;\n  }\n  .contentMargin {\n    margin-top: 50px;\n  }\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 814:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!pageContent\" class=\"container content-spinner\">\n\t<div class=\"col-md-12 loader\">\n\t\t<img class=\"\" src=\"../../../assets/images/content_spinner.gif\"/>\n\t</div>\n</div>\n<div *ngIf=\"pageContent\" [attr.data-wio-id]=\"pageContent.id\">\n    <div class=\"container-fluid\">\n        <div class=\"row main-title-margin fadeIn\" #blogSection>\n            <div class=\"col-md-4 col-sm-4 col-xs-12 blog-image-section no-padding wow fadeInLeft\">\n                <img alt={{blogData.blogbg.alt}} src={{blogData.blogbg.url}} class=\"img-responsive hidden-xs\"/>\n                <div class=\"img-future-mobile hidden-md hidden-sm hidden-lg\">\n                    <img alt={{blogData.blogbg.alt}} src={{blogData.blogbg.url}} class=\"img-responsive\"/>\n                    <h2 class=\"sectionHeading-future\">{{blogData.futurelabel[0].text}}</h2>\n                </div>\n            </div>\n            <div class=\"col-md-8 col-sm-8 col-xs-12 blog-content-section wow fadeInRight\">\n                <h1 class=\"sectionHeading col-md-12 no-padding hidden-xs\">{{blogData.maintitle[0].text}}</h1>\n                <h3 class=\"col-xs-12 sectionHeading-mobile hidden-md hidden-sm hidden-lg text-center\">{{blogData.maintitle[0].text}}</h3>\n                <div class=\"col-md-6 col-xs-12 blog-cards\" *ngFor=\"let item of blogData.blogdata\">\n                    <hr class=\"col-md-12 customLine no-padding\" />\n                    <div class=\" col-md-12 info-section no-padding\">\n                        <span>{{item.dateposted[0].text}}</span>\n                        <span class=\"divider\">&#47;</span>\n                        <span>{{item.topic[0].text}}</span>\n                        <span class=\"divider\">&#47;</span>\n                        <span>{{item.author[0].text}}</span>\n                    </div>\n                    <div class=\"col-md-12 no-padding\">\n                        <div>\n                            <p class=\"blog-title\">{{item.blogtitle[0].text}}</p>\n                        </div>\n                        <div>\n                            <span>{{item.description[0].text}}</span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row main-title-margin\" #labSection>\n            <div class=\"col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12 in-lab-section\">\n                <div class=\"wow fadeInUp\">\n                    <h1 class=\"sectionHeading col-md-12 no-padding hidden-xs\">{{labData.maintitle[0].text}}</h1>\n                    <h3 class=\"col-xs-12 sectionHeading-mobile hidden-md hidden-sm hidden-lg text-center\">{{labData.maintitle[0].text}}</h3>\n                    <div class=\"col-md-12 col-xs-12 lab-cards\" *ngFor=\"let item of labData.articles\">\n                        <div class=\"article-content\">\n                            <img class=\"pdf-icon\" src=\"../../../assets/images/icons-pdf.png\"/>\n                            <span class=\"article-text\">{{item.articleinfo[0].text}}</span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row efficiency\" #efficiency [ngStyle]=\"{'background-image': 'url(' + efficiencyData.background.url + ')'}\">\n            <div class=\"col-md-8 col-md-offset-2 text-center wow fadeInUp\">\n                <h4 class=\"efficiencyText\">{{efficiencyData.maintitle[0].text}}</h4>\n                <div class=\"efficiencyStatement sectionTop-efficiency\">{{efficiencyData.subtitle[0].text}}</div>\n                <button class=\"btn btn-custom bottomMargin sectionTop-efficiency \" routerLink=\"/getstarted\">{{efficiencyData.getstartedlabel[0].text}}</button>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ })

});
//# sourceMappingURL=4.chunk.js.map