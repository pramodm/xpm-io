webpackJsonp([5,13],{

/***/ 783:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__work_component__ = __webpack_require__(802);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__prismic_prismic_service__ = __webpack_require__(137);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkModule", function() { return WorkModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: "",
        component: __WEBPACK_IMPORTED_MODULE_3__work_component__["a" /* WorkComponent */]
    }
];
var WorkModule = (function () {
    function WorkModule() {
    }
    return WorkModule;
}());
WorkModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forChild(routes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__work_component__["a" /* WorkComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_4__prismic_prismic_service__["a" /* PrismicService */]]
    })
], WorkModule);

//# sourceMappingURL=work.module.js.map

/***/ }),

/***/ 784:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, "/* Efficiency */\n.efficiency{\n    background-position: right 50px;\n    background-repeat: no-repeat;\n\tbackground-size: 40%;\n    background-color: #0c3445;\n        padding-top: 50px;\n    padding-bottom: 50px;\n}\n.efficiencyText{\n    font-size: 30px;\n    color: white;\n\tfont-weight: bold;\n\tline-height: 1.2;\n}\n.efficiencyStatement{\n\tfont-size: 18px;\n  \tfont-weight: 500;\n\tcolor: #4a4a4a;\n\tline-height: 1.5;\n}\n.sectionTop-efficiency{\n    margin-top:70px;\n}\n.bottomMargin{\n    margin-bottom: 100px;\n}\n.btn-custom {\n\tborder-radius: 30px;\n\tbackground-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n\tcolor: white;\n\ttext-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n\n.btn-custom:active, .btn-custom:focus {\n    outline: none;\n}\n/* Efficiency CSS */\n@media screen and (max-width: 767px){\n\t.efficiency{\n\t\tbackground-position: 50px 20px;\n\t\tbackground-size: 100%;\n\t}\n\t.efficiencyText{\n        font-size: 18px;\n\t\tpadding: 0 15px;\n\t\tmargin-top: 50px;\n    }\n    .efficiencyStatement{\n\t\tfont-size: 14px;\n\t\tmargin-top: 50px;\n        padding: 0 15px;\n\t}\n\t.bottomMargin{\n\t\tmargin-bottom: 50px;\n\t}\n\t.sectionTop-efficiency{\n\t\tmargin-top:50px;\n\t}\n\t.efficiency .btn-custom {\n\t\tfont-size: 14px;\n\t}\n}\n", ""]);

// exports


/***/ }),

/***/ 802:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prismic_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_wow__ = __webpack_require__(302);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorkComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var WorkComponent = (function () {
    function WorkComponent(prismic, route, titleService, _headerService, meta, wowService) {
        this.prismic = prismic;
        this.route = route;
        this.titleService = titleService;
        this._headerService = _headerService;
        this.meta = meta;
        this.wowService = wowService;
        this.PrismicDOM = __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default.a;
        this.toolbar = false;
        this.tabEventAttached = false;
        this.wowService.init();
    }
    WorkComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeStream = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].fromPromise(this.prismic.buildContext())
            .subscribe(function (ctx) {
            _this.ctx = ctx;
            _this.fetchPage();
        });
        this.subscription = this._headerService.currentSection.subscribe(function (info) { return _this.sectionInfo = info; });
    };
    WorkComponent.prototype.ngOnDestroy = function () {
        this.routeStream.unsubscribe();
        this.subscription.unsubscribe();
    };
    WorkComponent.prototype.ngAfterViewChecked = function () {
        if (this.caseStudies && this.caseStudies.nativeElement && (this.sectionInfo.section !== '')) {
            this.scrollToSection(this.sectionInfo);
            this.sectionInfo = { section: '' };
        }
        else if (this.ctx && !this.toolbar) {
            // this.prismic.toolbar(this.ctx.api);
            this.toolbar = true;
            window.scrollTo(0, 0);
        }
        if (this.caseStudies && !this.tabEventAttached) {
            this.tabEventAttached = true;
            $(document).ready(function () {
                $('#case-study-select').on('change', function (e) {
                    $('#case-study-tab li a').eq($(this).val()).tab('show');
                });
            });
        }
    };
    WorkComponent.prototype.scrollOffsetForFixedHeader = function () {
        var scrolledY = window.scrollY;
        if (scrolledY) {
            window.scroll(0, scrolledY - 160);
        }
    };
    WorkComponent.prototype.scrollToSection = function (obj) {
        var _this = this;
        setImmediate(function () {
            if (obj.section === 'casestudies') {
                _this.caseStudies.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'testimonials') {
                _this.testimonials.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'customers') {
                _this.customers.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            _this.scrollOffsetForFixedHeader();
        });
    };
    WorkComponent.prototype.fetchPage = function () {
        var _this = this;
        this.ctx.api.query([
            __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default.a.Predicates.any('document.type', ['download', 'workcasestudies', 'wptestimonial', 'efficiency']),
        ])
            .then(function (response) {
            _this.toolbar = false;
            _this.pageContent = response.results;
            var metaDescription = "";
            var metaKeyWords = [];
            _this.pageContent.map(function (prop) {
                if (prop.uid === 'workcasestudies') {
                    _this.casestudiesData = prop.data;
                    metaDescription = _this.casestudiesData['meta-description'];
                    metaKeyWords.push(_this.casestudiesData['meta-title']);
                }
                else if (prop.uid === 'wptestimonial') {
                    _this.testimonialData = prop.data;
                    metaKeyWords.push(_this.testimonialData['meta-title']);
                }
                else if (prop.uid === 'workautomobile') {
                    _this.automobileCaseStudyData = prop.data;
                }
                else if (prop.uid === 'worktechnology') {
                    _this.technologyCaseStudyData = prop.data;
                }
                else if (prop.uid === 'workhealthcare') {
                    _this.healthcareCasestudyData = prop.data;
                }
                else if (prop.uid === 'efficiency') {
                    _this.efficiencyData = prop.data;
                    metaKeyWords.push(_this.efficiencyData['meta-title']);
                }
            });
            _this.meta.updateTag({ name: 'description', content: metaDescription });
            _this.meta.updateTag({ name: 'author', content: 'Xpms' });
            _this.meta.updateTag({ name: 'keywords', content: metaKeyWords.toString() });
        })
            .catch(function (e) { return console.log(e); });
    };
    return WorkComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('caseStudies'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], WorkComponent.prototype, "caseStudies", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('testimonials'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], WorkComponent.prototype, "testimonials", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('customers'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object)
], WorkComponent.prototype, "customers", void 0);
WorkComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-work',
        template: __webpack_require__(819),
        styles: [__webpack_require__(811)]
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */]) === "function" && _j || Object])
], WorkComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j;
//# sourceMappingURL=work.component.js.map

/***/ }),

/***/ 811:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports
exports.i(__webpack_require__(784), "");

// module
exports.push([module.i, ".nav-customTabs{\n\tborder-bottom: none;\n\ttext-transform: uppercase;\n}\n.nav-customTabs>li.active>a, .nav-customTabs>li.active>a:focus, .nav-customTabs>li.active>a:hover{\n\tborder: none;\n    border-bottom: 5px solid #f7d761;\n    color: #25aae1;\n    font-weight: bold;\n}\n.nav-customTabs>li>a {\n    margin-right: 2px;\n    line-height: 1.42857143;\n    border: 1px solid transparent;\n    border-radius: 4px 4px 0 0;\n    color: #4a4a4a;\n}\n.nav-customTabs>li>a:hover {\n    border-color: #eee #eee #ddd;\n    background-color: transparent;\n    border: none;\n    color: #32b3e4;\n}\n.leftZero{\n\tpadding-left:0px;\n}\n\n.customMargin{\n    margin-top: 30px;\n}\n.sectionTop{\n  margin-top:50px;\n}\n\n.testimonialTop{\n  margin-top:30px;\n  margin-bottom: 30px;\n}\n.names{\n\tcolor: #25aae1;\n\tfont-weight: bold;\n  font-size: 14px;\n  margin-bottom: 0;\n}\n.designation{\n\tfont-size: 12px;\n  font-weight: 500;\n  color: #9b9b9b;\n}\n.customLine{\n\tborder-top: 6px solid #f7d761;\n\tmargin-bottom: 0px;\n}\n.case-study-wrapper {\n  margin-bottom: 20px;\n}\n.customEnsoList {\n  padding-bottom: 20px;\n}\n.sectionTop2{\n  margin-top:70px;\n}\n.btn-custom {\n  border-radius: 30px;\n  background-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n  color: white;\n  text-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n.btn-custom img {\n\tmargin-top: -3px;\n\tmargin-left: 3px;\n\theight: 12px;\n}\n.testimonial-description {\n  padding-bottom: 20px;\n  margin-bottom: 0px;\n}\n\n.row{\n\tmargin-left:0px;\n\tmargin-right:0px;\n}\n.contentMargin{\n  margin-top:100px;\n}\n.solutionHeading{\n  font-weight: 600;\n}\n@media screen and (min-width: 768px) {\n  .solutionHeading{\n    color: #25aae1;\n  }\n  .img-width{\n    width:75%;\n  }\n  .custom-top{\n    margin-top:15px;\n    height: 40px;\n  }\n  .testimonial{\n    margin-top: 50px;\n    padding-bottom: 50px;\n  }\n  .case-study-section {\n    margin-top: 30px;\n  }\n}\n@media screen and (max-width: 767px) {\n  .solutionHeading{\n    text-align: center;\n    font-size: 20px;\n    margin-top: 0;\n    text-transform: uppercase;\n  }\n  .img-about-mobile {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n  }\n  .img-about-mobile img {\n    width: 50%;\n    height: 50%;\n  }\n  .heading-mobile {\n    color: #25aae1;\n    position: absolute;\n    left: 35%;\n  }\n  .contentMargin{\n    margin-top: 50px;\n  }\n  .btn-custom {\n    font-size: 14px;\n  }\n  .case-study-wrapper {\n    margin-bottom: 30px;\n    padding: 0;\n  }\n  .img-width {    \n    display: -webkit-box;    \n    display: -ms-flexbox;    \n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n  }\n  .img-width img{\n    width: 50%;\n  }\n  .custom-top{\n    margin-top:15px;\n    text-align: center;\n    font-size: 16px;\n  }\n  .SubHeadings {\n    font-size: 18px;\n  }\n  .testimonial{\n    margin-top: 50px;\n    padding-bottom: 50px;\n  }\n  .case-study-section {\n    margin-top: 50px;\n  }\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 819:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!pageContent\" class=\"container content-spinner\">\n\t<div class=\"col-md-12 loader\">\n\t\t<img class=\"\" src=\"../../../assets/images/content_spinner.gif\"/>\n\t</div>\n</div>\n<div *ngIf=\"pageContent\" [attr.data-wio-id]=\"pageContent.id\">\n    <div class=\"container-fluid row contentMargin no-padding\">\n        <div class=\"fullbodyContainer\">\n            <section class=\"caseStudies\" #caseStudies>\n                <div class=\"row\">\n                    <div class=\"col-lg-4 col-sm-4 col-md-4 col-xs-12 img-about-mobile no-padding\">\n                        <img alt={{casestudiesData.caesstudyimage.alt}} src={{casestudiesData.caesstudyimage.url}} height=\"350px\" width=\"75%\" class=\"img-responsive wow fadeInRight\" />\n                        <h2 class=\"heading-mobile hidden-lg hidden-md hidden-sm\">{{casestudiesData.datasheets[0].text}}</h2>\n                    </div>\n                    <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12 wow fadeInLeft\">\n                        <div class=\"row\">\n                            <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 no-padding\">\n                                <h1 class=\"solutionHeading hidden-xs\">{{casestudiesData.datasheets[0].text}}</h1>\n                                <p class=\"customMargin hidden-xs\">{{casestudiesData.datasheetdescription[0].text}}</p>\n                            </div>\n                        </div>\n                        <div class=\"row case-study-section\">\n                            <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 no-padding\">\n                                <ul class=\"nav nav-tabs nav-customTabs hidden-xs\" style=\"padding-left: 0px;\" id=\"case-study-tab\">\n                                    <li class=\"active\">\n                                        <a data-toggle=\"tab\" href=\"#home\">{{automobileCaseStudyData.tabtitleall[0].text}}</a>\n                                    </li>\n                                    <li>\n                                        <a data-toggle=\"tab\" href=\"#menu1\">{{healthcareCasestudyData.casestudytitle[0].text}}</a>\n                                    </li>\n                                    <li>\n                                        <a data-toggle=\"tab\" href=\"#menu2\">{{technologyCaseStudyData.casestudytitle[0].text}}</a>\n                                    </li>\n                                    <li>\n                                        <a data-toggle=\"tab\" href=\"#menu2\">{{automobileCaseStudyData.casestudytitle[0].text}}</a>\n                                    </li>\n                                </ul>\n                                <select id='case-study-select' class=\"nav nav-tabs form-control hidden-md hidden-sm hidden-lg\">\n                                    <option class=\"form-control\" value='0'>{{automobileCaseStudyData.tabtitleall[0].text}}</option>\n                                    <option class=\"form-control\" value='1'>{{healthcareCasestudyData.casestudytitle[0].text}}</option>\n                                    <option class=\"form-control\" value='2'>{{technologyCaseStudyData.casestudytitle[0].text}}</option>\n                                    <option class=\"form-control\" value='3'>{{automobileCaseStudyData.casestudytitle[0].text}}</option>\n                                </select>\n                                <div class=\"tab-content sectionTop \">\n                                    <div id=\"home\" class=\"tab-pane fade in active\">\n                                        <div class=\"row sectionTop\">\n                                            <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 leftZero case-study-wrapper\" *ngFor=\"let item of casestudiesData.datasheets1\">\n                                                <div class=\"img-width\">\n                                                    <img alt={{item.datasheetimage.alt}} src={{item.datasheetimage.url}} class=\"img-responsive\" />\n                                                    <p class=\"custom-top\">{{item.datasheettext[0].text}}</p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div id=\"menu1\" class=\"tab-pane fade\">\n                                        <p class=\"sectionTop customMargin\">{{healthcareCasestudyData.sectionone[0].text}}</p>\n                                        <ul class=\"customMargin\" type=\"square\">\n                                            <li>\n                                                <span>{{technologyCaseStudyData.sectiontwo[0].pointone[0].text}}</span>\n                                            </li>\n                                            <li>\n                                                <span>{{technologyCaseStudyData.sectiontwo[0].pointtwo[0].text}}</span>\n                                            </li>\n                                            <li>\n                                                <span>{{technologyCaseStudyData.sectiontwo[0].pointthree[0].text}}</span>\n                                            </li>\n                                        </ul>\n                                    </div>\n                                    <div id=\"menu2\" class=\"tab-pane fade\">\n                                        <p class=\"sectionTop customMargin\">{{automobileCaseStudyData.sectionone[0].text}}</p>\n                                        <ul class=\"customMargin\" type=\"square\">\n                                            <li>\n                                                <span>{{automobileCaseStudyData.sectiontwo[0].pointone[0].text}}</span>\n                                            </li>\n                                            <li>\n                                                <span>{{automobileCaseStudyData.sectiontwo[0].pointtwo[0].text}}</span>\n                                            </li>\n                                            <li>\n                                                <span>{{automobileCaseStudyData.sectiontwo[0].pointthree[0].text}}</span>\n                                            </li>\n                                        </ul>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </section>\n            <section class=\"testimonial\" #testimonials>\n                <div class=\"row\">\n                    <div class=\"col-lg-4 col-sm-4 col-md-4 hidden-xs no-padding\">\n                    </div>\n                    <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12\">\n                        <div class=\"row\">\n                            <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 no-padding\">\n                                <h1 class=\"solutionHeading\">{{testimonialData.maintitle[0].text}}</h1>\n                                <p class=\"customMargin hidden-xs testimonial-description\">{{testimonialData.subtitle[0].text}}</p>\n                            </div>\n                        </div>\n                        <div class=\"\" *ngFor=\"let tdata of testimonialData.testimonialdata; let k = index;\">\n                            <hr class=\"customLine\">\n                            <div class=\"row\">\n                                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 customEnsoList no-padding\">\n                                    <h3 class=\"SubHeadings\">{{tdata.title1[0].text}}</h3>\n                                    <p class=\"testimonialTop\">\n                                            {{tdata.description[0].text}}\n                                    </p>\n                                    <p class=\"names\">{{tdata.name[0].text}}</p>\n                                    <span class=\"designation\">{{tdata.designation[0].text}}</span>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </section>\n            \n        </div>\n    </div>\n    <div class=\"contentMargin\">\n        <section class=\"efficiency\" #customers [ngStyle]=\"{'background-image': 'url(' + efficiencyData.background.url + ')'}\">\n            <div class=\"row  wow fadeInUp\">\n                <div class=\"col-md-8 col-md-offset-2 no-padding text-center\">\n                    <h4 class=\"efficiencyText\">{{efficiencyData.maintitle[0].text}}</h4>\n                    <div class=\"efficiencyStatement sectionTop-efficiency\">{{efficiencyData.subtitle[0].text}}</div>\n                    <button routerLink=\"/getstarted\" class=\"btn btn-custom bottomMargin sectionTop-efficiency\">{{efficiencyData.getstartedlabel[0].text}}</button>\n                </div>\n            </div>\n        </section>\n    </div>\n</div>\n"

/***/ })

});
//# sourceMappingURL=5.chunk.js.map