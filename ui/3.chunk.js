webpackJsonp([3,13],{

/***/ 780:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__open_positions_component__ = __webpack_require__(799);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(50);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OpenPositionsModule", function() { return OpenPositionsModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: "",
        component: __WEBPACK_IMPORTED_MODULE_2__open_positions_component__["a" /* OpenPositionsComponent */]
    }
];
var OpenPositionsModule = (function () {
    function OpenPositionsModule() {
    }
    return OpenPositionsModule;
}());
OpenPositionsModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* RouterModule */].forChild(routes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__open_positions_component__["a" /* OpenPositionsComponent */]]
    })
], OpenPositionsModule);

//# sourceMappingURL=open-positions.module.js.map

/***/ }),

/***/ 784:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, "/* Efficiency */\n.efficiency{\n    background-position: right 50px;\n    background-repeat: no-repeat;\n\tbackground-size: 40%;\n    background-color: #0c3445;\n        padding-top: 50px;\n    padding-bottom: 50px;\n}\n.efficiencyText{\n    font-size: 30px;\n    color: white;\n\tfont-weight: bold;\n\tline-height: 1.2;\n}\n.efficiencyStatement{\n\tfont-size: 18px;\n  \tfont-weight: 500;\n\tcolor: #4a4a4a;\n\tline-height: 1.5;\n}\n.sectionTop-efficiency{\n    margin-top:70px;\n}\n.bottomMargin{\n    margin-bottom: 100px;\n}\n.btn-custom {\n\tborder-radius: 30px;\n\tbackground-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n\tcolor: white;\n\ttext-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n\n.btn-custom:active, .btn-custom:focus {\n    outline: none;\n}\n/* Efficiency CSS */\n@media screen and (max-width: 767px){\n\t.efficiency{\n\t\tbackground-position: 50px 20px;\n\t\tbackground-size: 100%;\n\t}\n\t.efficiencyText{\n        font-size: 18px;\n\t\tpadding: 0 15px;\n\t\tmargin-top: 50px;\n    }\n    .efficiencyStatement{\n\t\tfont-size: 14px;\n\t\tmargin-top: 50px;\n        padding: 0 15px;\n\t}\n\t.bottomMargin{\n\t\tmargin-bottom: 50px;\n\t}\n\t.sectionTop-efficiency{\n\t\tmargin-top:50px;\n\t}\n\t.efficiency .btn-custom {\n\t\tfont-size: 14px;\n\t}\n}\n", ""]);

// exports


/***/ }),

/***/ 787:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, ".main-title {\n    color: #25aae1;\n    font-weight: 600;\n    font-size: 40px;\n}\n.main-title-margin {\n    margin-top: 100px;\n}\n.sectionBorder6{\n\tborder-top: 6px solid #f7d761;\n\tmargin-top: 0px;\n}\n.margintop50 {\n    margin-top: 50px;\n}\n.marginbottom50 {\n    margin-bottom: 50px;\n}\n.margintop30 {\n    margin-top: 30px;\n}\n.marginbottom30 {\n    margin-bottom: 30px;\n}\n@media screen and (max-width: 767px) {\n    .main-title {\n        font-size: 20px;\n        text-transform: uppercase;\n    }\n    .main-title-margin {\n        margin-top: 50px;\n    }\n}", ""]);

// exports


/***/ }),

/***/ 799:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prismic_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(40);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OpenPositionsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var OpenPositionsComponent = (function () {
    function OpenPositionsComponent(prismic, route, titleService, meta) {
        this.prismic = prismic;
        this.route = route;
        this.titleService = titleService;
        this.meta = meta;
        this.PrismicDOM = __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default.a;
        this.toolbar = false;
        this.current = 0;
        this.pageLoaded = false;
    }
    OpenPositionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeStream = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].fromPromise(this.prismic.buildContext())
            .subscribe(function (ctx) {
            _this.ctx = ctx;
            _this.fetchPage();
        });
    };
    OpenPositionsComponent.prototype.ngOnDestroy = function () {
        this.routeStream.unsubscribe();
    };
    OpenPositionsComponent.prototype.handleAccordionClick = function (item, index) {
        item.isOpen = !item.isOpen;
    };
    OpenPositionsComponent.prototype.ngAfterViewChecked = function () {
        if (this.positionsSection && this.positionsSection.nativeElement && !this.pageLoaded) {
            // this.positionsSection.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            window.scrollTo(0, 0);
            this.pageLoaded = true;
        }
    };
    OpenPositionsComponent.prototype.fetchPage = function () {
        var _this = this;
        this.ctx.api.query([
            __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default.a.Predicates.any('document.type', ['openpositions', 'efficiency']),
        ])
            .then(function (response) {
            _this.toolbar = false;
            _this.pageContent = response.results;
            var metaDescription = "";
            var metaKeyWords = [];
            _this.pageContent.map(function (prop) {
                if (prop.uid === 'openpositions') {
                    _this.openPositionsData = prop.data;
                    metaDescription = _this.openPositionsData['meta-description'];
                    metaKeyWords.push(_this.openPositionsData['meta-title']);
                    _this.openPositionsData.openpositionsdata.map(function (item, index) {
                        if (index === 0) {
                            item.isOpen = true;
                        }
                        else {
                            item.isOpen = false;
                        }
                    });
                }
                else if (prop.uid === 'efficiency') {
                    _this.efficiencyData = prop.data;
                    metaKeyWords.push(_this.openPositionsData['meta-title']);
                }
            });
            _this.meta.updateTag({ name: 'description', content: metaDescription });
            _this.meta.updateTag({ name: 'author', content: 'Xpms' });
            _this.meta.updateTag({ name: 'keywords', content: metaKeyWords.toString() });
        })
            .catch(function (e) { return console.log(e); });
    };
    return OpenPositionsComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('positionsSection'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], OpenPositionsComponent.prototype, "positionsSection", void 0);
OpenPositionsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-open-positions',
        template: __webpack_require__(816),
        styles: [__webpack_require__(808)]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */]) === "function" && _e || Object])
], OpenPositionsComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=open-positions.component.js.map

/***/ }),

/***/ 808:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports
exports.i(__webpack_require__(784), "");
exports.i(__webpack_require__(787), "");

// module
exports.push([module.i, ".position-description {\n    margin-top: 40px;\n    margin-bottom: 20px;\n    font-size:14px;\n}\n.position-data {\n    padding-bottom: 30px;\n}\n.title-section {\n    cursor: pointer;\n    text-decoration: none;\n    color: #000;\n}\n.title-section .image-section{\n    width: 20px;\n}\n.description-label, .responsibility-label {\n    margin: 20px 0;\n}\n.responsibility-content ul li span{ \n    margin-right: 15px;\n}\n.send-resume-label {\n    margin: 20px 0 0 0;\n}\n.open-position-accordion {\n    margin-bottom: 50px;\n}\n.accordion-arrow-down {\n    width: 6px;\n    margin-bottom: 3px;\n    margin-right: 5px;\n}\n.accordion-arrow-right {\n    width:13px;\n    margin-bottom: 3px;\n    margin-right: 5px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 816:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!pageContent\" class=\"container content-spinner\">\n\t<div class=\"col-md-12 loader\">\n\t\t<img class=\"\" src=\"../../../assets/images/content_spinner.gif\"/>\n\t</div>\n</div>\n<div *ngIf=\"pageContent\" [attr.data-wio-id]=\"pageContent.id\">\n    <div class=\"main-title-margin\" style=\"background-repeat: no-repeat\"  [ngStyle]=\"{'background-image': 'url(' + openPositionsData.mainbackground.url + ')'}\">\n        <div class=\"container\" >\n            <div class=\"row\">\n                <div class=\"col-md-12 col-xs-12 no-padding\" #positionsSection>\n                    <div class=\"row\">\n                        <h1 class=\"col-md-12 col-xs-12 main-title\">\n                            {{openPositionsData.sectiontitle[0].text}}\n                        </h1>\n                        <div class=\"col-md-12 col-xs-12 position-description\">\n                            {{openPositionsData.sectiondescription[0].text}}\n                        </div>\n                        <div class=\"col-md-12 col-xs-12 open-position-accordion\" id=\"positions-accordion\">\n                            <div class=\"position-data\" *ngFor=\"let item of openPositionsData.openpositionsdata;let i = index\">\n                                <div class=\"\" (click)=\"handleAccordionClick(item, i)\" >\n                                    <h4 class=\"panel-title\">\n                                        <div class=\"row\">\n                                            <div class=\"col-md-3 col-sm-3 col-xs-6\">\n                                                <hr class=\"sectionBorder6 marginbottom30\"/>\n                                            </div>\n                                            <div class=\"col-md-12 col-sm-12 col-xs-12\">\n                                                <a class=\"title-section\">\n                                                    <span class=\"image-section\">\n                                                        <img class=\"accordion-arrow-right\" src=\"../../../assets/images/accordion-arrow-down.png\" *ngIf=\"item.isOpen\" />\n                                                        <img class=\"accordion-arrow-down\" src=\"../../../assets/images/accordion-arrow-right.png\" *ngIf=\"!item.isOpen\"/>\n                                                    </span>\n                                                    <span>\n                                                        {{item.positiontitle[0].text}}\n                                                    </span>\n                                                </a>\n                                            </div>\n                                        </div>\n                                    </h4>\n                                </div>\n                                <div id=\"collapse1\" class=\"panel-collapse\" [class.collapse]='!item.isOpen'>\n                                    <div class=\"panel-body\">\n                                        <div class=\"row\">\n                                            <div class=\"col-md-12 col-sm-12 col-xs-12 description-label\">\n                                                <label>{{openPositionsData.descriptionlabel[0].text}}</label>\n                                            </div>\n                                            <div class=\"col-md-12 col-sm-12 col-xs-12 description-content\">\n                                                <p>{{item.positiondescription[0].text}}</p>\n                                            </div>\n                                            <div class=\"col-md-12 col-sm-12 col-xs-12 responsibility-label\">\n                                                <label>{{openPositionsData.keyresponsibilitylabel[0].text}}</label>\n                                            </div>\n                                            <div class=\"col-md-12 col-sm-12 col-xs-12 responsibility-content\">\n                                                <ul class=\"\">\n                                                    <li>\n                                                        <span>&#45;</span>{{item.listone[0].text}}\n                                                    </li>\n                                                    <li>\n                                                        <span>&#45;</span>{{item.listtwo[0].text}}\n                                                    </li>\n                                                    <li>\n                                                        <span>&#45;</span>{{item.listthree[0].text}}\n                                                    </li>\n                                                </ul>\n                                            </div>\n                                            <div class=\"col-md-12 col-xs-12\">\n                                                <h5 class=\"send-resume-label\">{{openPositionsData.sendresumelabel[0].text}}\n                                                    <a\n                                                    href=\"mailto:careers@xpms.io\" class=\"about_heading d-inline\"\n                                                    style=\"color:#4a90e2;\">{{openPositionsData.mailid[0].text}}</a>\n                                                </h5>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"container-fluid\">\n            <div class=\"row efficiency\" #efficiency [ngStyle]=\"{'background-image': 'url(' + efficiencyData.background.url + ')'}\">\n                <div class=\"col-md-8 col-md-offset-2 text-center\">\n                    <h4 class=\"efficiencyText\">{{efficiencyData.maintitle[0].text}}</h4>\n                    <div class=\"efficiencyStatement sectionTop-efficiency\">{{efficiencyData.subtitle[0].text}}</div>\n                    <button class=\"btn btn-custom bottomMargin sectionTop-efficiency \">{{efficiencyData.getstartedlabel[0].text}}</button>\n                </div>\n            </div>\n        </div>\n    </div>\n    \n</div>"

/***/ })

});
//# sourceMappingURL=3.chunk.js.map