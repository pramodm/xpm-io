webpackJsonp([6,13],{

/***/ 779:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__get_started_component__ = __webpack_require__(798);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(50);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetStartedModule", function() { return GetStartedModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: "",
        component: __WEBPACK_IMPORTED_MODULE_2__get_started_component__["a" /* GetStartedComponent */]
    }
];
var GetStartedModule = (function () {
    function GetStartedModule() {
    }
    return GetStartedModule;
}());
GetStartedModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* RouterModule */].forChild(routes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__get_started_component__["a" /* GetStartedComponent */]]
    })
], GetStartedModule);

//# sourceMappingURL=get-started.module.js.map

/***/ }),

/***/ 787:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, ".main-title {\n    color: #25aae1;\n    font-weight: 600;\n    font-size: 40px;\n}\n.main-title-margin {\n    margin-top: 100px;\n}\n.sectionBorder6{\n\tborder-top: 6px solid #f7d761;\n\tmargin-top: 0px;\n}\n.margintop50 {\n    margin-top: 50px;\n}\n.marginbottom50 {\n    margin-bottom: 50px;\n}\n.margintop30 {\n    margin-top: 30px;\n}\n.marginbottom30 {\n    margin-bottom: 30px;\n}\n@media screen and (max-width: 767px) {\n    .main-title {\n        font-size: 20px;\n        text-transform: uppercase;\n    }\n    .main-title-margin {\n        margin-top: 50px;\n    }\n}", ""]);

// exports


/***/ }),

/***/ 798:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prismic_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(40);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GetStartedComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var GetStartedComponent = (function () {
    function GetStartedComponent(prismic, route, titleService, meta) {
        this.prismic = prismic;
        this.route = route;
        this.titleService = titleService;
        this.meta = meta;
        this.PrismicDOM = __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default.a;
        this.toolbar = false;
        this.pageLoaded = false;
    }
    GetStartedComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeStream = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].fromPromise(this.prismic.buildContext())
            .subscribe(function (ctx) {
            _this.ctx = ctx;
            _this.fetchPage();
        });
    };
    GetStartedComponent.prototype.ngOnDestroy = function () {
        this.routeStream.unsubscribe();
    };
    GetStartedComponent.prototype.ngAfterViewChecked = function () {
        if (this.startedSection && this.startedSection.nativeElement && !this.pageLoaded) {
            window.scrollTo(0, 0);
            this.pageLoaded = true;
        }
    };
    GetStartedComponent.prototype.fetchPage = function () {
        var _this = this;
        this.ctx.api.query([
            __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default.a.Predicates.any('document.type', ['getstarted']),
        ])
            .then(function (response) {
            _this.toolbar = false;
            _this.pageContent = response.results;
            var metaDescription = "";
            var metaKeyWords = [];
            _this.pageContent.map(function (prop) {
                if (prop.uid === 'getstarted') {
                    _this.getStartedData = prop.data;
                    metaDescription = _this.getStartedData['meta-description'];
                    metaKeyWords.push(_this.getStartedData['meta-title']);
                }
            });
            _this.meta.updateTag({ name: 'description', content: metaDescription });
            _this.meta.updateTag({ name: 'author', content: 'Xpms' });
            _this.meta.updateTag({ name: 'keywords', content: metaKeyWords.toString() });
        })
            .catch(function (e) { return console.log(e); });
    };
    return GetStartedComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('startedSection'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], GetStartedComponent.prototype, "startedSection", void 0);
GetStartedComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-get-started',
        template: __webpack_require__(815),
        styles: [__webpack_require__(807)]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */]) === "function" && _e || Object])
], GetStartedComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=get-started.component.js.map

/***/ }),

/***/ 807:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports
exports.i(__webpack_require__(787), "");

// module
exports.push([module.i, ".engagement-bg {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 100%;\n    margin: 30px 0 50px 0;\n}\n.submit-request-section {\n    margin-bottom: 50px;\n    margin-top: 30px;\n}\n.demoForm{\n    border: none;\n    border-radius: 0px;\n    background-color: transparent;\n    border-bottom: 1px solid #aeaeae;\n    box-shadow: none;\n}\n.demobtn{\n    background-color:#f7d761;\n    border-radius:0px;\n    font-weight:bold;\n    border-color: #f7d761;\n    width: 75px;\n}\n.btn-request {\n    border-radius: 30px;\n\tbackground-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n\tcolor: white;\n\ttext-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n    margin-top: 30px;\n\tpadding: 6px 20px;\n}\n.btn-cancel {\n    border-radius: 30px;\n\tcolor: #000;\n\ttext-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n    padding: 6px 20px;\n    margin-top: 30px;\n}\n.submit-request-section .content{\n    margin-bottom:30px;\n}\n@media screen and (min-width: 768px) {\n    .submit-request-section .content{\n        padding-left: 0;\n    }\n    .get-started-content {\n        padding: 0;\n    }\n}\n@media screen and (max-width: 767px) {\n    .btn-request,.btn-cancel {\n\t\tfont-size: 14px;\n\t}\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 815:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!pageContent\" class=\"container content-spinner\">\n  <div class=\"col-md-12 loader\">\n    <img class=\"\" src=\"../../../assets/images/content_spinner.gif\"/>\n  </div>\n</div>\n<div *ngIf=\"pageContent\" [attr.data-wio-id]=\"pageContent.id\">\n  <div class=\"container main-title-margin\" >\n    <div class=\"row\">\n      <div class=\"col-md-12 col-xs-12 get-started-content\" #startedSection>\n        <hr class=\"sectionBorder6 marginbottom50\"/>\n        <h1 class=\"text-center main-title\">{{getStartedData.engagementlabel[0].text}}</h1>\n        <div class=\"engagement-bg\">\n            <img alt={{getStartedData.engagementbg.alt}} src={{getStartedData.engagementbg.url}}  class=\"img-responsive\"/>\n        </div>\n        <hr class=\"sectionBorder6 marginbottom50\"/>\n      </div>\n      <div class=\"col-md-12 col-xs-12 submit-request-section\">\n        <form class=\"row\">\n          <div class=\"col-md-4 col-sm-4 col-xs-12 content\">\n            <div class=\"input-group \">\n              <div class=\"input-group-btn\">\n                <button type=\"button\" class=\"btn btn-default demobtn\">{{getStartedData.namelabel[0].text}}</button>\n              </div>\n              <input class=\"form-control demoForm\" placeholder=\"Name\">\n            </div>\n          </div>\n          <div class=\"col-md-4 col-sm-4 col-xs-12 content\">\n            <div class=\"input-group \">\n              <div class=\"input-group-btn\">\n                <button type=\"button\" class=\"btn btn-default demobtn\">{{getStartedData.mobilelabel[0].text}}</button>\n              </div>\n              <input class=\"form-control demoForm\" placeholder=\"Mobile\">\n            </div>\n          </div>\n          <div class=\"col-md-4 col-sm-4 col-xs-12 content\">\n            <div class=\"input-group \">\n              <div class=\"input-group-btn\">\n                <button type=\"button\" class=\"btn btn-default demobtn\">{{getStartedData.emaillabel[0].text}}</button>\n              </div>\n              <input class=\"form-control demoForm\" placeholder=\"Email\">\n            </div>\n          </div>\n          <div class=\"col-md-offset-2 col-md-6 text-center\">\n            <button class=\"btn btn-request\">\n              {{getStartedData.submitlabel[0].text}}\n            </button>\n            <button class=\"btn btn-default btn-cancel\">\n              {{getStartedData.cancellabel[0].text}}\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ })

});
//# sourceMappingURL=6.chunk.js.map