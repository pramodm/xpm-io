webpackJsonp([1,13],{

/***/ 777:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__enso_component__ = __webpack_require__(796);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngu_carousel__ = __webpack_require__(788);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(50);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnsoModule", function() { return EnsoModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: "",
        component: __WEBPACK_IMPORTED_MODULE_2__enso_component__["a" /* EnsoComponent */]
    }
];
var EnsoModule = (function () {
    function EnsoModule() {
    }
    return EnsoModule;
}());
EnsoModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [__WEBPACK_IMPORTED_MODULE_2__enso_component__["a" /* EnsoComponent */]],
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_4__ngu_carousel__["a" /* NguCarouselModule */], __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* RouterModule */].forChild(routes)],
        providers: [__WEBPACK_IMPORTED_MODULE_3__prismic_prismic_service__["a" /* PrismicService */]]
    })
], EnsoModule);

//# sourceMappingURL=enso.module.js.map

/***/ }),

/***/ 785:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguCarouselService; });


var NguCarouselService = /** @class */ (function () {
    function NguCarouselService() {
        this.getData = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
    }
    NguCarouselService.prototype.reset = function (token) {
        this.getData.next({ id: token, ref: 'reset' });
    };
    NguCarouselService.prototype.moveToSlide = function (token, index, animate) {
        this.getData.next({
            id: token,
            ref: 'moveToSlide',
            index: index,
            animation: animate
        });
    };
    NguCarouselService.prototype.destroy = function (token) {
        this.getData.next({ id: token, ref: 'destroy' });
    };
    NguCarouselService.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    /** @nocollapse */
    NguCarouselService.ctorParameters = function () { return []; };
    return NguCarouselService;
}());

//# sourceMappingURL=ngu-carousel.service.js.map

/***/ }),

/***/ 786:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return NguCarouselItemDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return NguCarouselNextDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return NguCarouselPrevDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguCarouselPointDirective; });

var NguCarouselItemDirective = /** @class */ (function () {
    function NguCarouselItemDirective() {
    }
    NguCarouselItemDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{
                    // tslint:disable-next-line:directive-selector
                    selector: '[NguCarouselItem]'
                },] },
    ];
    /** @nocollapse */
    NguCarouselItemDirective.ctorParameters = function () { return []; };
    return NguCarouselItemDirective;
}());

var NguCarouselNextDirective = /** @class */ (function () {
    function NguCarouselNextDirective() {
    }
    NguCarouselNextDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{
                    // tslint:disable-next-line:directive-selector
                    selector: '[NguCarouselNext]'
                },] },
    ];
    /** @nocollapse */
    NguCarouselNextDirective.ctorParameters = function () { return []; };
    return NguCarouselNextDirective;
}());

var NguCarouselPrevDirective = /** @class */ (function () {
    function NguCarouselPrevDirective() {
    }
    NguCarouselPrevDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{
                    // tslint:disable-next-line:directive-selector
                    selector: '[NguCarouselPrev]'
                },] },
    ];
    /** @nocollapse */
    NguCarouselPrevDirective.ctorParameters = function () { return []; };
    return NguCarouselPrevDirective;
}());

var NguCarouselPointDirective = /** @class */ (function () {
    function NguCarouselPointDirective() {
    }
    NguCarouselPointDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{
                    // tslint:disable-next-line:directive-selector
                    selector: '[NguCarouselPoint]'
                },] },
    ];
    /** @nocollapse */
    NguCarouselPointDirective.ctorParameters = function () { return []; };
    return NguCarouselPointDirective;
}());

//# sourceMappingURL=ngu-carousel.directive.js.map

/***/ }),

/***/ 788:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_ngu_carousel_module__ = __webpack_require__(789);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__src_ngu_carousel_module__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__src_ngu_carousel_ngu_carousel_interface__ = __webpack_require__(791);
/* unused harmony reexport NguCarousel */
/* unused harmony reexport NguCarouselStore */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__src_ngu_carousel_service__ = __webpack_require__(785);
/* unused harmony reexport NguCarouselService */



//# sourceMappingURL=index.js.map

/***/ }),

/***/ 789:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__ = __webpack_require__(786);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngu_item_ngu_item_component__ = __webpack_require__(792);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngu_carousel_ngu_carousel_component__ = __webpack_require__(790);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngu_tile_ngu_tile_component__ = __webpack_require__(793);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngu_carousel_service__ = __webpack_require__(785);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguCarouselModule; });







var NguCarouselModule = /** @class */ (function () {
    function NguCarouselModule() {
    }
    NguCarouselModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"], args: [{
                    imports: [__WEBPACK_IMPORTED_MODULE_3__angular_common__["CommonModule"]],
                    exports: [
                        __WEBPACK_IMPORTED_MODULE_4__ngu_carousel_ngu_carousel_component__["a" /* NguCarouselComponent */],
                        __WEBPACK_IMPORTED_MODULE_1__ngu_item_ngu_item_component__["a" /* NguItemComponent */],
                        __WEBPACK_IMPORTED_MODULE_5__ngu_tile_ngu_tile_component__["a" /* NguTileComponent */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["a" /* NguCarouselPointDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["b" /* NguCarouselItemDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["c" /* NguCarouselNextDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["d" /* NguCarouselPrevDirective */]
                    ],
                    declarations: [
                        __WEBPACK_IMPORTED_MODULE_4__ngu_carousel_ngu_carousel_component__["a" /* NguCarouselComponent */],
                        __WEBPACK_IMPORTED_MODULE_1__ngu_item_ngu_item_component__["a" /* NguItemComponent */],
                        __WEBPACK_IMPORTED_MODULE_5__ngu_tile_ngu_tile_component__["a" /* NguTileComponent */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["a" /* NguCarouselPointDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["b" /* NguCarouselItemDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["c" /* NguCarouselNextDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["d" /* NguCarouselPrevDirective */]
                    ],
                    providers: [__WEBPACK_IMPORTED_MODULE_6__ngu_carousel_service__["a" /* NguCarouselService */]]
                },] },
    ];
    /** @nocollapse */
    NguCarouselModule.ctorParameters = function () { return []; };
    return NguCarouselModule;
}());

//# sourceMappingURL=ngu-carousel.module.js.map

/***/ }),

/***/ 790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__ = __webpack_require__(786);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngu_carousel_service__ = __webpack_require__(785);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguCarouselComponent; });




var NguCarouselComponent = /** @class */ (function () {
    function NguCarouselComponent(el, renderer, carouselSer, platformId) {
        this.el = el;
        this.renderer = renderer;
        this.carouselSer = carouselSer;
        this.platformId = platformId;
        this.carouselLoad = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        // tslint:disable-next-line:no-output-on-prefix
        this.onMove = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.initData = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.pauseCarousel = false;
        this.Arr1 = Array;
        this.pointNumbers = [];
        this.data = {
            type: 'fixed',
            token: '',
            deviceType: 'lg',
            items: 0,
            load: 0,
            deviceWidth: 0,
            carouselWidth: 0,
            itemWidth: 0,
            visibleItems: { start: 0, end: 0 },
            slideItems: 0,
            itemWidthPer: 0,
            itemLength: 0,
            currentSlide: 0,
            easing: 'cubic-bezier(0, 0, 0.2, 1)',
            speed: 400,
            transform: { xs: 0, sm: 0, md: 0, lg: 0, all: 0 },
            loop: false,
            dexVal: 0,
            touchTransform: 0,
            touch: { active: false, swipe: '', velocity: 0 },
            isEnd: false,
            isFirst: true,
            isLast: false,
            RTL: false,
            button: { visibility: 'disabled' },
            point: true,
            vertical: { enabled: false, height: 400 }
        };
    }
    NguCarouselComponent.prototype.ngOnChanges = function (changes) { };
    NguCarouselComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.carousel = this.el.nativeElement;
        this.carouselMain = this.carouselMain1.nativeElement;
        this.carouselInner = this.carouselInner1.nativeElement;
        this.rightBtn = this.next.nativeElement;
        this.leftBtn = this.prev.nativeElement;
        this.data.type = this.userData.grid.all !== 0 ? 'fixed' : 'responsive';
        this.data.loop = this.userData.loop || false;
        this.userData.easing = this.userData.easing || 'cubic-bezier(0, 0, 0.2, 1)';
        this.data.touch.active = this.userData.touch || false;
        this.data.RTL = this.userData.RTL ? true : false;
        if (this.userData.vertical && this.userData.vertical.enabled) {
            this.data.vertical.enabled = this.userData.vertical.enabled;
            this.data.vertical.height = this.userData.vertical.height;
        }
        this.directionSym = this.data.RTL ? '' : '-';
        this.data.point =
            this.userData.point && typeof this.userData.point.visible !== 'undefined'
                ? this.userData.point.visible
                : true;
        this.withAnim = true;
        this.carouselSize();
        // const datas = this.itemsElements.first.nativeElement.getBoundingClientRect().width;
        this.carouselSerSub = this.carouselSer.getData.subscribe(function (res) {
            if (res.id === _this.data.token) {
                if (res.ref === 'moveToSlide') {
                    if (_this.pointers !== res.index && res.index < _this.pointIndex) {
                        _this.withAnim = res.animation === false ? false : true;
                        _this.moveTo(res.index);
                    }
                }
                else if (res.ref === 'reset') {
                    if (_this.pointers !== 0 && 0 < _this.pointIndex) {
                        _this.withAnim = false;
                        _this.reset();
                    }
                }
            }
        });
    };
    NguCarouselComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        this.listener1 = this.renderer.listen(this.leftBtn, 'click', function () {
            return _this.carouselScrollOne(0);
        });
        this.listener2 = this.renderer.listen(this.rightBtn, 'click', function () {
            return _this.carouselScrollOne(1);
        });
        this.carouselCssNode = this.createStyleElem();
        this.storeCarouselData();
        this.buttonControl();
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_common__["isPlatformBrowser"])(this.platformId)) {
            this.carouselInterval();
            this.onWindowScrolling();
            if (!this.data.vertical.enabled) {
                this.touch();
            }
            this.listener3 = this.renderer.listen('window', 'resize', function (event) {
                _this.onResizing(event);
            });
        }
        this.items.changes.subscribe(function (val) {
            _this.data.isLast = false;
            _this.carouselPoint();
            _this.buttonControl();
        });
    };
    NguCarouselComponent.prototype.ngAfterViewInit = function () {
        if (this.userData.point.pointStyles) {
            var datas = this.userData.point.pointStyles.replace(/.ngucarouselPoint/g, "." + this.data.token + " .ngucarouselPoint");
            this.createStyleElem(datas);
        }
        else if (this.userData.point && this.userData.point.visible) {
            this.renderer.addClass(this.pointMain.nativeElement, 'ngucarouselPointDefault');
        }
    };
    NguCarouselComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.carouselInt);
        // tslint:disable-next-line:no-unused-expression
        this.itemsSubscribe && this.itemsSubscribe.unsubscribe();
        this.carouselSerSub && this.carouselSerSub.unsubscribe();
        this.carouselLoad.complete();
        // remove listeners
        for (var i = 1; i <= 8; i++) {
            // tslint:disable-next-line:no-eval
            eval("this.listener" + i + " && this.listener" + i + "()");
        }
    };
    NguCarouselComponent.prototype.onResizing = function (event) {
        var _this = this;
        clearTimeout(this.onResize);
        this.onResize = setTimeout(function () {
            if (_this.data.deviceWidth !== event.target.outerWidth) {
                _this.setStyle(_this.carouselInner, 'transition', "");
                _this.storeCarouselData();
            }
        }, 500);
    };
    /** Get Touch input */
    NguCarouselComponent.prototype.touch = function () {
        var _this = this;
        if (this.userData.touch) {
            var hammertime = new Hammer(this.carouselInner);
            hammertime.get('pan').set({ direction: Hammer.DIRECTION_HORIZONTAL });
            hammertime.on('panstart', function (ev) {
                _this.data.carouselWidth = _this.carouselInner.offsetWidth;
                _this.data.touchTransform = _this.data.transform[_this.data.deviceType];
                _this.data.dexVal = 0;
                _this.setStyle(_this.carouselInner, 'transition', '');
            });
            if (this.data.vertical.enabled) {
                hammertime.on('panup', function (ev) {
                    _this.touchHandling('panleft', ev);
                });
                hammertime.on('pandown', function (ev) {
                    _this.touchHandling('panright', ev);
                });
            }
            else {
                hammertime.on('panleft', function (ev) {
                    _this.touchHandling('panleft', ev);
                });
                hammertime.on('panright', function (ev) {
                    _this.touchHandling('panright', ev);
                });
            }
            hammertime.on('panend', function (ev) {
                if (Math.abs(ev.velocity) > 1) {
                    _this.data.touch.velocity = ev.velocity;
                    var direc = 0;
                    if (!_this.data.RTL) {
                        direc = _this.data.touch.swipe === 'panright' ? 0 : 1;
                    }
                    else {
                        direc = _this.data.touch.swipe === 'panright' ? 1 : 0;
                    }
                    _this.carouselScrollOne(direc);
                }
                else {
                    _this.data.dexVal = 0;
                    _this.setStyle(_this.carouselInner, 'transition', 'transform 324ms cubic-bezier(0, 0, 0.2, 1)');
                    _this.setStyle(_this.carouselInner, 'transform', '');
                }
            });
            hammertime.on('hammer.input', function (ev) {
                // allow nested touch events to no propagate, this may have other side affects but works for now.
                // TODO: It is probably better to check the source element of the event and only apply the handle to the correct carousel
                ev.srcEvent.stopPropagation();
            });
        }
    };
    /** handle touch input */
    NguCarouselComponent.prototype.touchHandling = function (e, ev) {
        // vertical touch events seem to cause to panstart event with an odd delta
        // and a center of {x:0,y:0} so this will ignore them
        if (ev.center.x === 0) {
            return;
        }
        ev = Math.abs(this.data.vertical.enabled ? ev.deltaY : ev.deltaX);
        var valt = ev - this.data.dexVal;
        valt =
            this.data.type === 'responsive'
                ? Math.abs(ev - this.data.dexVal) /
                    (this.data.vertical.enabled
                        ? this.data.vertical.height
                        : this.data.carouselWidth) *
                    100
                : valt;
        this.data.dexVal = ev;
        this.data.touch.swipe = e;
        if (!this.data.RTL) {
            this.data.touchTransform =
                e === 'panleft'
                    ? valt + this.data.touchTransform
                    : this.data.touchTransform - valt;
        }
        else {
            this.data.touchTransform =
                e === 'panright'
                    ? valt + this.data.touchTransform
                    : this.data.touchTransform - valt;
        }
        if (this.data.touchTransform > 0) {
            if (this.data.type === 'responsive') {
                this.setStyle(this.carouselInner, 'transform', this.data.vertical.enabled
                    ? "translate3d(0, " + this.directionSym + this.data.touchTransform + "%, 0)"
                    : "translate3d(" + this.directionSym + this.data.touchTransform + "%, 0, 0)");
            }
            else {
                this.setStyle(this.carouselInner, 'transform', this.data.vertical.enabled
                    ? "translate3d(0, " + this.directionSym + this.data.touchTransform + "px, 0)"
                    : "translate3d(" + this.directionSym + this.data.touchTransform + "px, 0px, 0px)");
            }
        }
        else {
            this.data.touchTransform = 0;
        }
    };
    /** this used to disable the scroll when it is not on the display */
    NguCarouselComponent.prototype.onWindowScrolling = function () {
        var top = this.carousel.offsetTop;
        var scrollY = window.scrollY;
        var heightt = window.innerHeight;
        var carouselHeight = this.carousel.offsetHeight;
        if (top <= scrollY + heightt - carouselHeight / 4 &&
            top + carouselHeight / 2 >= scrollY) {
            this.carouselIntervalEvent(0);
        }
        else {
            this.carouselIntervalEvent(1);
        }
    };
    /** store data based on width of the screen for the carousel */
    NguCarouselComponent.prototype.storeCarouselData = function () {
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_common__["isPlatformBrowser"])(this.platformId)) {
            this.data.deviceWidth = window.innerWidth;
        }
        else {
            this.data.deviceWidth = 1200;
        }
        this.data.carouselWidth = this.carouselMain.offsetWidth;
        if (this.data.type === 'responsive') {
            this.data.deviceType =
                this.data.deviceWidth >= 1200
                    ? 'lg'
                    : this.data.deviceWidth >= 992
                        ? 'md'
                        : this.data.deviceWidth >= 768 ? 'sm' : 'xs';
            this.data.items = this.userData.grid[this.data.deviceType];
            this.data.itemWidth = this.data.carouselWidth / this.data.items;
        }
        else {
            this.data.items = Math.trunc(this.data.carouselWidth / this.userData.grid.all);
            this.data.itemWidth = this.userData.grid.all;
            this.data.deviceType = 'all';
        }
        this.data.slideItems = +(this.userData.slide < this.data.items
            ? this.userData.slide
            : this.data.items);
        this.data.load =
            this.userData.load >= this.data.slideItems
                ? this.userData.load
                : this.data.slideItems;
        this.userData.speed =
            this.userData.speed || this.userData.speed > -1
                ? this.userData.speed
                : 400;
        this.initData.emit(this.data);
        this.carouselPoint();
    };
    /** Used to reset the carousel */
    NguCarouselComponent.prototype.reset = function () {
        this.carouselCssNode.innerHTML = '';
        this.moveTo(0);
        this.carouselPoint();
    };
    /** Init carousel point */
    NguCarouselComponent.prototype.carouselPoint = function () {
        // if (this.userData.point.visible === true) {
        var Nos = this.items.length - (this.data.items - this.data.slideItems);
        this.pointIndex = Math.ceil(Nos / this.data.slideItems);
        var pointers = [];
        if (this.pointIndex > 1 || !this.userData.point.hideOnSingleSlide) {
            for (var i = 0; i < this.pointIndex; i++) {
                pointers.push(i);
            }
        }
        this.pointNumbers = pointers;
        this.carouselPointActiver();
        if (this.pointIndex <= 1) {
            this.btnBoolean(1, 1);
        }
        else {
            if (this.data.currentSlide === 0 && !this.data.loop) {
                this.btnBoolean(1, 0);
            }
            else {
                this.btnBoolean(0, 0);
            }
        }
        this.buttonControl();
        // }
    };
    /** change the active point in carousel */
    NguCarouselComponent.prototype.carouselPointActiver = function () {
        var i = Math.ceil(this.data.currentSlide / this.data.slideItems);
        this.pointers = i;
    };
    /** this function is used to scoll the carousel when point is clicked */
    NguCarouselComponent.prototype.moveTo = function (index) {
        if (this.pointers !== index && index < this.pointIndex) {
            var slideremains = 0;
            var btns = this.data.currentSlide < index ? 1 : 0;
            if (index === 0) {
                this.btnBoolean(1, 0);
                slideremains = index * this.data.slideItems;
            }
            else if (index === this.pointIndex - 1) {
                this.btnBoolean(0, 1);
                slideremains = this.items.length - this.data.items;
            }
            else {
                this.btnBoolean(0, 0);
                slideremains = index * this.data.slideItems;
            }
            this.carouselScrollTwo(btns, slideremains, this.data.speed);
        }
    };
    /** set the style of the carousel based the inputs data */
    NguCarouselComponent.prototype.carouselSize = function () {
        this.data.token = this.generateID();
        var dism = '';
        this.styleSelector = "." + this.data.token + " > .ngucarousel > .ngucarousel-inner > .ngucarousel-items";
        if (this.userData.custom === 'banner') {
            this.renderer.addClass(this.carousel, 'banner');
        }
        if (this.userData.animation === 'lazy') {
            dism += this.styleSelector + " > .item {transition: transform .6s ease;}";
        }
        var itemStyle = '';
        if (this.data.vertical.enabled) {
            var itemWidth_xs = this.styleSelector + " > .item {height: " + this.data
                .vertical.height / +this.userData.grid.xs + "px}";
            var itemWidth_sm = this.styleSelector + " > .item {height: " + this.data
                .vertical.height / +this.userData.grid.sm + "px}";
            var itemWidth_md = this.styleSelector + " > .item {height: " + this.data
                .vertical.height / +this.userData.grid.md + "px}";
            var itemWidth_lg = this.styleSelector + " > .item {height: " + this.data
                .vertical.height / +this.userData.grid.lg + "px}";
            itemStyle = "@media (max-width:767px){" + itemWidth_xs + "}\n                    @media (min-width:768px){" + itemWidth_sm + "}\n                    @media (min-width:992px){" + itemWidth_md + "}\n                    @media (min-width:1200px){" + itemWidth_lg + "}";
        }
        else if (this.data.type === 'responsive') {
            var itemWidth_xs = this.userData.type === 'mobile'
                ? this.styleSelector + " .item {flex: 0 0 " + 95 /
                    +this.userData.grid.xs + "%}"
                : this.styleSelector + " .item {flex: 0 0 " + 100 /
                    +this.userData.grid.xs + "%}";
            var itemWidth_sm = this.styleSelector + " > .item {flex: 0 0 " + 100 /
                +this.userData.grid.sm + "%}";
            var itemWidth_md = this.styleSelector + " > .item {flex: 0 0 " + 100 /
                +this.userData.grid.md + "%}";
            var itemWidth_lg = this.styleSelector + " > .item {flex: 0 0 " + 100 /
                +this.userData.grid.lg + "%}";
            itemStyle = "@media (max-width:767px){" + itemWidth_xs + "}\n                    @media (min-width:768px){" + itemWidth_sm + "}\n                    @media (min-width:992px){" + itemWidth_md + "}\n                    @media (min-width:1200px){" + itemWidth_lg + "}";
        }
        else {
            itemStyle = this.styleSelector + " .item {flex: 0 0 " + this.userData.grid.all + "px}";
        }
        this.renderer.addClass(this.carousel, this.data.token);
        if (this.data.vertical.enabled) {
            this.renderer.addClass(this.carouselInner, 'nguvertical');
            this.renderer.setStyle(this.forTouch.nativeElement, 'height', this.data.vertical.height + "px");
        }
        // tslint:disable-next-line:no-unused-expression
        this.data.RTL &&
            !this.data.vertical.enabled &&
            this.renderer.addClass(this.carousel, 'ngurtl');
        this.createStyleElem(dism + " " + itemStyle);
    };
    /** logic to scroll the carousel step 1 */
    NguCarouselComponent.prototype.carouselScrollOne = function (Btn) {
        var itemSpeed = this.data.speed;
        var translateXval, currentSlide = 0;
        var touchMove = Math.ceil(this.data.dexVal / this.data.itemWidth);
        this.setStyle(this.carouselInner, 'transform', '');
        if (this.pointIndex === 1) {
            return;
        }
        else if (Btn === 0 &&
            ((!this.data.loop && !this.data.isFirst) || this.data.loop)) {
            var slide = this.data.slideItems * this.pointIndex;
            var currentSlideD = this.data.currentSlide - this.data.slideItems;
            var MoveSlide = currentSlideD + this.data.slideItems;
            this.btnBoolean(0, 1);
            if (this.data.currentSlide === 0) {
                currentSlide = this.items.length - this.data.items;
                itemSpeed = 400;
                this.btnBoolean(0, 1);
            }
            else if (this.data.slideItems >= MoveSlide) {
                currentSlide = translateXval = 0;
                this.btnBoolean(1, 0);
            }
            else {
                this.btnBoolean(0, 0);
                if (touchMove > this.data.slideItems) {
                    currentSlide = this.data.currentSlide - touchMove;
                    itemSpeed = 200;
                }
                else {
                    currentSlide = this.data.currentSlide - this.data.slideItems;
                }
            }
            this.carouselScrollTwo(Btn, currentSlide, itemSpeed);
        }
        else if (Btn === 1 &&
            ((!this.data.loop && !this.data.isLast) || this.data.loop)) {
            if (this.items.length <=
                this.data.currentSlide + this.data.items + this.data.slideItems &&
                !this.data.isLast) {
                currentSlide = this.items.length - this.data.items;
                this.btnBoolean(0, 1);
            }
            else if (this.data.isLast) {
                currentSlide = translateXval = 0;
                itemSpeed = 400;
                this.btnBoolean(1, 0);
            }
            else {
                this.btnBoolean(0, 0);
                if (touchMove > this.data.slideItems) {
                    currentSlide =
                        this.data.currentSlide +
                            this.data.slideItems +
                            (touchMove - this.data.slideItems);
                    itemSpeed = 200;
                }
                else {
                    currentSlide = this.data.currentSlide + this.data.slideItems;
                }
            }
            this.carouselScrollTwo(Btn, currentSlide, itemSpeed);
        }
        // cubic-bezier(0.15, 1.04, 0.54, 1.13)
    };
    /** logic to scroll the carousel step 2 */
    NguCarouselComponent.prototype.carouselScrollTwo = function (Btn, currentSlide, itemSpeed) {
        if (this.data.dexVal !== 0) {
            var val = Math.abs(this.data.touch.velocity);
            var somt = Math.floor(this.data.dexVal /
                val /
                this.data.dexVal *
                (this.data.deviceWidth - this.data.dexVal));
            somt = somt > itemSpeed ? itemSpeed : somt;
            itemSpeed = somt < 200 ? 200 : somt;
            this.data.dexVal = 0;
        }
        if (this.withAnim) {
            this.setStyle(this.carouselInner, 'transition', "transform " + itemSpeed + "ms " + this.userData.easing);
            this.userData.animation &&
                this.carouselAnimator(Btn, currentSlide + 1, currentSlide + this.data.items, itemSpeed, Math.abs(this.data.currentSlide - currentSlide));
        }
        else {
            this.setStyle(this.carouselInner, 'transition', "");
        }
        this.data.itemLength = this.items.length;
        this.transformStyle(currentSlide);
        this.data.currentSlide = currentSlide;
        this.onMove.emit(this.data);
        this.carouselPointActiver();
        this.carouselLoadTrigger();
        this.buttonControl();
        this.withAnim = true;
    };
    /** boolean function for making isFirst and isLast */
    NguCarouselComponent.prototype.btnBoolean = function (first, last) {
        this.data.isFirst = first ? true : false;
        this.data.isLast = last ? true : false;
    };
    NguCarouselComponent.prototype.transformString = function (grid, slide) {
        var collect = '';
        collect += this.styleSelector + " { transform: translate3d(";
        if (this.data.vertical.enabled) {
            this.data.transform[grid] =
                this.data.vertical.height / this.userData.grid[grid] * slide;
            collect += "0, -" + this.data.transform[grid] + "px, 0";
        }
        else {
            this.data.transform[grid] = 100 / this.userData.grid[grid] * slide;
            collect += "" + this.directionSym + this.data.transform[grid] + "%, 0, 0";
        }
        collect += "); }";
        return collect;
    };
    /** set the transform style to scroll the carousel  */
    NguCarouselComponent.prototype.transformStyle = function (slide) {
        var slideCss = '';
        if (this.data.type === 'responsive') {
            slideCss = "@media (max-width: 767px) {" + this.transformString('xs', slide) + "}\n      @media (min-width: 768px) {" + this.transformString('sm', slide) + " }\n      @media (min-width: 992px) {" + this.transformString('md', slide) + " }\n      @media (min-width: 1200px) {" + this.transformString('lg', slide) + " }";
        }
        else {
            this.data.transform.all = this.userData.grid.all * slide;
            slideCss = this.styleSelector + " { transform: translate3d(" + this.directionSym + this.data.transform.all + "px, 0, 0);";
        }
        this.carouselCssNode.innerHTML = slideCss;
    };
    /** this will trigger the carousel to load the items */
    NguCarouselComponent.prototype.carouselLoadTrigger = function () {
        if (typeof this.userData.load === 'number') {
            // tslint:disable-next-line:no-unused-expression
            this.items.length - this.data.load <=
                this.data.currentSlide + this.data.items &&
                this.carouselLoad.emit(this.data.currentSlide);
        }
    };
    /** generate Class for each carousel to set specific style */
    NguCarouselComponent.prototype.generateID = function () {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < 6; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return "ngucarousel" + text;
    };
    /** handle the auto slide */
    NguCarouselComponent.prototype.carouselInterval = function () {
        var _this = this;
        if (typeof this.userData.interval === 'number' && this.data.loop) {
            this.listener4 = this.renderer.listen(this.carouselMain, 'touchstart', function () {
                _this.carouselIntervalEvent(1);
            });
            this.listener5 = this.renderer.listen(this.carouselMain, 'touchend', function () {
                _this.carouselIntervalEvent(0);
            });
            this.listener6 = this.renderer.listen(this.carouselMain, 'mouseenter', function () {
                _this.carouselIntervalEvent(1);
            });
            this.listener7 = this.renderer.listen(this.carouselMain, 'mouseleave', function () {
                _this.carouselIntervalEvent(0);
            });
            this.listener8 = this.renderer.listen('window', 'scroll', function () {
                clearTimeout(_this.onScrolling);
                _this.onScrolling = setTimeout(function () {
                    _this.onWindowScrolling();
                }, 600);
            });
            this.carouselInt = setInterval(function () {
                // tslint:disable-next-line:no-unused-expression
                !_this.pauseCarousel && _this.carouselScrollOne(1);
            }, this.userData.interval);
        }
    };
    /** pause interval for specific time */
    NguCarouselComponent.prototype.carouselIntervalEvent = function (ev) {
        var _this = this;
        this.evtValue = ev;
        if (this.evtValue === 0) {
            clearTimeout(this.pauseInterval);
            this.pauseInterval = setTimeout(function () {
                // tslint:disable-next-line:no-unused-expression
                _this.evtValue === 0 && (_this.pauseCarousel = false);
            }, this.userData.interval);
        }
        else {
            this.pauseCarousel = true;
        }
    };
    /** animate the carousel items */
    NguCarouselComponent.prototype.carouselAnimator = function (direction, start, end, speed, length) {
        var _this = this;
        var val = length < 5 ? length : 5;
        val = val === 1 ? 3 : val;
        var itemList = this.items.toArray();
        if (direction === 1) {
            for (var i = start - 1; i < end; i++) {
                val = val * 2;
                itemList[i] &&
                    this.setStyle(itemList[i].nativeElement, 'transform', "translate3d(" + val + "px, 0, 0)");
            }
        }
        else {
            for (var i = end - 1; i >= start - 1; i--) {
                val = val * 2;
                itemList[i] &&
                    this.setStyle(itemList[i].nativeElement, 'transform', "translate3d(-" + val + "px, 0, 0)");
            }
        }
        setTimeout(function () {
            _this.items.forEach(function (elem) {
                return _this.setStyle(elem.nativeElement, 'transform', "translate3d(0, 0, 0)");
            });
        }, speed * 0.7);
    };
    /** control button for loop */
    NguCarouselComponent.prototype.buttonControl = function () {
        var arr = [];
        if (!this.data.loop || (this.data.isFirst && this.data.isLast)) {
            arr = [
                this.data.isFirst ? 'none' : 'block',
                this.data.isLast ? 'none' : 'block'
            ];
        }
        else {
            arr = ['block', 'block'];
        }
        this.setStyle(this.leftBtn, 'display', arr[0]);
        this.setStyle(this.rightBtn, 'display', arr[1]);
    };
    /** Short form for setElementStyle */
    NguCarouselComponent.prototype.setStyle = function (el, prop, val) {
        this.renderer.setStyle(el, prop, val);
    };
    /** For generating style tag */
    NguCarouselComponent.prototype.createStyleElem = function (datas) {
        var styleItem = this.renderer.createElement('style');
        if (datas) {
            var styleText = this.renderer.createText(datas);
            this.renderer.appendChild(styleItem, styleText);
        }
        this.renderer.appendChild(this.carousel, styleItem);
        return styleItem;
    };
    NguCarouselComponent.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"], args: [{
                    // tslint:disable-next-line:component-selector
                    selector: 'ngu-carousel',
                    template: "<div #ngucarousel class=\"ngucarousel\"><div #forTouch class=\"ngucarousel-inner\"><div #nguitems class=\"ngucarousel-items\"><ng-content select=\"[NguCarouselItem]\"></ng-content></div><div style=\"clear: both\"></div></div><ng-content select=\"[NguCarouselPrev]\"></ng-content><ng-content select=\"[NguCarouselNext]\"></ng-content></div><div #points *ngIf=\"data.point\"><ul class=\"ngucarouselPoint\"><li #pointInner *ngFor=\"let i of pointNumbers; let i = index\" [class.active]=\"i==pointers\" (click)=\"moveTo(i)\"></li></ul></div>",
                    styles: [
                        "\n    :host {\n      display: block;\n      position: relative;\n    }\n\n    :host.ngurtl {\n      direction: rtl;\n    }\n    \n    .nguvertical {\n      flex-direction: column;\n    }\n\n    .ngucarousel .ngucarousel-inner {\n      position: relative;\n      overflow: hidden;\n    }\n    .ngucarousel .ngucarousel-inner .ngucarousel-items {\n      position: relative;\n      display: flex;\n    }\n\n    .banner .ngucarouselPointDefault .ngucarouselPoint {\n      position: absolute;\n      width: 100%;\n      bottom: 20px;\n    }\n    .banner .ngucarouselPointDefault .ngucarouselPoint li {\n      background: rgba(255, 255, 255, 0.55);\n    }\n    .banner .ngucarouselPointDefault .ngucarouselPoint li.active {\n      background: white;\n    }\n    .banner .ngucarouselPointDefault .ngucarouselPoint li:hover {\n      cursor: pointer;\n    }\n\n    .ngucarouselPointDefault .ngucarouselPoint {\n      list-style-type: none;\n      text-align: center;\n      padding: 12px;\n      margin: 0;\n      white-space: nowrap;\n      overflow: auto;\n      box-sizing: border-box;\n    }\n    .ngucarouselPointDefault .ngucarouselPoint li {\n      display: inline-block;\n      border-radius: 50%;\n      background: rgba(0, 0, 0, 0.55);\n      padding: 4px;\n      margin: 0 4px;\n      transition-timing-function: cubic-bezier(0.17, 0.67, 0.83, 0.67);\n      transition: 0.4s;\n    }\n    .ngucarouselPointDefault .ngucarouselPoint li.active {\n      background: #6b6b6b;\n      transform: scale(1.8);\n    }\n    .ngucarouselPointDefault .ngucarouselPoint li:hover {\n      cursor: pointer;\n    }\n\n  "
                    ]
                },] },
    ];
    /** @nocollapse */
    NguCarouselComponent.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], },
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Renderer2"], },
        { type: __WEBPACK_IMPORTED_MODULE_3__ngu_carousel_service__["a" /* NguCarouselService */], },
        { type: Object, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"], args: [__WEBPACK_IMPORTED_MODULE_1__angular_core__["PLATFORM_ID"],] },] },
    ]; };
    NguCarouselComponent.propDecorators = {
        'userData': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"], args: ['inputs',] },],
        'reseter': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"], args: ['reset',] },],
        'carouselLoad': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"], args: ['carouselLoad',] },],
        'onMove': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"], args: ['onMove',] },],
        'initData': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"], args: ['initData',] },],
        'items': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ContentChildren"], args: [__WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["b" /* NguCarouselItemDirective */], { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'points': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChildren"], args: ['pointInner', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'next': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ContentChild"], args: [__WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["c" /* NguCarouselNextDirective */], { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'prev': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ContentChild"], args: [__WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["d" /* NguCarouselPrevDirective */], { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'carouselMain1': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['ngucarousel', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'carouselInner1': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['nguitems', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'carousel1': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['main', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'pointMain': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['points', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'forTouch': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['forTouch', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
    };
    return NguCarouselComponent;
}());

//# sourceMappingURL=ngu-carousel.component.js.map

/***/ }),

/***/ 791:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export NguCarouselStore */
/* unused harmony export ItemsControl */
/* unused harmony export Vertical */
/* unused harmony export NguButton */
/* unused harmony export Touch */
/* unused harmony export Transfrom */
/* unused harmony export NguCarousel */
var NguCarouselStore = /** @class */ (function () {
    function NguCarouselStore() {
    }
    return NguCarouselStore;
}());

var ItemsControl = /** @class */ (function () {
    function ItemsControl() {
    }
    return ItemsControl;
}());

var Vertical = /** @class */ (function () {
    function Vertical() {
    }
    return Vertical;
}());

var NguButton = /** @class */ (function () {
    function NguButton() {
    }
    return NguButton;
}());

var Touch = /** @class */ (function () {
    function Touch() {
    }
    return Touch;
}());

var Transfrom = /** @class */ (function () {
    function Transfrom() {
    }
    return Transfrom;
}());

var NguCarousel = /** @class */ (function () {
    function NguCarousel() {
    }
    return NguCarousel;
}());

//# sourceMappingURL=ngu-carousel.interface.js.map

/***/ }),

/***/ 792:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguItemComponent; });

var NguItemComponent = /** @class */ (function () {
    function NguItemComponent() {
        this.classes = true;
    }
    NguItemComponent.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                    selector: 'ngu-item',
                    template: "<ng-content></ng-content>",
                    styles: ["\n    :host {\n        display: inline-block;\n        white-space: initial;\n        vertical-align: top;\n    }\n  "]
                },] },
    ];
    /** @nocollapse */
    NguItemComponent.ctorParameters = function () { return []; };
    NguItemComponent.propDecorators = {
        'classes': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"], args: ['class.item',] },],
    };
    return NguItemComponent;
}());

//# sourceMappingURL=ngu-item.component.js.map

/***/ }),

/***/ 793:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguTileComponent; });

var NguTileComponent = /** @class */ (function () {
    function NguTileComponent() {
        this.classes = true;
    }
    NguTileComponent.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                    selector: 'ngu-tile',
                    template: "<div class=\"tile\"><ng-content></ng-content></div>",
                    styles: ["\n    :host {\n        display: inline-block;\n        white-space: initial;\n        padding: 10px;\n        box-sizing: border-box;\n        vertical-align: top;\n    }\n\n    .tile {\n        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\n    }\n\n    * {\n        box-sizing: border-box;\n    }\n  "]
                },] },
    ];
    /** @nocollapse */
    NguTileComponent.ctorParameters = function () { return []; };
    NguTileComponent.propDecorators = {
        'classes': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"], args: ['class.item',] },],
    };
    return NguTileComponent;
}());

//# sourceMappingURL=ngu-tile.component.js.map

/***/ }),

/***/ 796:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prismic_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_wow__ = __webpack_require__(302);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnsoComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var EnsoComponent = (function () {
    function EnsoComponent(prismic, route, titleService, _headerService, meta, wowService) {
        this.prismic = prismic;
        this.route = route;
        this.titleService = titleService;
        this._headerService = _headerService;
        this.meta = meta;
        this.wowService = wowService;
        this.PrismicDOM = __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default.a;
        this.toolbar = false;
        this.isMobile = false;
        this.wowService.init();
    }
    EnsoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeStream = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].fromPromise(this.prismic.buildContext())
            .subscribe(function (ctx) {
            _this.ctx = ctx;
            _this.fetchPage();
        });
        this.subscription = this._headerService.currentSection.subscribe(function (info) { return _this.sectionInfo = info; });
        this.dataSheetCarousel = {
            grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
            slide: 1,
            speed: 500,
            interval: 5000,
            point: {
                visible: false
            },
            load: 2,
            touch: true,
            loop: true,
            custom: 'banner'
        };
    };
    EnsoComponent.prototype.ngOnDestroy = function () {
        this.routeStream.unsubscribe();
        this.subscription.unsubscribe();
    };
    EnsoComponent.prototype.ngAfterViewChecked = function () {
        if (this.featureDetails && this.featureDetails.nativeElement && (this.sectionInfo.section !== '')) {
            this.scrollToSection(this.sectionInfo);
            this.sectionInfo = { section: '' };
        }
        else if (this.ctx && !this.toolbar) {
            // this.prismic.toolbar(this.ctx.api);
            this.toolbar = true;
            window.scrollTo(0, 0);
        }
    };
    EnsoComponent.prototype.scrollOffsetForFixedHeader = function () {
        var scrolledY = window.scrollY;
        if (scrolledY) {
            window.scroll(0, scrolledY - 160);
        }
    };
    EnsoComponent.prototype.scrollToSection = function (obj) {
        var _this = this;
        setImmediate(function () {
            if (obj.section === 'features') {
                _this.featureDetails.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
                _this.featureDetails.nativeElement.focus();
            }
            else if (obj.section === 'datasheet') {
                _this.datasheets.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
                _this.datasheets.nativeElement.focus();
            }
            else if (obj.section === 'technology') {
                _this.technology.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
                _this.technology.nativeElement.focus();
            }
            else if (obj.section === 'advantage') {
                _this.advantage.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
                _this.advantage.nativeElement.focus();
            }
            else if (obj.section === 'demo') {
                _this.demo.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
                _this.demo.nativeElement.focus();
            }
            else if (obj.section === 'why') {
                _this.whyElement.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
                _this.whyElement.nativeElement.focus();
            }
            _this.scrollOffsetForFixedHeader();
        });
    };
    EnsoComponent.prototype.fetchPage = function () {
        var _this = this;
        this.ctx.api.query([
            __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default.a.Predicates.any('document.type', ['summary', 'featuredetailed', 'download', 'diagram', 'advantages', 'technology', 'ensodemo', 'efficiency']),
        ])
            .then(function (response) {
            _this.toolbar = false;
            _this.pageContent = response.results;
            var metaDescription = "";
            var metaKeyWords = [];
            _this.pageContent.map(function (prop) {
                if (prop.uid === 'advantages') {
                    _this.advantagesData = prop.data;
                    metaKeyWords.push(_this.advantagesData['meta-title']);
                }
                else if (prop.uid === 'summary') {
                    _this.summaryData = prop.data.ensoplatform;
                    metaDescription = prop.data['meta-description'];
                    metaKeyWords.push(prop.data['meta-title']);
                }
                else if (prop.uid === 'diagram') {
                    _this.diagramData = prop.data;
                    metaKeyWords.push(_this.diagramData['meta-title']);
                }
                else if (prop.uid === 'datasheets') {
                    _this.dataSheetData = prop.data;
                    metaKeyWords.push(_this.dataSheetData['meta-title']);
                }
                else if (prop.uid === 'featuresdetailed') {
                    _this.featureDetailedData = prop.data;
                    metaKeyWords.push(_this.featureDetailedData['meta-title']);
                }
                else if (prop.uid === 'ensotechnology') {
                    _this.technologyData = prop.data;
                    metaKeyWords.push(_this.technologyData['meta-title']);
                }
                else if (prop.uid === 'demorequestform') {
                    _this.demoData = prop.data;
                    metaKeyWords.push(_this.demoData['meta-title']);
                }
                else if (prop.uid === 'efficiency') {
                    _this.efficiencyData = prop.data;
                    metaKeyWords.push(_this.efficiencyData['meta-title']);
                }
            });
            _this.meta.updateTag({ name: 'description', content: metaDescription });
            _this.meta.updateTag({ name: 'author', content: 'Xpms' });
            _this.meta.updateTag({ name: 'keywords', content: metaKeyWords.toString() });
        })
            .catch(function (e) { return console.log(e); });
    };
    EnsoComponent.prototype.myfunc = function (event) {
        // carouselLoad will trigger this funnction when your load value reaches
        // it is helps to load the data by parts to increase the performance of the app
        // must use feature to all carousel
    };
    return EnsoComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('whyElement'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], EnsoComponent.prototype, "whyElement", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('featureDetails'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], EnsoComponent.prototype, "featureDetails", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('datasheets'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object)
], EnsoComponent.prototype, "datasheets", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('technology'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _d || Object)
], EnsoComponent.prototype, "technology", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('advantage'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _e || Object)
], EnsoComponent.prototype, "advantage", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('demo'),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _f || Object)
], EnsoComponent.prototype, "demo", void 0);
EnsoComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-enso',
        template: __webpack_require__(813),
        styles: [__webpack_require__(805)]
    }),
    __metadata("design:paramtypes", [typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */]) === "function" && _m || Object])
], EnsoComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
//# sourceMappingURL=enso.component.js.map

/***/ }),

/***/ 805:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, ".customBackground{\n    background-size: contain;\n    background-position: center;\n    background-repeat: no-repeat;\n    margin-top: 100px;\n}\n\n.advantages{\n\tborder-top:4px solid #f7d761;\n\tpadding-top: 15px;\n}\n.Technologycontainer{\n\tbackground-color: #f7d761;\n}\n.Technologycontainer .wrapper{\n\tpadding-bottom: 50px;\n}\n.customImageWidth{\n\twidth: 70%;\n\tmargin: 0 auto;\n}\n.customEnsoList ul {\n    padding: 15px;\n}\n.customEnsoList li{\n\tcolor:#f7d761;\n\tfont-size: 15px;\n}\n.customEnsoList li span{\n\tcolor:black;\n}\n.features{\n\tmargin-bottom:30px;\n}\n.dataSheetsBg{\n\tbackground-color: #f7d761;\n    -webkit-clip-path: polygon(0px 8%, 1600px 0%, 1600px 100%, 0px 100%);\n            clip-path: polygon(0px 8%, 1600px 0%, 1600px 100%, 0px 100%);\n}\n.dataSheets{\n\t-webkit-clip-path: polygon(0px 60px, 1600px 0px, 1600px 100%, 0px 100%);\n\t        clip-path: polygon(0px 60px, 1600px 0px, 1600px 100%, 0px 100%);\n    background-size: cover;\n    background-position-y: -200px;\n}\n.zeroMargin{\n\tmargin-top:0px;\n}\n\n.subHeading{\n    font-size: 20px;\n    font-weight: 300;\n    color: #9b9b9b;\n}\n\n.technology-subtitle{\n    margin:15px 0 0 0;\n    min-height: 40px;\n    text-align: center;\n}\n.headings{\n    font-size:40px;\n    color:#4a4a4a;\n    font-weight: bold;\n    text-align: center;\n\tmargin: 60px 0px 60px 0px;\n\ttext-transform: uppercase;\n}\n.headings2{\n    font-size:40px;\n    color:#4a4a4a;\n    font-weight: bold;\n    text-align: center;\n    text-transform: uppercase;\n}\n.headings3{\n    font-size:30px;\n    color:#4a4a4a;\n    font-weight: bold;\n\tmargin: 40px 0;\n    text-transform: uppercase;\n}\n.image-style{\n    width: 80px;\n}\n.sectionTop{\n    margin-top: 100px;\n}\n.btn-enso{\n    border: 1px solid #25aae1;\n    border-radius: 28px;\n    color: #25aae1;\n    text-transform: uppercase;\n    background-color: white;\n    margin-left: 25px;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n.product-slider { padding: 45px; }\n\n.no-padding{\n  padding-left:0px;\n  padding-right:0px;\n}\n\n.btn-download {\n    border-radius: 20px;\n    margin-bottom: 15px;\n    background-color: #ffffff;\n    box-shadow: 0 1px 2px 0 rgba(58, 21, 21, 0.5);\n    text-transform: uppercase;\n\theight: 40px;\n\tpadding: 6px 20px;\n}\n/* Efficiency */\n.efficiency{\n    background-position: right 50px;\n    background-repeat: no-repeat;\n\tbackground-size: 40%;\n    background-color: #f4f4f4;\n}\n.sectionTop2{\n    margin-top:70px;\n}\n.bottomMargin{\n    margin-bottom: 100px;\n}\n\n.btn-custom {\n    border-radius: 30px;\n    background-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n    color: white;\n    text-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n\n.iconStyle{\n  width: 80%;\n    margin: 0 auto;\n}\n\n.iconStyle2{\n  width: 60%;\n  margin: 0 auto;\n}\n.iconStyle{\n\twidth: 50px;\n\theight: 50px;\n}\n.iconStyle3 {\n    width: 50%;\n    margin: 0 auto;\n}\n.customEnsoList ul {\n    padding: 15px;\n    list-style-type:square;\n}\n.customEnsoList li{\n\tcolor:#f7d761;\n\tfont-size: 15px;\n}\n.customEnsoList li span{\n\tcolor:black;\n\tfont-size: 14px;\n}\n.enso-feature-subheading {\n\tmargin: 10px 0 20px 0;\n}\n.custom-top {\n    margin-top: 15px;\n}\n\n.enso-placeholder {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.diagram-section {\n    padding-top: 50px;\n    padding-bottom: 50px;\n}\n.img-technology {\n    width: 50%;\n    margin: 0 auto;\n}\n.demoForm{\n    border: none;\n    border-radius: 0px;\n    background-color: transparent;\n    border-bottom: 1px solid #aeaeae;\n    box-shadow: none;\n}\n.demobtn{\n  background-color:#f7d761;\n  border-radius:0px;\n  font-weight:bold;\n}\n.demoHeight{\n  height:550px;\n}\n.feature-label {\n    display: block;\n    margin-top: 10px; \n}\n@media screen and (max-width: 600px){\n  .customMargin{\n    margin-top:20px;\n  }\n  .customBackground{\n    margin-top:30px;\n  }\n  .headings{\n      font-size:20px;\n  }\n  .img-technology{\n    width:100%;\n  }\n  .efficiency{\n\n    background-color: #f4f4f4;\n  }\n  .demoHeight{\n    height:450px;\n  }\n}\n@media screen and (min-width: 768px) {\n    .row-eq-height {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n    }\n    .iconStyle2 {\n        width: 80px;\n        height: 80px;\n    }\n    .enso-logo {\n        width: 210px;\n    }\n}\n@media screen and (min-width: 992px) {\n    .advantages-left-wrapper {\n        padding-right: 30px;\n    }\n    .advantages-right-wrapper {\n        padding-left: 30px;\n    }\n    \n}\n@media screen and (min-width: 1200px) {\n\t.summary-icon {\n\t\twidth: 12%;\n    }    \n\t.leftRs {\n\t\tleft: -30px;\n\t}\n\t.rightRs {\n\t\tright: -30px;\n\t}\n}\n@media screen and (max-width: 767px) {\n    .product-slider { padding: 15px; }\n    .headings2,.headings3 {\n        font-size: 20px;\n    }\n    .btn-download {\n        font-size: 12px;\n    }\n    .enso-logo {\n        width: 150px;\n        height: 45px;\n    }\n    .subHeading {\n        font-size: 14px;\n        display: block;\n    }\n    .btn-custom {\n        font-size: 12px;\n    }\n    .iconStyle2 {\n        width: 50%;\n    }\n}\n.row{\n\tmargin-left:0px;\n\tmargin-right:0px;\n}\n.carousel-section {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.carousel-section img {\n    width: 50%;\n}\n.bannerStyle h1 {\n    background-color: #ccc;\n    min-height: 300px;\n    text-align: center;\n    line-height: 300px;\n}\n.leftRs {\n    position: absolute;\n    top: 30%;\n    bottom: 0;\n    width: 50px;\n    height: 50px;\n    border: none;\n    padding: 0;\n    box-shadow: none;\n    border-radius: 50%;\n    left: 0;\n    background: transparent;\n}\n\n.leftRs img , .rightRs img{\n    width: 50px;\n    height: 50px;\n}\n.rightRs {\n    position: absolute;\n    top: 30%;\n    bottom: 0;\n    width: 50px;\n    height: 50px;\n    border: none;\n    padding: 0;\n    box-shadow: none;\n    border-radius: 50%;\n    right: 0;\n    background: transparent;\n}\n.btn-enso img, .btn-custom img {\n\tmargin-top: -3px;\n\tmargin-left: 3px;\n\theight: 12px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 813:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!pageContent\" class=\"container content-spinner\">\n\t<div class=\"col-md-12 loader\">\n\t\t<img class=\"\" src=\"../../../assets/images/content_spinner.gif\"/>\n\t</div>\n</div>\n<div *ngIf=\"pageContent\" [attr.data-wio-id]=\"pageContent.id\">\n\t<div #why class=\"row row-eq-height customBackground\" [ngStyle]=\"{'background-image': 'url(' + summaryData[0].summarybackground.url + ')'}\" #whyElement>\n\t\t<div class=\"col-lg-7 col-md-7 text-center hidden-sm hidden-xs enso-placeholder wow fadeInLeft\">\n\t\t\t\t<img alt={{summaryData[0].ensoplatformplaceholder.alt}} src={{summaryData[0].ensoplatformplaceholder.url}} class=\"img-responsive\"/>\n\t\t\t</div>\n\t\t<div class=\"col-lg-5 col-md-5 hidden-sm hidden-xs wow fadeInRight\">\n\t\t\t<img class=\"enso-logo\" alt={{summaryData[0].ensologo.alt}} src={{summaryData[0].ensologo.url}}/>\n\t\t\t<br>\n\t\t\t<label class=\"subHeading\">{{summaryData[0].ensotitle[0].text}}</label>\n\t\t\t<div class=\"row custom-top\">\n\t\t\t\t<div class=\"col-lg-10 col-sm-10 col-xs-10 col-md-10 no-padding\">\n\t\t\t\t\t<div>\n            <b>{{summaryData[0].ensoheading[0].text}}</b>\n          </div>\n\t\t\t\t\t<div class=\"custom-top\">{{summaryData[0].ensodata[0].text}}</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row custom-top\">\n\t\t\t\t<div class=\"col-lg-10 col-sm-10 col-xs-10 col-md-10 no-padding\">\n\t\t\t\t\t<div class=\"enso-feature-subheading\">\n            <b>{{summaryData[0].ensosubheading[0].text}}</b>\n          </div>\n\t\t\t\t\t<div class=\"custom-top\">\n            <div class=\"row\">\n              <div class=\"col-lg-2 col-sm-3 col-md-2 col-xs-2 no-padding summary-icon\">\n                <img alt={{summaryData[0].ensosubheadingicon1.alt}} src={{summaryData[0].ensosubheadingicon1.url}} class=\"img-responsive iconStyle wow fadeInUp\" data-wow-delay=\"0.6s\"/>\n              </div>\n              <div class=\"col-lg-10 col-md-10 col-sm-9 col-xs-10\">\n                <b>{{summaryData[0].ensosubheadingtitle1[0].text}}</b>\n                {{summaryData[0].ensosubheadingdata[0].text}}\n              </div>\n            </div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"custom-top\">\n            <div class=\"row\">\n              <div class=\"col-lg-2 col-sm-3 col-md-2 col-xs-2 no-padding summary-icon\">\n                <img alt={{summaryData[0].ensosubheadingicon2.alt}} src={{summaryData[0].ensosubheadingicon2.url}} class=\"img-responsive iconStyle wow fadeInUp\" data-wow-delay=\"0.6s\"/>\n              </div>\n              <div class=\"col-lg-10 col-md-10 col-sm-9 col-xs-10\">\n                <b>{{summaryData[0].ensosubheadingtitle2[0].text}}</b>\n                {{summaryData[0].ensosubheadingdata2[0].text}}\n              </div>\n            </div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"custom-top\">\n            <div class=\"row\">\n              <div class=\"col-lg-2 col-sm-3 col-md-2 col-xs-2 no-padding summary-icon\">\n                <img alt={{summaryData[0].ensosubheadingicon3.alt}} src={{summaryData[0].ensosubheadingicon3.url}} class=\"img-responsive iconStyle  wow fadeInUp\" data-wow-delay=\"0.6s\"/>\n              </div>\n              <div class=\"col-lg-10 col-md-10 col-sm-9 col-xs-10\">\n                <b>{{summaryData[0].ensosubheadingtitle3[0].text}}</b>\n                {{summaryData[0].ensosubheadingdata3[0].text}}\n              </div>\n            </div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"custom-top\">\n            <div class=\"row\">\n              <div class=\"col-lg-2 col-sm-3 col-md-2 col-xs-2 no-padding summary-icon\">\n                <img alt={{summaryData[0].ensosubheadingicon4.alt}} src={{summaryData[0].ensosubheadingicon4.url}} class=\"img-responsive iconStyle wow fadeInUp\" data-wow-delay=\"0.6s\"/>\n              </div>\n              <div class=\"col-lg-10 col-md-10 col-sm-9 col-xs-10\">\n                <b>{{summaryData[0].ensosubheadingtitle4[0].text}}</b>\n                {{summaryData[0].ensosubheading4[0].text}}\n              </div>\n            </div>\n\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"custom-top\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-sm-12 no-padding\" style=\"margin-top: 20px;\">\n\t\t\t\t\t\t\t\t\t<button class=\"btn btn-custom\">\n\t\t\t\t\t\t\t\t\t\t{{summaryData[0].requestdemobtn[0].text}}\n\t\t\t\t\t\t\t\t\t\t<img src=\"../../../assets/images/right-arrow.png\" />\n\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t<!-- <button class=\"btn btn-enso\">\n\t\t\t\t\t\t\t\t\t\t{{summaryData[0].demobtn[0].text}}\t\t\t\n\t\t\t\t\t\t\t\t\t\t<img src=\"../../../assets/images/right-arrow.png\" />\n\t\t\t\t\t\t\t\t\t</button> -->\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n    <div class=\"col-sm-12 col-xs-12 hidden-lg hidden-md text-center\">\n\t\t\t\t<img class=\"enso-logo\" alt={{summaryData[0].ensologo.alt}} src={{summaryData[0].ensologo.url}}/>\n        <label class=\"subHeading\">{{summaryData[0].ensotitle[0].text}}</label>\n        <img alt={{summaryData[0].ensoplatformplaceholder.url}} src={{summaryData[0].ensoplatformplaceholder.url}} class=\"img-responsive\" style=\"margin-top:20px;\"/>\n\t\t\t\t<button class=\"btn btn-custom\">\n\t\t\t\t\t{{summaryData[0].requestdemobtn[0].text}}\n\t\t\t\t\t<img src=\"../../../assets/images/right-arrow.png\" />\n\t\t\t\t</button>\n\t\t\t\t<!-- <button class=\"btn btn-enso  hidden-sm hidden-xs\">\n\t\t\t\t\t{{summaryData[0].demobtn[0].text}}\t\t\t\n\t\t\t\t\t<img src=\"../../../assets/images/right-arrow.png\" />\n\t\t\t\t</button> -->\n\t\t</div>\n\t\t<div class=\"col-sm-12 col-xs-12 hidden-lg hidden-md customMargin\">\n\t\t\t<br>\n\t\t\t<div class=\"row custom-top\">\n\t\t\t\t<div class=\"col-lg-12 col-sm-12 col-xs-12 col-md-12 no-padding\">\n\t\t\t\t\t<div>\n            <b>{{summaryData[0].ensoheading[0].text}}</b>\n          </div>\n\t\t\t\t\t<div class=\"custom-top\">{{summaryData[0].ensodata[0].text}}</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row custom-top\">\n\t\t\t\t<div class=\"col-lg-12 col-sm-12 col-xs-12 col-md-12 no-padding\">\n\t\t\t\t\t<div>\n            <b>{{summaryData[0].ensosubheading[0].text}}</b>\n          </div>\n\t\t\t\t\t<div class=\"custom-top\">\n            <div class=\"row\">\n              <div class=\"col-lg-2 col-sm-2 col-md-2 col-xs-2 no-padding\">\n                <img alt={{summaryData[0].ensosubheadingicon1.alt}} src={{summaryData[0].ensosubheadingicon1.url}} class=\"img-responsive iconStyle wow fadeInUp\" data-wow-delay=\"0.6s\"/>\n              </div>\n              <div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-10\">\n                <b>{{summaryData[0].ensosubheadingtitle1[0].text}}</b>\n                {{summaryData[0].ensosubheadingdata[0].text}}\n              </div>\n            </div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"custom-top\">\n            <div class=\"row\">\n              <div class=\"col-lg-2 col-sm-2 col-md-2 col-xs-2 no-padding\">\n                <img alt={{summaryData[0].ensosubheadingicon2.alt}} src={{summaryData[0].ensosubheadingicon2.url}} class=\"img-responsive iconStyle wow fadeInUp\" data-wow-delay=\"0.6s\"/>\n              </div>\n              <div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-10\">\n                <b>{{summaryData[0].ensosubheadingtitle2[0].text}}</b>\n                {{summaryData[0].ensosubheadingdata2[0].text}}\n              </div>\n            </div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"custom-top\">\n            <div class=\"row\">\n              <div class=\"col-lg-2 col-sm-2 col-md-2 col-xs-2 no-padding\">\n                <img alt={{summaryData[0].ensosubheadingicon3.alt}} src={{summaryData[0].ensosubheadingicon3.url}} class=\"img-responsive iconStyle wow fadeInUp\" data-wow-delay=\"0.6s\"/>\n              </div>\n              <div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-10\">\n                <b>{{summaryData[0].ensosubheadingtitle3[0].text}}</b>\n                {{summaryData[0].ensosubheadingdata3[0].text}}\n              </div>\n            </div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"custom-top\">\n            <div class=\"row\">\n              <div class=\"col-lg-2 col-sm-2 col-md-2 col-xs-2 no-padding \">\n                <img alt={{summaryData[0].ensosubheadingicon4.alt}} src={{summaryData[0].ensosubheadingicon4.url}} class=\"img-responsive iconStyle wow fadeInUp\" data-wow-delay=\"0.6s\"/>\n              </div>\n              <div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-10\">\n                <b>{{summaryData[0].ensosubheadingtitle4[0].text}}</b>\n                {{summaryData[0].ensosubheading4[0].text}}\n              </div>\n            </div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<section #advantage class=\"Advantagecontainer\">\n\t\t<h3 class=\"headings\">{{advantagesData.title[0].text}}</h3>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"container no-padding\">\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 advantages-left-wrapper\">\n\t\t\t\t\t<div class=\"row advantages wow fadeInRight\">\n\t\t\t\t\t\t<div class=\"col-lg-2 col-md-2 hidden-xs hidden-sm pr-0\">\n\t\t\t\t\t\t\t<img height=\"90px\" width=\"90px\" alt={{advantagesData.subheadingicon1.alt}} src={{advantagesData.subheadingicon1.url}} class=\"img-responsive\"/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-10 col-md-10 col-xs-12 col-sm-12\">\n\t\t\t\t\t\t\t<label>{{advantagesData.subheading[0].text}}</label>\n\t\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t\t{{advantagesData.subheadingdata[0].text}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 advantages-right-wrapper customMargin\">\n\t\t\t\t\t<div class=\"row advantages wow fadeInRight\">\n\t\t\t\t\t\t<div class=\"col-lg-2 col-md-2 hidden-xs hidden-sm pr-0\">\n\t\t\t\t\t\t\t<img height=\"75px\" width=\"75px\" alt={{advantagesData.subheadingicon2.alt}} src={{advantagesData.subheadingicon2.url}} class=\"img-responsive\"/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-10 col-md-10 col-xs-12 col-sm-12\">\n\t\t\t\t\t\t\t<label>{{advantagesData.subheading2[0].text}}</label>\n\t\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t\t{{advantagesData.subheadingdata2[0].text}}\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n    <br>\n\t\t<div class=\"container-fluid text-center hidden-sm hidden-xs diagram-section\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<img alt={{diagramData.platformdiagram.alt}} src={{diagramData.platformdiagram.url}} class=\"iconStyle3 img-responsive\"/>\n\t\t\t</div>\n\t\t</div>\n\t</section>\n  <br>\n\t<section class=\"Technologycontainer\" #technology>\n\t\t<div class=\"row wow fadeInUp\">\n\t\t\t<div class=\"col-lg-12 col-sm-12 col-xs-12 col-sm-12 text-center wrapper\">\n\t\t\t\t<h3 class=\"headings\">{{technologyData.title[0].text}}</h3>\n\t\t\t\t<div>\n\t\t\t\t\t{{technologyData.subtitle[0].text}}\n\t\t\t\t</div>\n\t\t\t\t<br>\n\t\t\t\t<img alt={{technologyData.techstructure.alt}} src={{technologyData.techstructure.url}} class=\"img-responsive img-technology\"/>\n\t\t\t</div>\n\t\t</div>\n\t\t<br/>\n\t</section>\n\t<div #featureDetails>\n\t\t<section class=\"container no-padding hidden-sm hidden-xs\">\n\t\t\t<h3 class=\"headings\">{{featureDetailedData.title[0].text}}</h3>\n\t\t\t<div class=\"row features\">\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 advantages-left-wrapper wow fadeInLeft\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-3 no-padding text-left\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2 wow fadeInUp\" data-wow-delay=\"0.6s\" alt=\"{{featureDetailedData.extensibilityicon.alt}}\" src=\"{{featureDetailedData.extensibilityicon.url}}\"/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-9 customEnsoList\">\n\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.extensibilitytitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<span>{{featureDetailedData.extensibilitydata[0].text}}</span>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.extensibilitydatalist1[0].text}}</span></li>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.extensibilitydatalist2[0].text}}</span></li>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.extensibilitydatalist3[0].text}}</span></li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 advantages-right-wrapper wow fadeInRight\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-3 no-padding text-left\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2 wow fadeInUp\" data-wow-delay=\"0.6s\" alt={{featureDetailedData.cognitiveicon.alt}} src={{featureDetailedData.cognitiveicon.url}}/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-9 customEnsoList\">\n\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.cognitivetitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<span>{{featureDetailedData.cognitivedata[0].text}}</span>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.cognitivedatalist1[0].text}}</span></li>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.cognitivedatalist2[0].text}}</span></li>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.cognitivedatalist3[0].text}}</span></li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row features\">\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInLeft\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-3 no-padding text-left\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2 wow fadeInUp\"  data-wow-delay=\"0.6s\" alt={{featureDetailedData.vocabularyicon.alt}} src={{featureDetailedData.vocabularyicon.url}}/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-9 customEnsoList\">\n\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.vocabulurytitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<span>{{featureDetailedData.vocubularydata[0].text}}</span>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.vocabulurydatalist1[0].text}}</span></li>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.vocabulurydatalist2[0].text}}</span></li>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.vocabulurydatalist3[0].text}}</span></li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInRight\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-3 no-padding text-left\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2 wow fadeInUp\"  data-wow-delay=\"0.6s\" alt={{featureDetailedData.serviceicon.alt}} src={{featureDetailedData.serviceicon.url}}/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-9 customEnsoList\">\n\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.servicetitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<span>{{featureDetailedData.servicedata[0].text}}</span>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.servicedatalist1[0].text}}</span></li>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.servicedatalist2[0].text}}</span></li>\n\t\t\t\t\t\t\t\t<li><span>{{featureDetailedData.servicedatalist3[0].text}}</span></li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row features\">\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInLeft\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-3 no-padding text-left\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2 wow fadeInUp\"  data-wow-delay=\"0.6s\" alt={{featureDetailedData.learningicon.alt}} src={{featureDetailedData.learningicon.url}}/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-9 customEnsoList\">\n\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.learningtile[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<span>{{featureDetailedData.learningdata[0].text}}</span>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<span>{{featureDetailedData.learningdatalist1[0].text}}</span>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<span>{{featureDetailedData.learningdatalist2[0].text}}</span>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInRight\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-3 no-padding text-left\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2 wow fadeInUp\" data-wow-delay=\"0.6s\" alt={{featureDetailedData.casemanagementicon.alt}} src={{featureDetailedData.casemanagementicon.url}}/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-9 customEnsoList\">\n\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.casemanagementtitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<span>{{featureDetailedData.casemanagementdata[0].text}}</span>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<span>{{featureDetailedData.casemanagementdatalist1[0].text}}</span>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<span>{{featureDetailedData.casemanagementdatalist2[0].text}}</span>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<span>{{featureDetailedData.casemanagementdatalist3[0].text}}</span>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\t\t<section class=\"container no-padding hidden-lg hidden-md wow fadeInUp\">\n\t\t\t<h3 class=\"headings\">{{featureDetailedData.title[0].text}}</h3>\n\t\t\t<div class=\"row features\">\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-center\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2\" alt=\"{{featureDetailedData.extensibilityicon.alt}}\" src=\"{{featureDetailedData.extensibilityicon.url}}\"/>\n\t\t\t\t\t\t\t<label class=\"feature-label\">\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.extensibilitytitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-center\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2\" alt={{featureDetailedData.cognitiveicon.alt}} src={{featureDetailedData.cognitiveicon.url}}/>\n\t\t\t\t\t\t\t<label class=\"feature-label\">\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.cognitivetitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row features\">\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-center\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2\" alt={{featureDetailedData.vocabularyicon.alt}} src={{featureDetailedData.vocabularyicon.url}}/>\n\t\t\t\t\t\t\t<label class=\"feature-label\">\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.vocabulurytitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-center\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2\" alt={{featureDetailedData.serviceicon.alt}} src={{featureDetailedData.serviceicon.url}}/>\n\t\t\t\t\t\t\t<label class=\"feature-label\">\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.servicetitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row features\">\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-center\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2\" alt={{featureDetailedData.learningicon.alt}} src={{featureDetailedData.learningicon.url}}/>\n\t\t\t\t\t\t\t<label class=\"feature-label\">\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.learningtile[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-center\">\n\t\t\t\t\t\t\t<img class=\"iconStyle2\" alt={{featureDetailedData.casemanagementicon.alt}} src={{featureDetailedData.casemanagementicon.url}}/>\n\t\t\t\t\t\t\t<label class=\"feature-label\">\n\t\t\t\t\t\t\t\t<b>{{featureDetailedData.casemanagementtitle[0].text}}</b>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n\t\n\t<div class=\"row dataSheetsBg\" #datasheets >\n\t\t<div class=\"col-lg-12 col-sm-12 col-xs-12 col-md-12 no-padding \">\n\t\t\t<div class=\"dataSheets\" [ngStyle]=\"{'background-image': 'url(' + dataSheetData.downloadbg.url + ')'}\">\n\t\t\t\t<div class=\"row  wow fadeInUp\">\n\t\t\t\t\t<div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 no-padding\">\n\t\t\t\t\t\t<h3 class=\"headings2 sectionTop\">{{dataSheetData.datasheets[0].text}}</h3>\n\t\t\t\t\t\t<div class=\"product-slider\">\n\t\t\t\t\t\t\t<ngu-carousel\n\t\t\t\t\t\t\t\t[inputs]=\"dataSheetCarousel\">\n\t\t\t\t\t\t\t\t<ngu-item NguCarouselItem *ngFor=\"let c1 of dataSheetData.datasheets1\">\n\t\t\t\t\t\t\t\t\t<div class=\"carousel-section\">\n\t\t\t\t\t\t\t\t\t\t<img alt={{c1.datasheetimage.alt}} src={{c1.datasheetimage.url}}/>\n\t\t\t\t\t\t\t\t\t\t<p class=\"technology-subtitle\">{{c1.datasheettext[0].text}}</p>\n\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-download\">{{dataSheetData.downloadlabel[0].text}}</button>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</ngu-item>\n\t\t\t\t\t\t\t\t<button NguCarouselPrev class='leftRs'>\n\t\t\t\t\t\t\t\t\t<img src=\"../../assets/images/left-chevron.png\" class=\"image-style\"/>\n\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t<button NguCarouselNext class='rightRs'>\n\t\t\t\t\t\t\t\t\t<img src=\"../../assets/images/right-chevron.png\" class=\"image-style\"/>\n\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t</ngu-carousel>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<section class=\"efficiency demoHeight\" #demo [ngStyle]=\"{'background-image': 'url(' + efficiencyData.background.url + ')'}\">\n\t\t<div class=\"row wow fadeInUp\">\n      <div class=\"container\">\n        <div class=\"col-lg-6 col-sm-12 col-md-6 col-sm-12\">\n        <h3 class=\"headings3\">{{demoData.maintitle[0].text}}</h3>\n        <p>{{demoData.description[0].text}}</p>\n        <br>\n        <form>\n          <div class=\"input-group features\">\n              <div class=\"input-group-btn\">\n                <button type=\"button\" class=\"btn btn-default demobtn\">{{demoData.namelabel[0].text}}</button>\n              </div>\n              <input class=\"form-control demoForm\" placeholder=\"Name\">\n          </div>\n          <div class=\"input-group features\">\n              <div class=\"input-group-btn\">\n                <button type=\"button\" class=\"btn btn-default demobtn\">{{demoData.emaillabel[0].text}}</button>\n              </div>\n              <input class=\"form-control demoForm\" placeholder=\"Email\">\n          </div>\n          <div class=\"col-sm-12 col-xs-12 hidden-lg hidden-md\" style=\"margin-top: 20px;\">\n\n          </div>\n          <div class=\"col-lg-12 col-md-12 no-padding hidden-sm hidden-xs\" style=\"margin-top: 20px;\">\n\t\t\t\t\t\t<button class=\"btn btn-custom\">\n\t\t\t\t\t\t\t{{demoData.demobtn[0].text}}\n\t\t\t\t\t\t</button>\n          </div>\n        </form>\n      </div>\n      </div>\n    </div>\n\t</section>\n</div>\t\n\n"

/***/ })

});
//# sourceMappingURL=1.chunk.js.map