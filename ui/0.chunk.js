webpackJsonp([0,13],{

/***/ 776:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_component__ = __webpack_require__(795);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngu_carousel__ = __webpack_require__(788);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_useful_swiper__ = __webpack_require__(803);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_useful_swiper___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_useful_swiper__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_wow__ = __webpack_require__(302);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: "",
        component: __WEBPACK_IMPORTED_MODULE_2__dashboard_component__["a" /* DashboardComponent */]
    }
];
var DashboardModule = (function () {
    function DashboardModule() {
    }
    return DashboardModule;
}());
DashboardModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_5_angular2_useful_swiper__["SwiperModule"], __WEBPACK_IMPORTED_MODULE_4__ngu_carousel__["a" /* NguCarouselModule */], __WEBPACK_IMPORTED_MODULE_7_ngx_wow__["a" /* NgwWowModule */], __WEBPACK_IMPORTED_MODULE_6__angular_router__["a" /* RouterModule */].forChild(routes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__dashboard_component__["a" /* DashboardComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_3__prismic_prismic_service__["a" /* PrismicService */]]
    })
], DashboardModule);

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ 784:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, "/* Efficiency */\n.efficiency{\n    background-position: right 50px;\n    background-repeat: no-repeat;\n\tbackground-size: 40%;\n    background-color: #0c3445;\n        padding-top: 50px;\n    padding-bottom: 50px;\n}\n.efficiencyText{\n    font-size: 30px;\n    color: white;\n\tfont-weight: bold;\n\tline-height: 1.2;\n}\n.efficiencyStatement{\n\tfont-size: 18px;\n  \tfont-weight: 500;\n\tcolor: #4a4a4a;\n\tline-height: 1.5;\n}\n.sectionTop-efficiency{\n    margin-top:70px;\n}\n.bottomMargin{\n    margin-bottom: 100px;\n}\n.btn-custom {\n\tborder-radius: 30px;\n\tbackground-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n\tcolor: white;\n\ttext-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n\n.btn-custom:active, .btn-custom:focus {\n    outline: none;\n}\n/* Efficiency CSS */\n@media screen and (max-width: 767px){\n\t.efficiency{\n\t\tbackground-position: 50px 20px;\n\t\tbackground-size: 100%;\n\t}\n\t.efficiencyText{\n        font-size: 18px;\n\t\tpadding: 0 15px;\n\t\tmargin-top: 50px;\n    }\n    .efficiencyStatement{\n\t\tfont-size: 14px;\n\t\tmargin-top: 50px;\n        padding: 0 15px;\n\t}\n\t.bottomMargin{\n\t\tmargin-bottom: 50px;\n\t}\n\t.sectionTop-efficiency{\n\t\tmargin-top:50px;\n\t}\n\t.efficiency .btn-custom {\n\t\tfont-size: 14px;\n\t}\n}\n", ""]);

// exports


/***/ }),

/***/ 785:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguCarouselService; });


var NguCarouselService = /** @class */ (function () {
    function NguCarouselService() {
        this.getData = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
    }
    NguCarouselService.prototype.reset = function (token) {
        this.getData.next({ id: token, ref: 'reset' });
    };
    NguCarouselService.prototype.moveToSlide = function (token, index, animate) {
        this.getData.next({
            id: token,
            ref: 'moveToSlide',
            index: index,
            animation: animate
        });
    };
    NguCarouselService.prototype.destroy = function (token) {
        this.getData.next({ id: token, ref: 'destroy' });
    };
    NguCarouselService.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    /** @nocollapse */
    NguCarouselService.ctorParameters = function () { return []; };
    return NguCarouselService;
}());

//# sourceMappingURL=ngu-carousel.service.js.map

/***/ }),

/***/ 786:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return NguCarouselItemDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return NguCarouselNextDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return NguCarouselPrevDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguCarouselPointDirective; });

var NguCarouselItemDirective = /** @class */ (function () {
    function NguCarouselItemDirective() {
    }
    NguCarouselItemDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{
                    // tslint:disable-next-line:directive-selector
                    selector: '[NguCarouselItem]'
                },] },
    ];
    /** @nocollapse */
    NguCarouselItemDirective.ctorParameters = function () { return []; };
    return NguCarouselItemDirective;
}());

var NguCarouselNextDirective = /** @class */ (function () {
    function NguCarouselNextDirective() {
    }
    NguCarouselNextDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{
                    // tslint:disable-next-line:directive-selector
                    selector: '[NguCarouselNext]'
                },] },
    ];
    /** @nocollapse */
    NguCarouselNextDirective.ctorParameters = function () { return []; };
    return NguCarouselNextDirective;
}());

var NguCarouselPrevDirective = /** @class */ (function () {
    function NguCarouselPrevDirective() {
    }
    NguCarouselPrevDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{
                    // tslint:disable-next-line:directive-selector
                    selector: '[NguCarouselPrev]'
                },] },
    ];
    /** @nocollapse */
    NguCarouselPrevDirective.ctorParameters = function () { return []; };
    return NguCarouselPrevDirective;
}());

var NguCarouselPointDirective = /** @class */ (function () {
    function NguCarouselPointDirective() {
    }
    NguCarouselPointDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{
                    // tslint:disable-next-line:directive-selector
                    selector: '[NguCarouselPoint]'
                },] },
    ];
    /** @nocollapse */
    NguCarouselPointDirective.ctorParameters = function () { return []; };
    return NguCarouselPointDirective;
}());

//# sourceMappingURL=ngu-carousel.directive.js.map

/***/ }),

/***/ 788:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_ngu_carousel_module__ = __webpack_require__(789);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__src_ngu_carousel_module__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__src_ngu_carousel_ngu_carousel_interface__ = __webpack_require__(791);
/* unused harmony reexport NguCarousel */
/* unused harmony reexport NguCarouselStore */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__src_ngu_carousel_service__ = __webpack_require__(785);
/* unused harmony reexport NguCarouselService */



//# sourceMappingURL=index.js.map

/***/ }),

/***/ 789:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__ = __webpack_require__(786);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngu_item_ngu_item_component__ = __webpack_require__(792);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngu_carousel_ngu_carousel_component__ = __webpack_require__(790);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngu_tile_ngu_tile_component__ = __webpack_require__(793);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngu_carousel_service__ = __webpack_require__(785);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguCarouselModule; });







var NguCarouselModule = /** @class */ (function () {
    function NguCarouselModule() {
    }
    NguCarouselModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"], args: [{
                    imports: [__WEBPACK_IMPORTED_MODULE_3__angular_common__["CommonModule"]],
                    exports: [
                        __WEBPACK_IMPORTED_MODULE_4__ngu_carousel_ngu_carousel_component__["a" /* NguCarouselComponent */],
                        __WEBPACK_IMPORTED_MODULE_1__ngu_item_ngu_item_component__["a" /* NguItemComponent */],
                        __WEBPACK_IMPORTED_MODULE_5__ngu_tile_ngu_tile_component__["a" /* NguTileComponent */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["a" /* NguCarouselPointDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["b" /* NguCarouselItemDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["c" /* NguCarouselNextDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["d" /* NguCarouselPrevDirective */]
                    ],
                    declarations: [
                        __WEBPACK_IMPORTED_MODULE_4__ngu_carousel_ngu_carousel_component__["a" /* NguCarouselComponent */],
                        __WEBPACK_IMPORTED_MODULE_1__ngu_item_ngu_item_component__["a" /* NguItemComponent */],
                        __WEBPACK_IMPORTED_MODULE_5__ngu_tile_ngu_tile_component__["a" /* NguTileComponent */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["a" /* NguCarouselPointDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["b" /* NguCarouselItemDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["c" /* NguCarouselNextDirective */],
                        __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["d" /* NguCarouselPrevDirective */]
                    ],
                    providers: [__WEBPACK_IMPORTED_MODULE_6__ngu_carousel_service__["a" /* NguCarouselService */]]
                },] },
    ];
    /** @nocollapse */
    NguCarouselModule.ctorParameters = function () { return []; };
    return NguCarouselModule;
}());

//# sourceMappingURL=ngu-carousel.module.js.map

/***/ }),

/***/ 790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__ = __webpack_require__(786);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngu_carousel_service__ = __webpack_require__(785);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguCarouselComponent; });




var NguCarouselComponent = /** @class */ (function () {
    function NguCarouselComponent(el, renderer, carouselSer, platformId) {
        this.el = el;
        this.renderer = renderer;
        this.carouselSer = carouselSer;
        this.platformId = platformId;
        this.carouselLoad = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        // tslint:disable-next-line:no-output-on-prefix
        this.onMove = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.initData = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.pauseCarousel = false;
        this.Arr1 = Array;
        this.pointNumbers = [];
        this.data = {
            type: 'fixed',
            token: '',
            deviceType: 'lg',
            items: 0,
            load: 0,
            deviceWidth: 0,
            carouselWidth: 0,
            itemWidth: 0,
            visibleItems: { start: 0, end: 0 },
            slideItems: 0,
            itemWidthPer: 0,
            itemLength: 0,
            currentSlide: 0,
            easing: 'cubic-bezier(0, 0, 0.2, 1)',
            speed: 400,
            transform: { xs: 0, sm: 0, md: 0, lg: 0, all: 0 },
            loop: false,
            dexVal: 0,
            touchTransform: 0,
            touch: { active: false, swipe: '', velocity: 0 },
            isEnd: false,
            isFirst: true,
            isLast: false,
            RTL: false,
            button: { visibility: 'disabled' },
            point: true,
            vertical: { enabled: false, height: 400 }
        };
    }
    NguCarouselComponent.prototype.ngOnChanges = function (changes) { };
    NguCarouselComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.carousel = this.el.nativeElement;
        this.carouselMain = this.carouselMain1.nativeElement;
        this.carouselInner = this.carouselInner1.nativeElement;
        this.rightBtn = this.next.nativeElement;
        this.leftBtn = this.prev.nativeElement;
        this.data.type = this.userData.grid.all !== 0 ? 'fixed' : 'responsive';
        this.data.loop = this.userData.loop || false;
        this.userData.easing = this.userData.easing || 'cubic-bezier(0, 0, 0.2, 1)';
        this.data.touch.active = this.userData.touch || false;
        this.data.RTL = this.userData.RTL ? true : false;
        if (this.userData.vertical && this.userData.vertical.enabled) {
            this.data.vertical.enabled = this.userData.vertical.enabled;
            this.data.vertical.height = this.userData.vertical.height;
        }
        this.directionSym = this.data.RTL ? '' : '-';
        this.data.point =
            this.userData.point && typeof this.userData.point.visible !== 'undefined'
                ? this.userData.point.visible
                : true;
        this.withAnim = true;
        this.carouselSize();
        // const datas = this.itemsElements.first.nativeElement.getBoundingClientRect().width;
        this.carouselSerSub = this.carouselSer.getData.subscribe(function (res) {
            if (res.id === _this.data.token) {
                if (res.ref === 'moveToSlide') {
                    if (_this.pointers !== res.index && res.index < _this.pointIndex) {
                        _this.withAnim = res.animation === false ? false : true;
                        _this.moveTo(res.index);
                    }
                }
                else if (res.ref === 'reset') {
                    if (_this.pointers !== 0 && 0 < _this.pointIndex) {
                        _this.withAnim = false;
                        _this.reset();
                    }
                }
            }
        });
    };
    NguCarouselComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        this.listener1 = this.renderer.listen(this.leftBtn, 'click', function () {
            return _this.carouselScrollOne(0);
        });
        this.listener2 = this.renderer.listen(this.rightBtn, 'click', function () {
            return _this.carouselScrollOne(1);
        });
        this.carouselCssNode = this.createStyleElem();
        this.storeCarouselData();
        this.buttonControl();
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_common__["isPlatformBrowser"])(this.platformId)) {
            this.carouselInterval();
            this.onWindowScrolling();
            if (!this.data.vertical.enabled) {
                this.touch();
            }
            this.listener3 = this.renderer.listen('window', 'resize', function (event) {
                _this.onResizing(event);
            });
        }
        this.items.changes.subscribe(function (val) {
            _this.data.isLast = false;
            _this.carouselPoint();
            _this.buttonControl();
        });
    };
    NguCarouselComponent.prototype.ngAfterViewInit = function () {
        if (this.userData.point.pointStyles) {
            var datas = this.userData.point.pointStyles.replace(/.ngucarouselPoint/g, "." + this.data.token + " .ngucarouselPoint");
            this.createStyleElem(datas);
        }
        else if (this.userData.point && this.userData.point.visible) {
            this.renderer.addClass(this.pointMain.nativeElement, 'ngucarouselPointDefault');
        }
    };
    NguCarouselComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.carouselInt);
        // tslint:disable-next-line:no-unused-expression
        this.itemsSubscribe && this.itemsSubscribe.unsubscribe();
        this.carouselSerSub && this.carouselSerSub.unsubscribe();
        this.carouselLoad.complete();
        // remove listeners
        for (var i = 1; i <= 8; i++) {
            // tslint:disable-next-line:no-eval
            eval("this.listener" + i + " && this.listener" + i + "()");
        }
    };
    NguCarouselComponent.prototype.onResizing = function (event) {
        var _this = this;
        clearTimeout(this.onResize);
        this.onResize = setTimeout(function () {
            if (_this.data.deviceWidth !== event.target.outerWidth) {
                _this.setStyle(_this.carouselInner, 'transition', "");
                _this.storeCarouselData();
            }
        }, 500);
    };
    /** Get Touch input */
    NguCarouselComponent.prototype.touch = function () {
        var _this = this;
        if (this.userData.touch) {
            var hammertime = new Hammer(this.carouselInner);
            hammertime.get('pan').set({ direction: Hammer.DIRECTION_HORIZONTAL });
            hammertime.on('panstart', function (ev) {
                _this.data.carouselWidth = _this.carouselInner.offsetWidth;
                _this.data.touchTransform = _this.data.transform[_this.data.deviceType];
                _this.data.dexVal = 0;
                _this.setStyle(_this.carouselInner, 'transition', '');
            });
            if (this.data.vertical.enabled) {
                hammertime.on('panup', function (ev) {
                    _this.touchHandling('panleft', ev);
                });
                hammertime.on('pandown', function (ev) {
                    _this.touchHandling('panright', ev);
                });
            }
            else {
                hammertime.on('panleft', function (ev) {
                    _this.touchHandling('panleft', ev);
                });
                hammertime.on('panright', function (ev) {
                    _this.touchHandling('panright', ev);
                });
            }
            hammertime.on('panend', function (ev) {
                if (Math.abs(ev.velocity) > 1) {
                    _this.data.touch.velocity = ev.velocity;
                    var direc = 0;
                    if (!_this.data.RTL) {
                        direc = _this.data.touch.swipe === 'panright' ? 0 : 1;
                    }
                    else {
                        direc = _this.data.touch.swipe === 'panright' ? 1 : 0;
                    }
                    _this.carouselScrollOne(direc);
                }
                else {
                    _this.data.dexVal = 0;
                    _this.setStyle(_this.carouselInner, 'transition', 'transform 324ms cubic-bezier(0, 0, 0.2, 1)');
                    _this.setStyle(_this.carouselInner, 'transform', '');
                }
            });
            hammertime.on('hammer.input', function (ev) {
                // allow nested touch events to no propagate, this may have other side affects but works for now.
                // TODO: It is probably better to check the source element of the event and only apply the handle to the correct carousel
                ev.srcEvent.stopPropagation();
            });
        }
    };
    /** handle touch input */
    NguCarouselComponent.prototype.touchHandling = function (e, ev) {
        // vertical touch events seem to cause to panstart event with an odd delta
        // and a center of {x:0,y:0} so this will ignore them
        if (ev.center.x === 0) {
            return;
        }
        ev = Math.abs(this.data.vertical.enabled ? ev.deltaY : ev.deltaX);
        var valt = ev - this.data.dexVal;
        valt =
            this.data.type === 'responsive'
                ? Math.abs(ev - this.data.dexVal) /
                    (this.data.vertical.enabled
                        ? this.data.vertical.height
                        : this.data.carouselWidth) *
                    100
                : valt;
        this.data.dexVal = ev;
        this.data.touch.swipe = e;
        if (!this.data.RTL) {
            this.data.touchTransform =
                e === 'panleft'
                    ? valt + this.data.touchTransform
                    : this.data.touchTransform - valt;
        }
        else {
            this.data.touchTransform =
                e === 'panright'
                    ? valt + this.data.touchTransform
                    : this.data.touchTransform - valt;
        }
        if (this.data.touchTransform > 0) {
            if (this.data.type === 'responsive') {
                this.setStyle(this.carouselInner, 'transform', this.data.vertical.enabled
                    ? "translate3d(0, " + this.directionSym + this.data.touchTransform + "%, 0)"
                    : "translate3d(" + this.directionSym + this.data.touchTransform + "%, 0, 0)");
            }
            else {
                this.setStyle(this.carouselInner, 'transform', this.data.vertical.enabled
                    ? "translate3d(0, " + this.directionSym + this.data.touchTransform + "px, 0)"
                    : "translate3d(" + this.directionSym + this.data.touchTransform + "px, 0px, 0px)");
            }
        }
        else {
            this.data.touchTransform = 0;
        }
    };
    /** this used to disable the scroll when it is not on the display */
    NguCarouselComponent.prototype.onWindowScrolling = function () {
        var top = this.carousel.offsetTop;
        var scrollY = window.scrollY;
        var heightt = window.innerHeight;
        var carouselHeight = this.carousel.offsetHeight;
        if (top <= scrollY + heightt - carouselHeight / 4 &&
            top + carouselHeight / 2 >= scrollY) {
            this.carouselIntervalEvent(0);
        }
        else {
            this.carouselIntervalEvent(1);
        }
    };
    /** store data based on width of the screen for the carousel */
    NguCarouselComponent.prototype.storeCarouselData = function () {
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_common__["isPlatformBrowser"])(this.platformId)) {
            this.data.deviceWidth = window.innerWidth;
        }
        else {
            this.data.deviceWidth = 1200;
        }
        this.data.carouselWidth = this.carouselMain.offsetWidth;
        if (this.data.type === 'responsive') {
            this.data.deviceType =
                this.data.deviceWidth >= 1200
                    ? 'lg'
                    : this.data.deviceWidth >= 992
                        ? 'md'
                        : this.data.deviceWidth >= 768 ? 'sm' : 'xs';
            this.data.items = this.userData.grid[this.data.deviceType];
            this.data.itemWidth = this.data.carouselWidth / this.data.items;
        }
        else {
            this.data.items = Math.trunc(this.data.carouselWidth / this.userData.grid.all);
            this.data.itemWidth = this.userData.grid.all;
            this.data.deviceType = 'all';
        }
        this.data.slideItems = +(this.userData.slide < this.data.items
            ? this.userData.slide
            : this.data.items);
        this.data.load =
            this.userData.load >= this.data.slideItems
                ? this.userData.load
                : this.data.slideItems;
        this.userData.speed =
            this.userData.speed || this.userData.speed > -1
                ? this.userData.speed
                : 400;
        this.initData.emit(this.data);
        this.carouselPoint();
    };
    /** Used to reset the carousel */
    NguCarouselComponent.prototype.reset = function () {
        this.carouselCssNode.innerHTML = '';
        this.moveTo(0);
        this.carouselPoint();
    };
    /** Init carousel point */
    NguCarouselComponent.prototype.carouselPoint = function () {
        // if (this.userData.point.visible === true) {
        var Nos = this.items.length - (this.data.items - this.data.slideItems);
        this.pointIndex = Math.ceil(Nos / this.data.slideItems);
        var pointers = [];
        if (this.pointIndex > 1 || !this.userData.point.hideOnSingleSlide) {
            for (var i = 0; i < this.pointIndex; i++) {
                pointers.push(i);
            }
        }
        this.pointNumbers = pointers;
        this.carouselPointActiver();
        if (this.pointIndex <= 1) {
            this.btnBoolean(1, 1);
        }
        else {
            if (this.data.currentSlide === 0 && !this.data.loop) {
                this.btnBoolean(1, 0);
            }
            else {
                this.btnBoolean(0, 0);
            }
        }
        this.buttonControl();
        // }
    };
    /** change the active point in carousel */
    NguCarouselComponent.prototype.carouselPointActiver = function () {
        var i = Math.ceil(this.data.currentSlide / this.data.slideItems);
        this.pointers = i;
    };
    /** this function is used to scoll the carousel when point is clicked */
    NguCarouselComponent.prototype.moveTo = function (index) {
        if (this.pointers !== index && index < this.pointIndex) {
            var slideremains = 0;
            var btns = this.data.currentSlide < index ? 1 : 0;
            if (index === 0) {
                this.btnBoolean(1, 0);
                slideremains = index * this.data.slideItems;
            }
            else if (index === this.pointIndex - 1) {
                this.btnBoolean(0, 1);
                slideremains = this.items.length - this.data.items;
            }
            else {
                this.btnBoolean(0, 0);
                slideremains = index * this.data.slideItems;
            }
            this.carouselScrollTwo(btns, slideremains, this.data.speed);
        }
    };
    /** set the style of the carousel based the inputs data */
    NguCarouselComponent.prototype.carouselSize = function () {
        this.data.token = this.generateID();
        var dism = '';
        this.styleSelector = "." + this.data.token + " > .ngucarousel > .ngucarousel-inner > .ngucarousel-items";
        if (this.userData.custom === 'banner') {
            this.renderer.addClass(this.carousel, 'banner');
        }
        if (this.userData.animation === 'lazy') {
            dism += this.styleSelector + " > .item {transition: transform .6s ease;}";
        }
        var itemStyle = '';
        if (this.data.vertical.enabled) {
            var itemWidth_xs = this.styleSelector + " > .item {height: " + this.data
                .vertical.height / +this.userData.grid.xs + "px}";
            var itemWidth_sm = this.styleSelector + " > .item {height: " + this.data
                .vertical.height / +this.userData.grid.sm + "px}";
            var itemWidth_md = this.styleSelector + " > .item {height: " + this.data
                .vertical.height / +this.userData.grid.md + "px}";
            var itemWidth_lg = this.styleSelector + " > .item {height: " + this.data
                .vertical.height / +this.userData.grid.lg + "px}";
            itemStyle = "@media (max-width:767px){" + itemWidth_xs + "}\n                    @media (min-width:768px){" + itemWidth_sm + "}\n                    @media (min-width:992px){" + itemWidth_md + "}\n                    @media (min-width:1200px){" + itemWidth_lg + "}";
        }
        else if (this.data.type === 'responsive') {
            var itemWidth_xs = this.userData.type === 'mobile'
                ? this.styleSelector + " .item {flex: 0 0 " + 95 /
                    +this.userData.grid.xs + "%}"
                : this.styleSelector + " .item {flex: 0 0 " + 100 /
                    +this.userData.grid.xs + "%}";
            var itemWidth_sm = this.styleSelector + " > .item {flex: 0 0 " + 100 /
                +this.userData.grid.sm + "%}";
            var itemWidth_md = this.styleSelector + " > .item {flex: 0 0 " + 100 /
                +this.userData.grid.md + "%}";
            var itemWidth_lg = this.styleSelector + " > .item {flex: 0 0 " + 100 /
                +this.userData.grid.lg + "%}";
            itemStyle = "@media (max-width:767px){" + itemWidth_xs + "}\n                    @media (min-width:768px){" + itemWidth_sm + "}\n                    @media (min-width:992px){" + itemWidth_md + "}\n                    @media (min-width:1200px){" + itemWidth_lg + "}";
        }
        else {
            itemStyle = this.styleSelector + " .item {flex: 0 0 " + this.userData.grid.all + "px}";
        }
        this.renderer.addClass(this.carousel, this.data.token);
        if (this.data.vertical.enabled) {
            this.renderer.addClass(this.carouselInner, 'nguvertical');
            this.renderer.setStyle(this.forTouch.nativeElement, 'height', this.data.vertical.height + "px");
        }
        // tslint:disable-next-line:no-unused-expression
        this.data.RTL &&
            !this.data.vertical.enabled &&
            this.renderer.addClass(this.carousel, 'ngurtl');
        this.createStyleElem(dism + " " + itemStyle);
    };
    /** logic to scroll the carousel step 1 */
    NguCarouselComponent.prototype.carouselScrollOne = function (Btn) {
        var itemSpeed = this.data.speed;
        var translateXval, currentSlide = 0;
        var touchMove = Math.ceil(this.data.dexVal / this.data.itemWidth);
        this.setStyle(this.carouselInner, 'transform', '');
        if (this.pointIndex === 1) {
            return;
        }
        else if (Btn === 0 &&
            ((!this.data.loop && !this.data.isFirst) || this.data.loop)) {
            var slide = this.data.slideItems * this.pointIndex;
            var currentSlideD = this.data.currentSlide - this.data.slideItems;
            var MoveSlide = currentSlideD + this.data.slideItems;
            this.btnBoolean(0, 1);
            if (this.data.currentSlide === 0) {
                currentSlide = this.items.length - this.data.items;
                itemSpeed = 400;
                this.btnBoolean(0, 1);
            }
            else if (this.data.slideItems >= MoveSlide) {
                currentSlide = translateXval = 0;
                this.btnBoolean(1, 0);
            }
            else {
                this.btnBoolean(0, 0);
                if (touchMove > this.data.slideItems) {
                    currentSlide = this.data.currentSlide - touchMove;
                    itemSpeed = 200;
                }
                else {
                    currentSlide = this.data.currentSlide - this.data.slideItems;
                }
            }
            this.carouselScrollTwo(Btn, currentSlide, itemSpeed);
        }
        else if (Btn === 1 &&
            ((!this.data.loop && !this.data.isLast) || this.data.loop)) {
            if (this.items.length <=
                this.data.currentSlide + this.data.items + this.data.slideItems &&
                !this.data.isLast) {
                currentSlide = this.items.length - this.data.items;
                this.btnBoolean(0, 1);
            }
            else if (this.data.isLast) {
                currentSlide = translateXval = 0;
                itemSpeed = 400;
                this.btnBoolean(1, 0);
            }
            else {
                this.btnBoolean(0, 0);
                if (touchMove > this.data.slideItems) {
                    currentSlide =
                        this.data.currentSlide +
                            this.data.slideItems +
                            (touchMove - this.data.slideItems);
                    itemSpeed = 200;
                }
                else {
                    currentSlide = this.data.currentSlide + this.data.slideItems;
                }
            }
            this.carouselScrollTwo(Btn, currentSlide, itemSpeed);
        }
        // cubic-bezier(0.15, 1.04, 0.54, 1.13)
    };
    /** logic to scroll the carousel step 2 */
    NguCarouselComponent.prototype.carouselScrollTwo = function (Btn, currentSlide, itemSpeed) {
        if (this.data.dexVal !== 0) {
            var val = Math.abs(this.data.touch.velocity);
            var somt = Math.floor(this.data.dexVal /
                val /
                this.data.dexVal *
                (this.data.deviceWidth - this.data.dexVal));
            somt = somt > itemSpeed ? itemSpeed : somt;
            itemSpeed = somt < 200 ? 200 : somt;
            this.data.dexVal = 0;
        }
        if (this.withAnim) {
            this.setStyle(this.carouselInner, 'transition', "transform " + itemSpeed + "ms " + this.userData.easing);
            this.userData.animation &&
                this.carouselAnimator(Btn, currentSlide + 1, currentSlide + this.data.items, itemSpeed, Math.abs(this.data.currentSlide - currentSlide));
        }
        else {
            this.setStyle(this.carouselInner, 'transition', "");
        }
        this.data.itemLength = this.items.length;
        this.transformStyle(currentSlide);
        this.data.currentSlide = currentSlide;
        this.onMove.emit(this.data);
        this.carouselPointActiver();
        this.carouselLoadTrigger();
        this.buttonControl();
        this.withAnim = true;
    };
    /** boolean function for making isFirst and isLast */
    NguCarouselComponent.prototype.btnBoolean = function (first, last) {
        this.data.isFirst = first ? true : false;
        this.data.isLast = last ? true : false;
    };
    NguCarouselComponent.prototype.transformString = function (grid, slide) {
        var collect = '';
        collect += this.styleSelector + " { transform: translate3d(";
        if (this.data.vertical.enabled) {
            this.data.transform[grid] =
                this.data.vertical.height / this.userData.grid[grid] * slide;
            collect += "0, -" + this.data.transform[grid] + "px, 0";
        }
        else {
            this.data.transform[grid] = 100 / this.userData.grid[grid] * slide;
            collect += "" + this.directionSym + this.data.transform[grid] + "%, 0, 0";
        }
        collect += "); }";
        return collect;
    };
    /** set the transform style to scroll the carousel  */
    NguCarouselComponent.prototype.transformStyle = function (slide) {
        var slideCss = '';
        if (this.data.type === 'responsive') {
            slideCss = "@media (max-width: 767px) {" + this.transformString('xs', slide) + "}\n      @media (min-width: 768px) {" + this.transformString('sm', slide) + " }\n      @media (min-width: 992px) {" + this.transformString('md', slide) + " }\n      @media (min-width: 1200px) {" + this.transformString('lg', slide) + " }";
        }
        else {
            this.data.transform.all = this.userData.grid.all * slide;
            slideCss = this.styleSelector + " { transform: translate3d(" + this.directionSym + this.data.transform.all + "px, 0, 0);";
        }
        this.carouselCssNode.innerHTML = slideCss;
    };
    /** this will trigger the carousel to load the items */
    NguCarouselComponent.prototype.carouselLoadTrigger = function () {
        if (typeof this.userData.load === 'number') {
            // tslint:disable-next-line:no-unused-expression
            this.items.length - this.data.load <=
                this.data.currentSlide + this.data.items &&
                this.carouselLoad.emit(this.data.currentSlide);
        }
    };
    /** generate Class for each carousel to set specific style */
    NguCarouselComponent.prototype.generateID = function () {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < 6; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return "ngucarousel" + text;
    };
    /** handle the auto slide */
    NguCarouselComponent.prototype.carouselInterval = function () {
        var _this = this;
        if (typeof this.userData.interval === 'number' && this.data.loop) {
            this.listener4 = this.renderer.listen(this.carouselMain, 'touchstart', function () {
                _this.carouselIntervalEvent(1);
            });
            this.listener5 = this.renderer.listen(this.carouselMain, 'touchend', function () {
                _this.carouselIntervalEvent(0);
            });
            this.listener6 = this.renderer.listen(this.carouselMain, 'mouseenter', function () {
                _this.carouselIntervalEvent(1);
            });
            this.listener7 = this.renderer.listen(this.carouselMain, 'mouseleave', function () {
                _this.carouselIntervalEvent(0);
            });
            this.listener8 = this.renderer.listen('window', 'scroll', function () {
                clearTimeout(_this.onScrolling);
                _this.onScrolling = setTimeout(function () {
                    _this.onWindowScrolling();
                }, 600);
            });
            this.carouselInt = setInterval(function () {
                // tslint:disable-next-line:no-unused-expression
                !_this.pauseCarousel && _this.carouselScrollOne(1);
            }, this.userData.interval);
        }
    };
    /** pause interval for specific time */
    NguCarouselComponent.prototype.carouselIntervalEvent = function (ev) {
        var _this = this;
        this.evtValue = ev;
        if (this.evtValue === 0) {
            clearTimeout(this.pauseInterval);
            this.pauseInterval = setTimeout(function () {
                // tslint:disable-next-line:no-unused-expression
                _this.evtValue === 0 && (_this.pauseCarousel = false);
            }, this.userData.interval);
        }
        else {
            this.pauseCarousel = true;
        }
    };
    /** animate the carousel items */
    NguCarouselComponent.prototype.carouselAnimator = function (direction, start, end, speed, length) {
        var _this = this;
        var val = length < 5 ? length : 5;
        val = val === 1 ? 3 : val;
        var itemList = this.items.toArray();
        if (direction === 1) {
            for (var i = start - 1; i < end; i++) {
                val = val * 2;
                itemList[i] &&
                    this.setStyle(itemList[i].nativeElement, 'transform', "translate3d(" + val + "px, 0, 0)");
            }
        }
        else {
            for (var i = end - 1; i >= start - 1; i--) {
                val = val * 2;
                itemList[i] &&
                    this.setStyle(itemList[i].nativeElement, 'transform', "translate3d(-" + val + "px, 0, 0)");
            }
        }
        setTimeout(function () {
            _this.items.forEach(function (elem) {
                return _this.setStyle(elem.nativeElement, 'transform', "translate3d(0, 0, 0)");
            });
        }, speed * 0.7);
    };
    /** control button for loop */
    NguCarouselComponent.prototype.buttonControl = function () {
        var arr = [];
        if (!this.data.loop || (this.data.isFirst && this.data.isLast)) {
            arr = [
                this.data.isFirst ? 'none' : 'block',
                this.data.isLast ? 'none' : 'block'
            ];
        }
        else {
            arr = ['block', 'block'];
        }
        this.setStyle(this.leftBtn, 'display', arr[0]);
        this.setStyle(this.rightBtn, 'display', arr[1]);
    };
    /** Short form for setElementStyle */
    NguCarouselComponent.prototype.setStyle = function (el, prop, val) {
        this.renderer.setStyle(el, prop, val);
    };
    /** For generating style tag */
    NguCarouselComponent.prototype.createStyleElem = function (datas) {
        var styleItem = this.renderer.createElement('style');
        if (datas) {
            var styleText = this.renderer.createText(datas);
            this.renderer.appendChild(styleItem, styleText);
        }
        this.renderer.appendChild(this.carousel, styleItem);
        return styleItem;
    };
    NguCarouselComponent.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"], args: [{
                    // tslint:disable-next-line:component-selector
                    selector: 'ngu-carousel',
                    template: "<div #ngucarousel class=\"ngucarousel\"><div #forTouch class=\"ngucarousel-inner\"><div #nguitems class=\"ngucarousel-items\"><ng-content select=\"[NguCarouselItem]\"></ng-content></div><div style=\"clear: both\"></div></div><ng-content select=\"[NguCarouselPrev]\"></ng-content><ng-content select=\"[NguCarouselNext]\"></ng-content></div><div #points *ngIf=\"data.point\"><ul class=\"ngucarouselPoint\"><li #pointInner *ngFor=\"let i of pointNumbers; let i = index\" [class.active]=\"i==pointers\" (click)=\"moveTo(i)\"></li></ul></div>",
                    styles: [
                        "\n    :host {\n      display: block;\n      position: relative;\n    }\n\n    :host.ngurtl {\n      direction: rtl;\n    }\n    \n    .nguvertical {\n      flex-direction: column;\n    }\n\n    .ngucarousel .ngucarousel-inner {\n      position: relative;\n      overflow: hidden;\n    }\n    .ngucarousel .ngucarousel-inner .ngucarousel-items {\n      position: relative;\n      display: flex;\n    }\n\n    .banner .ngucarouselPointDefault .ngucarouselPoint {\n      position: absolute;\n      width: 100%;\n      bottom: 20px;\n    }\n    .banner .ngucarouselPointDefault .ngucarouselPoint li {\n      background: rgba(255, 255, 255, 0.55);\n    }\n    .banner .ngucarouselPointDefault .ngucarouselPoint li.active {\n      background: white;\n    }\n    .banner .ngucarouselPointDefault .ngucarouselPoint li:hover {\n      cursor: pointer;\n    }\n\n    .ngucarouselPointDefault .ngucarouselPoint {\n      list-style-type: none;\n      text-align: center;\n      padding: 12px;\n      margin: 0;\n      white-space: nowrap;\n      overflow: auto;\n      box-sizing: border-box;\n    }\n    .ngucarouselPointDefault .ngucarouselPoint li {\n      display: inline-block;\n      border-radius: 50%;\n      background: rgba(0, 0, 0, 0.55);\n      padding: 4px;\n      margin: 0 4px;\n      transition-timing-function: cubic-bezier(0.17, 0.67, 0.83, 0.67);\n      transition: 0.4s;\n    }\n    .ngucarouselPointDefault .ngucarouselPoint li.active {\n      background: #6b6b6b;\n      transform: scale(1.8);\n    }\n    .ngucarouselPointDefault .ngucarouselPoint li:hover {\n      cursor: pointer;\n    }\n\n  "
                    ]
                },] },
    ];
    /** @nocollapse */
    NguCarouselComponent.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], },
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Renderer2"], },
        { type: __WEBPACK_IMPORTED_MODULE_3__ngu_carousel_service__["a" /* NguCarouselService */], },
        { type: Object, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"], args: [__WEBPACK_IMPORTED_MODULE_1__angular_core__["PLATFORM_ID"],] },] },
    ]; };
    NguCarouselComponent.propDecorators = {
        'userData': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"], args: ['inputs',] },],
        'reseter': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"], args: ['reset',] },],
        'carouselLoad': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"], args: ['carouselLoad',] },],
        'onMove': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"], args: ['onMove',] },],
        'initData': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"], args: ['initData',] },],
        'items': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ContentChildren"], args: [__WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["b" /* NguCarouselItemDirective */], { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'points': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChildren"], args: ['pointInner', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'next': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ContentChild"], args: [__WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["c" /* NguCarouselNextDirective */], { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'prev': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ContentChild"], args: [__WEBPACK_IMPORTED_MODULE_0__ngu_carousel_directive__["d" /* NguCarouselPrevDirective */], { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'carouselMain1': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['ngucarousel', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'carouselInner1': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['nguitems', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'carousel1': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['main', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'pointMain': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['points', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
        'forTouch': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"], args: ['forTouch', { read: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] },] },],
    };
    return NguCarouselComponent;
}());

//# sourceMappingURL=ngu-carousel.component.js.map

/***/ }),

/***/ 791:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export NguCarouselStore */
/* unused harmony export ItemsControl */
/* unused harmony export Vertical */
/* unused harmony export NguButton */
/* unused harmony export Touch */
/* unused harmony export Transfrom */
/* unused harmony export NguCarousel */
var NguCarouselStore = /** @class */ (function () {
    function NguCarouselStore() {
    }
    return NguCarouselStore;
}());

var ItemsControl = /** @class */ (function () {
    function ItemsControl() {
    }
    return ItemsControl;
}());

var Vertical = /** @class */ (function () {
    function Vertical() {
    }
    return Vertical;
}());

var NguButton = /** @class */ (function () {
    function NguButton() {
    }
    return NguButton;
}());

var Touch = /** @class */ (function () {
    function Touch() {
    }
    return Touch;
}());

var Transfrom = /** @class */ (function () {
    function Transfrom() {
    }
    return Transfrom;
}());

var NguCarousel = /** @class */ (function () {
    function NguCarousel() {
    }
    return NguCarousel;
}());

//# sourceMappingURL=ngu-carousel.interface.js.map

/***/ }),

/***/ 792:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguItemComponent; });

var NguItemComponent = /** @class */ (function () {
    function NguItemComponent() {
        this.classes = true;
    }
    NguItemComponent.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                    selector: 'ngu-item',
                    template: "<ng-content></ng-content>",
                    styles: ["\n    :host {\n        display: inline-block;\n        white-space: initial;\n        vertical-align: top;\n    }\n  "]
                },] },
    ];
    /** @nocollapse */
    NguItemComponent.ctorParameters = function () { return []; };
    NguItemComponent.propDecorators = {
        'classes': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"], args: ['class.item',] },],
    };
    return NguItemComponent;
}());

//# sourceMappingURL=ngu-item.component.js.map

/***/ }),

/***/ 793:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NguTileComponent; });

var NguTileComponent = /** @class */ (function () {
    function NguTileComponent() {
        this.classes = true;
    }
    NguTileComponent.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                    selector: 'ngu-tile',
                    template: "<div class=\"tile\"><ng-content></ng-content></div>",
                    styles: ["\n    :host {\n        display: inline-block;\n        white-space: initial;\n        padding: 10px;\n        box-sizing: border-box;\n        vertical-align: top;\n    }\n\n    .tile {\n        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);\n    }\n\n    * {\n        box-sizing: border-box;\n    }\n  "]
                },] },
    ];
    /** @nocollapse */
    NguTileComponent.ctorParameters = function () { return []; };
    NguTileComponent.propDecorators = {
        'classes': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"], args: ['class.item',] },],
    };
    return NguTileComponent;
}());

//# sourceMappingURL=ngu-tile.component.js.map

/***/ }),

/***/ 794:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(7);
//declare var Swiper: any;
var SwiperComponent = (function () {
    function SwiperComponent(elementRef, changeDetectorRef) {
        this.elementRef = elementRef;
        this.changeDetectorRef = changeDetectorRef;
        this.slideCount = 0;
        this.initialized = false;
        this.shouldInitialize = true;
    }
    Object.defineProperty(SwiperComponent.prototype, "initialize", {
        set: function (value) {
            this.shouldInitialize = this.initialized ? false : value;
        },
        enumerable: true,
        configurable: true
    });
    ;
    SwiperComponent.prototype.ngAfterViewInit = function () {
        if (this.shouldInitialize) {
            this.setup();
        }
    };
    SwiperComponent.prototype.setup = function () {
        if (!this.swiper) {
            // if rendered on server querySelector is undefined
            if (this.elementRef.nativeElement.querySelector) {
                this.swiperWrapper = this.elementRef.nativeElement.querySelector('.swiper-wrapper');
                this.slideCount = this.swiperWrapper.childElementCount;
                this.swiper = new Swiper(this.elementRef.nativeElement.querySelector('swiper > div'), this.config);
                this.changeDetectorRef.detectChanges();
            }
            this.shouldInitialize = false;
        }
    };
    SwiperComponent.prototype.ngAfterViewChecked = function () {
        if (this.shouldInitialize) {
            this.setup();
        }
        if (this.swiperWrapper && this.slideCount !== this.swiperWrapper.childElementCount) {
            this.slideCount = this.swiperWrapper.childElementCount;
            this.swiper.update();
        }
    };
    return SwiperComponent;
}());
SwiperComponent.decorators = [
    { type: core_1.Component, args: [{
                selector: 'swiper',
                template: "<div class=\"swiper-container\">\n                    <ng-content></ng-content>\n                </div>",
                styles: [':host {display: block;}', ':host > div {width: 100%;height: 100%;}']
            },] },
];
/** @nocollapse */
SwiperComponent.ctorParameters = function () { return [
    { type: core_1.ElementRef, },
    { type: core_1.ChangeDetectorRef, },
]; };
SwiperComponent.propDecorators = {
    'config': [{ type: core_1.Input },],
    'initialize': [{ type: core_1.Input, args: ['initialize',] },],
};
exports.SwiperComponent = SwiperComponent;
//# sourceMappingURL=swiper.component.js.map

/***/ }),

/***/ 795:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_dom__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismic_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_wow__ = __webpack_require__(302);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DashboardComponent = (function () {
    function DashboardComponent(prismic, route, titleService, meta, wowService) {
        this.prismic = prismic;
        this.route = route;
        this.titleService = titleService;
        this.meta = meta;
        this.wowService = wowService;
        this.headerData = [];
        this.PrismicDOM = __WEBPACK_IMPORTED_MODULE_5_prismic_dom___default.a;
        this.toolbar = false;
        this.isMobile = false;
        this.config = {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            spaceBetween: 30
        };
        this.wowService.init();
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeStream = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].fromPromise(this.prismic.buildContext())
            .subscribe(function (ctx) {
            _this.ctx = ctx;
            _this.fetchPage();
            $(document).ready(function () {
                if ($('[data-background]').length > 0) {
                    $('[data-background]').each(function () {
                        var $background, $backgroundmobile, $this;
                        $this = $(this);
                        $background = $(this).attr('data-background');
                        $backgroundmobile = $(this).attr('data-background-mobile');
                        if ($this.attr('data-background').substr(0, 1) === '#') {
                            return $this.css('background-color', $background);
                        }
                        else if ($this.attr('data-background-mobile')) {
                            return $this.css('background-image', 'url(' + $backgroundmobile + ')');
                        }
                        else {
                            return $this.css('background-image', 'url(' + $background + ')');
                        }
                    });
                }
            });
        });
        this.isMobile = true;
        this.dataSheetCarousel = {
            grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
            slide: 1,
            speed: 500,
            interval: 5000,
            point: {
                visible: false
            },
            load: 2,
            touch: true,
            loop: true,
            custom: 'banner'
        };
        this.dashboardCarousel = {
            grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
            slide: 1,
            speed: 1000,
            interval: 5000,
            point: {
                visible: true
            },
            load: 2,
            touch: true,
            loop: true,
            custom: 'banner'
        };
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        this.routeStream.unsubscribe();
    };
    DashboardComponent.prototype.ngAfterViewChecked = function () {
        if (this.ctx && !this.toolbar) {
            // this.prismic.toolbar(this.ctx.api);
            this.toolbar = true;
            window.scrollTo(0, 0);
        }
    };
    DashboardComponent.prototype.fetchPage = function () {
        var _this = this;
        this.ctx.api.query([
            __WEBPACK_IMPORTED_MODULE_4_prismic_javascript___default.a.Predicates.any('document.type', ['hpcarousel', 'summary', 'featureshort', 'hptestimonial', 'download', 'efficiency', 'industry', 'enso', 'herocarousel']),
        ])
            .then(function (response) {
            console.log(response.results);
            _this.pageContent = response.results;
            var metaDescription = "";
            var metaKeyWords = [];
            _this.pageContent.map(function (prop) {
                if (prop.uid === 'hpcarousel') {
                    _this.hpCarouselData = prop.data;
                    metaKeyWords.push(_this.hpCarouselData['meta-title']);
                }
                else if (prop.uid === 'summary') {
                    _this.summaryData = prop.data.ensoplatform;
                    metaDescription = prop.data['meta-description'];
                    metaKeyWords.push(prop.data['meta-title']);
                }
                else if (prop.uid === 'feature') {
                    _this.featureShortData = prop.data;
                    metaKeyWords.push(_this.featureShortData['meta-title']);
                }
                else if (prop.uid === 'testimonial') {
                    _this.testimonialData = prop.data;
                    metaKeyWords.push(_this.testimonialData['meta-title']);
                }
                else if (prop.uid === 'datasheets') {
                    _this.dataSheetData = prop.data;
                    metaKeyWords.push(_this.dataSheetData['meta-title']);
                }
                else if (prop.uid === 'efficiency') {
                    _this.efficiencyData = prop.data;
                    metaKeyWords.push(_this.efficiencyData['meta-title']);
                }
                else if (prop.uid === 'industry') {
                    _this.industryData = prop.data;
                    metaKeyWords.push(_this.industryData['meta-title']);
                }
                else if (prop.uid === 'enso') {
                    _this.ensoData = prop.data;
                    metaKeyWords.push(_this.ensoData['meta-title']);
                }
                else if (prop.uid === 'herocarousel') {
                    _this.carouselData = prop.data;
                    metaKeyWords.push(_this.carouselData['meta-title']);
                }
            });
            _this.meta.updateTag({ name: 'description', content: metaDescription });
            _this.meta.updateTag({ name: 'author', content: 'Xpms' });
            _this.meta.updateTag({ name: 'keywords', content: metaKeyWords.toString() });
        })
            .catch(function (e) { return console.log(e); });
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-dashboard',
        template: __webpack_require__(812),
        styles: [__webpack_require__(804)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_7_ngx_wow__["b" /* NgwWowService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7_ngx_wow__["b" /* NgwWowService */]) === "function" && _e || Object])
], DashboardComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ 803:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(7);
var common_1 = __webpack_require__(34);
var swiper_component_1 = __webpack_require__(794);
var SwiperModule = (function () {
    function SwiperModule() {
    }
    return SwiperModule;
}());
SwiperModule.decorators = [
    { type: core_1.NgModule, args: [{
                imports: [
                    common_1.CommonModule
                ],
                exports: [
                    swiper_component_1.SwiperComponent
                ],
                declarations: [
                    swiper_component_1.SwiperComponent
                ],
                providers: []
            },] },
];
/** @nocollapse */
SwiperModule.ctorParameters = function () { return []; };
exports.SwiperModule = SwiperModule;
var swiper_component_2 = __webpack_require__(794);
exports.SwiperComponent = swiper_component_2.SwiperComponent;
//# sourceMappingURL=swiper.module.js.map

/***/ }),

/***/ 804:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports
exports.i(__webpack_require__(784), "");

// module
exports.push([module.i, ".homePageBg{\n  background-image:url(" + __webpack_require__(821) + ");\n  background-size:100% 100%;\n  height:84vh;\n  background-attachment:fixed;\n\n}\n.flex-div {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    height: 100%;\n}\n.carousel-item {\n    height: 100vh;\n    min-height: 300px;\n    background: no-repeat center center scroll;\n    background-size: cover;\n  }\n  .img-styles{\n    height: 350px;\n    background-size: 70% 100%;\n    background-repeat: no-repeat;\n    background-position: center;\n  }\n  .role {\n    position: absolute;\n        bottom: 0px;\n    padding: 8px 10px;\n    left: 50px;\n}\n.btn-value{\n  background-color: #f7d761;\n    padding: 5px;\n    border-radius: 8px;\n}\n.hero-mainText{\n  font-size: 42px;\n    color: white;\n    font-weight: normal;\n    line-height: 1.4;\n}\n.middleText{\n  color:#f7d761;\n}\n.hero-subText{\n  color:white;\n  font-size:18px;\n  font-weight:normal;\n  line-height:1.4\n}\nul#menu li {\n  display:inline;\n  padding-right:5px;\n}\n.btn-connect{\n  border-radius: 40px;\n    background-color: white;\n    color: #333;\n    text-transform: uppercase;\n    font-size: 14px;\n    padding: 8px 18px;\n    font-family:'Raleway', sans-serif;\n    font-weight:normal;\n}\n.poly{\n  -webkit-clip-path: polygon(20% 0, 100% 0, 100% 100%, 0 100%);\n  clip-path: polygon(20% 0, 100% 0, 100% 100%, 0 100%);\n  background-color: #1d7aa1;\n  height: 100%;\n  text-align:center;\n  color:white;\n  font-weight:bold;\n  font-size:12px;\n}\n.designationWidth{\n      width: 70%;\n    margin: 0 auto;\n    padding-top: 8%;\n}\n.poly p{\n  font-size: 18px;\n  font-weight: normal;\n  font-style: normal;\n  color: #ffffff;\n}\n.flex-div-center{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.clients{\n  background-color: #27abe2;\n  top: -80px;\n  position: relative;\n  height: 17vh;\n}\n.h-100{\n  height:100%;\n}\n.client-img{\n  width:45%;\n}\n.intelligence{\n  background-color:#f7d761;\n  top: -80px;\n  position: relative;\n}\n.carousel-image{\n    height: 118vh;\n    margin-top: -65px;\n}\n.carousel-indicators {\n\tbottom: -10px;\n}\n.carousel-indicators li {\n    display: inline-block;\n    width: 10px;\n    height: 10px;\n\tmargin: 1px;\n\tmargin-right: 5px;\n    text-indent: -999px;\n    cursor: pointer;\n    background-color: rgb(202, 202, 202);\n    border: 1px solid #fff;\n    border-radius: 10px;\n}\n.carousel-indicators .active{\n\twidth: 12px;\n    height: 12px;\n    margin: 0;\n\tmargin-right: 5px;\n    background-color: #25aae1;\n}\n.hero {\n\tposition: relative;\n\tz-index:99;\n}\n.i-carousel-wrapper {\n\tposition: relative;\n}\n.i-carousel-wrapper .carousel, .i-carousel-wrapper .dashboard-carousel {\n\tposition: absolute;\n\tz-index: 9;\n\twidth: 100%;\n}\n.i-carousel {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center\n}\n.hero .carousel {\n\tposition: relative;\n\t-webkit-box-align: center;\n\t    -ms-flex-align: center;\n\t        align-items: center;\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n}\n.c-row {\n\theight: 100%;\n}\n.img-carousel-section {\n\tfloat: right;\n}\n.main-carousel {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.img-carousel-section img{\n\tz-index: 99;\n}\n\n.no-padding{\n    padding-left: 0px;\n    padding-right: 0px;\n}\n.platform_logo{\n    width: 200px;\n    margin-left: 75px;\n}\n.headerStyle{\n    font-weight: 300;\n\tcolor: #505050;\n\tfont-size: 46px;\n\tmargin: 10px 0;\n}\n.enso-feature-subheading {\n\tmargin: 10px 0 20px 0;\n}\n.subHeader{\n    font-weight: 700;\n    color: #25aae1;\n}\n.btn-custom{\n    border-radius: 30px;\n    background-image: radial-gradient(circle at 100% 100%, #39d3e4, #25aae1);\n    color: white;\n\ttext-transform: uppercase;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n.customBackground{\n    background-size: contain;\n    background-position: center;\n    background-repeat: no-repeat;\n}\n.subHeading{\n    font-size: 20px;\n    font-weight: 300;\n    color: #9b9b9b;\n}\n.custom-top{\n    margin-top:15px;\n}\n.headings{\n    font-size:32px;\n    color:#4a4a4a;\n    font-weight: bold;\n\t  margin: 60px 0px 60px 0px;\n}\n.headings2{\n    font-size:32px;\n    color:#4a4a4a;\n    font-weight: bold;\n    text-transform: uppercase;\n}\n.image-style{\n    width: 120px;\n}\n.sectionTop{\n    margin-top:50px;\n}\n.btn-enso{\n    border: 1px solid #25aae1;\n    border-radius: 28px;\n    color: #25aae1;\n    text-transform: uppercase;\n    background-color: white;\n\tmargin-left: 25px;\n\theight: 40px;\n\tfont-size: 14px;\n\tline-height: 2;\n\tpadding: 6px 20px;\n}\n.btn-enso img, .btn-custom img {\n\tmargin-top: -3px;\n\tmargin-left: 3px;\n\theight: 12px;\n}\n.caseStudy{\n\tbackground-size: 100% 100%;\n\tbackground-position: top 50px right 0;\n    background-repeat: no-repeat;\n }\n \n.customCaseStudy{\n    background-size: 100% 100%;\n    -webkit-transform: skewY(-1.1deg);\n            transform: skewY(-1.1deg);\n    margin-top: 0px;\n\tbackground-repeat: no-repeat;\n}\n\n.slider-inner h1{\n\tmargin-top: 0;\n}\n.slick-slider\n{\n\tposition: relative;\n\tdisplay: block;\n\tbox-sizing: border-box;\n\t-webkit-user-select: none;\n\t-moz-user-select: none;\n\t-ms-user-select: none;\n\tuser-select: none;\n\t-webkit-touch-callout: none;\n\t-khtml-user-select: none;\n\t-ms-touch-action: pan-y;\n\ttouch-action: pan-y;\n\t-webkit-tap-highlight-color: transparent;\n}\n\n.slick-list\n{\n\tposition: relative;\n\tdisplay: block;\n\toverflow: hidden;\n\tmargin: 0;\n\tpadding: 0;\n}\n.slick-list:focus\n{\n\toutline: none;\n}\n.slick-list.dragging\n{\n\tcursor: pointer;\n\tcursor: hand;\n}\n\n.slick-slider .slick-track,\n.slick-slider .slick-list\n{\n\t-webkit-transform: translate3d(0, 0, 0);\n\ttransform: translate3d(0, 0, 0);\n}\n\n.slick-track\n{\n\tposition: relative;\n\ttop: 0;\n\tleft: 0;\n\tdisplay: block;\n}\n.slick-track:before,\n.slick-track:after\n{\n\tdisplay: table;\n\tcontent: '';\n}\n.slick-track:after\n{\n\tclear: both;\n}\n.slick-loading .slick-track\n{\n\tvisibility: hidden;\n}\n\n.slick-slide\n{\n\tdisplay: none;\n\tfloat: left;\n\theight: 100%;\n\tmin-height: 1px;\n}\n.slick-slide img\n{\n    display: block;\n    width: 258px;\n}\n.slick-slide.slick-loading img\n{\n\tdisplay: none;\n}\n.slick-slide.dragging img\n{\n\tpointer-events: none;\n}\n.slick-initialized .slick-slide\n{\n\tdisplay: block;\n}\n.slick-loading .slick-slide\n{\n\tvisibility: hidden;\n}\n.slick-vertical .slick-slide\n{\n\tdisplay: block;\n\theight: auto;\n\tborder: 1px solid transparent;\n}\n.slick-arrow.slick-hidden {\n\tdisplay: none;\n}\n.slick-prev,\n.slick-next {\n\tposition: absolute;\n\tdisplay: none !important;\n\twidth: 60px;\n\theight: 100px;\n\tcursor: pointer;\n\tbackground: transparent;\n\ttop: 50%;\n\tmargin-top: -45px;\n\tfont-size: 0;\n\tborder: none;\n\tz-index: 2;\n}\n.slick-prev:before,\n.slick-next:before,\n.slick-prev:after,\n.slick-next:after {\n\tcontent: '';\n\tposition: absolute;\n\tbackground: #fff;\n\theight: 2px;\n\twidth: 50px;\n\ttop: 50%;\n\tleft: 5px;\n\ttransition: all 0.2s ease-in-out;\n}\n.slick-prev:before,\n.slick-next:before {\n\tmargin-top: -22px;\n}\n.slick-prev:after,\n.slick-next:after {\n\tmargin-top: 22px;\n\tmargin-top: 2.2rem;\n}\n.slick-prev:hover:before,\n.slick-next:hover:before {\n\tmargin-top: -18px;\n\tmargin-top: -1.8rem;\n}\n.slick-prev:hover:after,\n.slick-next:hover:after {\n\tmargin-top: 18px;\n\tmargin-top: 1.8rem;\n}\n.slick-prev {\n\tleft: -7px;\n\tleft: -0.7rem;\n}\n.slick-prev:before {\n\t-webkit-transform: rotate(-60deg);\n\ttransform: rotate(-60deg);\n}\n.slick-prev:after {\n\t-webkit-transform: rotate(60deg);\n\ttransform: rotate(60deg);\n}\n.slick-prev:hover:before {\n\t-webkit-transform: rotate(-45deg);\n\ttransform: rotate(-45deg);\n}\n.slick-prev:hover:after {\n\t-webkit-transform: rotate(45deg);\n\ttransform: rotate(45deg);\n}\n.slick-next {\n\tright: -7px;\n\tright: -0.7rem;\n}\n.slick-next:before {\n\t-webkit-transform: rotate(60deg);\n\ttransform: rotate(60deg);\n}\n.slick-next:after {\n\t-webkit-transform: rotate(-60deg);\n\ttransform: rotate(-60deg);\n}\n.slick-next:hover:before {\n\t-webkit-transform: rotate(45deg);\n\ttransform: rotate(45deg);\n}\n.slick-next:hover:after {\n\t-webkit-transform: rotate(-45deg);\n\ttransform: rotate(-45deg);\n}\n.ct-slick-arrows--type2 .slick-prev,\n.ct-slick-arrows--type2 .slick-next {\n\topacity: 0.85;\n\tfilter: progid:DXImageTransform.Microsoft.Alpha(Opacity=85);\n\ttransition: all 250ms ease-in-out;\n}\n.ct-slick-arrows--type2 .slick-prev:before,\n.ct-slick-arrows--type2 .slick-next:before,\n.ct-slick-arrows--type2 .slick-prev:after,\n.ct-slick-arrows--type2 .slick-next:after {\n\tbackground-color: #fff;\n\theight: 15px;\n\theight: 1.5rem;\n\twidth: 70px;\n\twidth: 7rem;\n}\n.ct-slick-arrows--type2 .slick-prev:before,\n.ct-slick-arrows--type2 .slick-next:before {\n\tmargin-top: -20px;\n\tmargin-top: -2rem;\n}\n.ct-slick-arrows--type2 .slick-prev:after,\n.ct-slick-arrows--type2 .slick-next:after {\n\tmargin-top: 20px;\n\tmargin-top: 2rem;\n}\n.ct-slick-arrows--type2 .slick-prev:hover:before,\n.ct-slick-arrows--type2 .slick-next:hover:before {\n\tmargin-top: -20px;\n\tmargin-top: -2rem;\n}\n.ct-slick-arrows--type2 .slick-prev:hover:after,\n.ct-slick-arrows--type2 .slick-next:hover:after {\n\tmargin-top: 20px;\n\tmargin-top: 2rem;\n}\n.ct-slick-arrows--type2 .slick-prev:hover:active,\n.ct-slick-arrows--type2 .slick-next:hover:active {\n\topacity: 1;\n\tfilter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);\n}\n.ct-slick-arrows--type2 .slick-prev {\n\tleft: 10px;\n\tleft: 1rem;\n}\n.ct-slick-arrows--type2 .slick-prev:before {\n\t-webkit-transform: rotate(-45deg);\n\ttransform: rotate(-45deg);\n}\n.ct-slick-arrows--type2 .slick-prev:after {\n\t-webkit-transform: rotate(45deg);\n\ttransform: rotate(45deg);\n}\n.ct-slick-arrows--type2 .slick-prev:hover:before {\n\tmargin-top: -27px;\n\tmargin-top: -2.7rem;\n\t-webkit-transform: rotate(-60deg);\n\ttransform: rotate(-60deg);\n}\n.ct-slick-arrows--type2 .slick-prev:hover:after {\n\tmargin-top: 27px;\n\tmargin-top: 2.7rem;\n\t-webkit-transform: rotate(60deg);\n\ttransform: rotate(60deg);\n}\n.ct-slick-arrows--type2 .slick-next {\n\tright: 10px;\n\tright: 1rem;\n}\n.ct-slick-arrows--type2 .slick-next:before,\n.ct-slick-arrows--type2 .slick-next:after {\n\tleft: auto;\n\tright: 0;\n}\n.ct-slick-arrows--type2 .slick-next:before {\n\t-webkit-transform: rotate(45deg);\n\ttransform: rotate(45deg);\n}\n.ct-slick-arrows--type2 .slick-next:after {\n\t-webkit-transform: rotate(-45deg);\n\ttransform: rotate(-45deg);\n}\n.ct-slick-arrows--type2 .slick-next:hover:before {\n\tmargin-top: -27px;\n\tmargin-top: -2.7rem;\n\t-webkit-transform: rotate(60deg);\n\ttransform: rotate(60deg);\n}\n.ct-slick-arrows--type2 .slick-next:hover:after {\n\tmargin-top: 27px;\n\tmargin-top: 2.7rem;\n\t-webkit-transform: rotate(-60deg);\n\ttransform: rotate(-60deg);\n}\n#home.ct-header.ct-header--slider {\n\tbackground-size: cover;\n}\n.ct-header .ct-slick {\n\tmargin-bottom: 0;\n}\n.slick-initialized .slick-slide {\n\tdisplay: block;\n}\n.ct-u-display-tablex {\n\tdisplay: block;\n\twidth: 100%;\n\theight: 100%;\n}\n.ct-u-display-tablex > .inner {\n\twidth: 100%;\n}\n.ct-header h1.big {\n    text-transform: uppercase;\n    color: #4a4a4a;\n    font-size: 35px;\n    font-weight: bold;\n}\n.ct-header p {\n\tfont-size: 20px;\n\tline-height: 1.2;\n\tcolor: white;\n  font-size: 14px;\n  text-align: right;\n  font-weight:bold;\n}\n\n.ct-slick .slick-track,\n.ct-slick .slick-list,\n.ct-slick .item,\n.ct-slick .slick-item {\n\tmin-height: inherit;\n}\n.ct-slick .item {\n\tbackground-size: cover;\n}\n.slick-list {\n\theight: 500px;\n}\n\n/* Transitions */\n.activate {\n\t-moz-opacity: 1;\n\t-khtml-opacity: 1;\n\t-webkit-opacity: 1;\n\topacity: 1;\n\t-ms-filter: progid:DXImageTransform.Microsoft.Alpha(opacity=1 * 100);\n\tfilter: alpha(opacity=1 * 100);\n}\n.animated {\n\t-moz-opacity: 0;\n\t-khtml-opacity: 0;\n\t-webkit-opacity: 0;\n\topacity: 0;\n\t-ms-filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0 * 100);\n\tfilter: alpha(opacity=0 * 100);\n}\n\n.image-width{\n    width: 75%;\n    margin: 0 auto;\n}\n.product-slider { \n\tpadding: 45px 100px; \n}\n\nbutton.slick-next {\n\tdisplay: none;\n\tright: 0 !important;\n\n}\n.technology-subtitle{\n    margin:15px 0 0 0;\n\tmin-height: 40px;\n    text-align: center;\n}\n.btn-download {\n    border-radius: 20px;\n\tbackground-color: #ffffff;\n\tfont-size: 14px;\n\theight: 40px;\n    box-shadow: 0 1px 2px 0 rgba(124, 124, 124, 0.5);\n\ttext-transform: uppercase;\n\tpadding: 6px 20px;\n}\n.feature-wrapper {\n\tmin-height: 225px;\n}\n.iconStyle{\n\twidth: 50px;\n\theight: 50px;\n}\n.swiper-pagination {\n\tbottom: 0px;\n}\n\n.ct-u-display-tablex .row-eq-height {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\t-webkit-box-align: center;\n\t    -ms-flex-align: center;\n\t        align-items: center;\n}\n.ef-bg{\n   background-image: linear-gradient(179deg, rgba(19, 19, 19, 0), rgba(0, 0, 0, 0.5));\n   height: 400px;\n   background-color: #092632;\n}\n.inner-bg{\n  background-image: url(" + __webpack_require__(820) + ");\n  background-size: 100% 100%;\n  background-position: center;\n  height:100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n}\n.inner-bg h2{\n  font-size: 26px;\n  font-weight: bold;\n  color: #f7d761;\n  margin: 0px 0px 20px 0px;\n}\n.inner-bg p{\n   font-size: 14px;\n  font-weight: normal;\n  color: #ffffff;\n}\n.inner-bottom{\n  margin-bottom: 60px;\n}\n@media screen and (max-width: 600px){\n\t.text-center-mobile {\n\t\ttext-align: center;\n\t}\n\t.headerStyle {\n\t\tfont-size: 20px;\n\t}\n}\n@media screen and (min-width: 768px){\n\t.hero .carousel {\n\t\theight: 80vh;\n\t}\t\n    .enso-logo {\n        width: 210px;\n\t}\n\t.testimonial-img{\n\t\tpadding-top: 18px;\n\t\t-webkit-transform: skewY(-1deg);\n\t\t        transform: skewY(-1deg);\n\t\twidth: 284px;\n\t}\n}\n@media screen and (min-width: 1200px){\n\t.summary-icon {\n\t\twidth: 12%;\n\t}\n\t.leftRs {\n\t\tleft: -30px;\n\t}\n\n\t.rightRs {\n\t\tright: -30px;\n\t}\n}\n\n@media screen and (max-width: 767px){\n\t.i-carousel-wrapper {\n\t\tmargin-bottom: 30px;\n\t}\n\t.i-carousel {\n\t\theight: 40vh;\n\t}\n\t.hero .carousel {\n\t\theight: 40vh;\n\t}\n\t.hero {\n\t\tmin-height: 40vh;\n\t}\n\t.main-carousel {\n\t\theight: 35vh;\n\t}\n\t.ct-header p {\n\t\tfont-size: 12px;\n\t\tline-height: 1.2;\n\t\tmargin: 20px 0;\n\t}\n    .subHeading {\n        font-size: 14px;\n\t\tdisplay: block;\n\t\tmargin-bottom: 20px;\n    }\t\n    .enso-logo {\n        width: 150px;\n        height: 45px;\n\t}\n\t.product-slider { padding: 15px; }\n\t.headings2,.headings {\n        font-size: 20px;\n\t}\n\t.caseStudy {\n\t\tmargin-top: 50px;\n\t}\n\t.btn-custom{\n\t\tfont-size: 12px;\n\t}\n\t.slider-inner h3 {\n\t\tfont-size: 18px;\n\t}\n\t.customCaseStudy-mobile{\n\t\tbackground-size: 100% 100%;\n\t\tmargin-top: -20px;\n\t\tbackground-repeat: no-repeat;\n\t}\n\t.img-testimonial-section {\n\t\tposition: relative;\n\t}\n\t.img-testimonial-section .img-wrapper{\n\t\tposition: absolute;\n\t\tbottom: 6px;\n\t\tright: 20%;\n\t}\n\t.slider-wrapper {\n\t\theight: 250px;\n\t\tdisplay: -webkit-box;\n\t\tdisplay: -ms-flexbox;\n\t\tdisplay: flex;\n\t}\n\t.swiper-pagination {\n\t\tbottom: 25px;\n\t}\n\t.slider-inner {\n\t\tpadding-right: 0;\n\t}\n\t.testimonial-img{\n\t\twidth: 100px;\n\t\tpadding-top: 18px;\n\t}\n}\n.slideInLeft {\n  -webkit-animation-name: slideInLeft;\n  animation-name: slideInLeft;\n  -webkit-animation-duration: 0.3s;\n  animation-duration: 0.3s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  }\n  @-webkit-keyframes slideInLeft {\n  0% {\n  -webkit-transform: translateX(-70%);\n  transform: translateX(-70%);\n  visibility: visible;\n  }\n  100% {\n  -webkit-transform: translateX(0);\n  transform: translateX(0);\n  }\n  }\n  @keyframes slideInLeft {\n  0% {\n  -webkit-transform: translateX(-70%);\n  transform: translateX(-70%);\n  visibility: visible;\n  }\n  100% {\n  -webkit-transform: translateX(0);\n  transform: translateX(0);\n  }\n  }\n.slideInRight {\n  -webkit-animation-name: slideInRight;\n  animation-name: slideInRight;\n  -webkit-animation-duration: 0.3s;\n  animation-duration: 0.3s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  }\n  @-webkit-keyframes slideInRight {\n  0% {\n  -webkit-transform: translateX(70%);\n  transform: translateX(70%);\n  visibility: visible;\n  }\n  100% {\n  -webkit-transform: translateX(0);\n  transform: translateX(0);\n  }\n  }\n  @keyframes slideInRight {\n  0% {\n  -webkit-transform: translateX(100%);\n  transform: translateX(100%);\n  visibility: visible;\n  }\n  100% {\n  -webkit-transform: translateX(0);\n  transform: translateX(0);\n  }\n  }\n@media screen and (min-width: 768px) and (max-width: 1024px){\n\t.hero {\n\t\ttop: -80px;\n\t}\n\t.row-eq-height{\n\t\tdisplay: -webkit-box;\n\t\tdisplay: -ms-flexbox;\n\t\tdisplay: flex;\n\t\t-webkit-box-align: center;\n\t\t    -ms-flex-align: center;\n\t\t        align-items: center;\n\t}\n\n\t.headerStyle {\n\t\tfont-size: 30px;\n\t}\n\t.main-carousel {\n\t\theight: 30vh;\n\t}\n\t.i-carousel {\n\t\theight: 35vh;\n\t}\n\t.hero {\n\t\tmin-height: 40vh;\n\t}\n}\n@media screen and (min-width: 1025px){\n\t.hero {\n\t\ttop: -80px;\n\t}\n\t.row-eq-height{\n\t\tdisplay: -webkit-box;\n\t\tdisplay: -ms-flexbox;\n\t\tdisplay: flex;\n\t\t-webkit-box-align: center;\n\t\t    -ms-flex-align: center;\n\t\t        align-items: center;\n\t}\n\n\t.main-carousel {\n\t\theight: 75vh;\n\t}\n\t.i-carousel {\n\t\theight: 80vh;\n\t}\n\t.hero {\n\t\tmin-height: 84vh;\n\t}\n}\n\n\n.row{\n\tmargin-left:0px;\n\tmargin-right:0px;\n}\n.carousel-section {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.carousel-section img {\n    width: 60%;\n}\n.bannerStyle h1 {\n    background-color: #ccc;\n    min-height: 300px;\n    text-align: center;\n    line-height: 300px;\n}\n.leftRs {\n    position: absolute;\n    top: 30%;\n    bottom: 0;\n    width: 50px;\n    height: 50px;\n    border: none;\n    padding: 0;\n    box-shadow: none;\n    border-radius: 50%;\n\tleft: 0;\n\tbackground-color: transparent;\n}\n\n.leftRs img , .rightRs img{\n\twidth: 50px;\n    height: 50px;\n}\n.rightRs {\n\tposition: absolute;\n    top: 30%;\n    bottom: 0;\n    width: 50px;\n    height: 50px;\n    border: none;\n    padding: 0;\n    box-shadow: none;\n    border-radius: 50%;\n    right: 0;\n\tbackground-color: transparent;\n}\n.carousel-description {\n\tword-wrap: break-word;\n\tdisplay: inline-block;\n\twidth: 90%;\n\tline-height: 1.5;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 812:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!pageContent\" class=\"container content-spinner\">\n\t<div class=\"col-md-12 loader\">\n\t\t<img class=\"\" src=\"../../../assets/images/content_spinner.gif\"/>\n\t</div>\n</div>\n<div *ngIf=\"pageContent\" [attr.data-wio-id]=\"pageContent.id\">\n  <section class=\"hero homePageBg\">\n    <div class=\"container main-container\" style=\"height:100%\">\n      <div class=\"row flex-div\">\n        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\"></div>\n        <div class=\"col-md-8 col-lg-8 col-md-8 col-xs-8\" style=\"padding-left:50px;\">\n          <div class=\"hero-description\" style=\"margin-top:150px;\">\n            <h3 class=\"hero-mainText\">Become an AI-First Enterprise with <span class=\"middleText\">Continuous Intelligence</span></h3>\n            <br>\n            <h5 class=\"hero-subText\">The world's leading companies are using Enso - a suite of Intelligent Automation products from Exponential Machines to build solutions that leverage AI for driving transformational growth.</h5>\n            <br>\n            <button class=\"btn btn-connect\" style=\"margin-top:20px;\">Learn more <span class=\"fa fa-chevron-right\"></span> </button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"clients\">\n    <div class=\"ct-slick-homepage h-100\">\n      <swiper [config]=\"config\" class=\"h-100\">\n        <div class=\"swiper-wrapper h-100\">\n          <div class=\"ct-header tablex item swiper-slide h-100\" *ngFor=\"let list of carouselData.listitems\">\n            <div class=\"ct-u-display-tablex h-100\">\n              <div class=\"inner h-100\">\n\n                  <div class=\"row flex-div-center h-100\">\n                    <div class=\"col-md-8 col-lg-8 col-sm-8 col-xs-8 slider-inner h-100\">\n                      <div style=\"font-size:16px;color:white;padding:20px 15px 0px 15px;\">\n                        \"{{list.content[0].text}}\"\n                      </div>\n                    </div>\n                    <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding h-100\">\n                      <div class=\"poly\">\n                        <div class=\"designationWidth\">\n                            <p>\n                             {{list.designation[0].text}}\n                           </p>\n                           <p>\n                             {{list.company[0].text}}\n                           </p>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"swiper-pagination\"></div>\n      </swiper>\n    </div>\n    <!--<div id=\"\" class=\"carousel slide h-100 myCarousel\" data-ride=\"carousel\">-->\n      <!--<div class=\"carousel-inner h-100\">-->\n          <!--<div class=\"item h-100\" *ngFor=\"let data of carouselData.listitems\">-->\n            <!--<div class=\"row flex-div-center  h-100\">-->\n              <!--<div class=\"col-lg-8 col-md-8 col-sm-8 h-100 \">-->\n                  <!--<div style=\"font-size:16px;color:white;padding:20px 15px 0px 15px;\">-->\n                    <!--“{{data.content[0].text}}”-->\n                  <!--</div>-->\n              <!--</div>-->\n              <!--<div class=\"col-lg-4 col-md-4 col-sm-4 no-padding h-100\">-->\n                  <!--<div class=\"poly\">-->\n                     <!--<p>-->\n                       <!--{{data.designation[0].text}}-->\n                     <!--</p>-->\n                     <!--<p>-->\n                       <!--{{data.company[0].text}}-->\n                     <!--</p>-->\n                  <!--</div>-->\n              <!--</div>-->\n            <!--</div>-->\n          <!--</div>-->\n        <!--<ol class=\"carousel-indicators\">-->\n          <!--<li data-target=\".myCarousel\" data-slide-to=\"0\" class=\"active\"></li>-->\n          <!--<li data-target=\".myCarousel\" data-slide-to=\"1\"></li>-->\n        <!--</ol>-->\n      <!--</div>-->\n    <!--</div>-->\n  </section>\n\t<!--<div class=\"row row-eq-height customBackground\"  [ngStyle]=\"{'background-image': 'url(' + summaryData[0].summarybackground.url + ')'}\">-->\n\t\t<!--<div class=\"col-lg-7 col-sm-7 col-md-7 col-xs-12 hidden-xs  wow fadeInLeft\">-->\n\t\t\t<!--<img alt={{summaryData[0].ensoplatformplaceholder.alt}} src={{summaryData[0].ensoplatformplaceholder.url}} class=\"img-responsive\"/>-->\n\t\t<!--</div>-->\n\t\t<!--<div class=\"col-lg-5 col-sm-5 col-md-5 col-xs-12 wow fadeInLeft\">-->\n\t\t\t<!--<div class=\"row text-center-mobile\">-->\n\t\t\t\t<!--<div class=\"hidden-xs\">-->\n\t\t\t\t\t<!--<a class=\"\">-->\n\t\t\t\t\t\t<!--<img class=\"enso-logo\" alt={{summaryData[0].ensologo.alt}} src={{summaryData[0].ensologo.url}}/>-->\n\t\t\t\t\t<!--</a>-->\n\t\t\t\t<!--</div>-->\n\t\t\t\t<!--<div class=\"hidden-sm hidden-lg hidden-md\">-->\n\t\t\t\t\t<!--<div class=\"row\">-->\n\t\t\t\t\t\t<!--<div class=\"col-xs-12 no-padding\">-->\n\t\t\t\t\t\t\t<!--<img class=\"enso-logo\" alt={{summaryData[0].ensologo.alt}} src={{summaryData[0].ensologo.url}}/>-->\n\t\t\t\t\t\t\t<!--<label class=\"subHeading\">{{summaryData[0].ensotitle[0].text}}</label>-->\n\t\t\t\t\t\t\t<!--<img alt={{summaryData[0].ensoplatformplaceholder.alt}} src={{summaryData[0].ensoplatformplaceholder.url}} class=\"img-responsive\"/>-->\n\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--</div>-->\n\t\t\t\t<!--</div>-->\n\t\t\t\t<!--<br>-->\n\t\t\t\t<!--<label class=\"subHeading hidden-xs\">{{summaryData[0].ensotitle[0].text}}</label>-->\n\t\t\t<!--</div>-->\n\t\t\t<!--<div class=\"row custom-top\">-->\n\t\t\t\t<!--<div class=\"col-lg-10 col-sm-10 col-xs-12 col-md-10 no-padding\">-->\n\t\t\t\t\t<!--<div><b>{{summaryData[0].ensoheading[0].text}}</b></div>-->\n\t\t\t\t\t<!--<div class=\"custom-top\">{{summaryData[0].ensodata[0].text}}</div>-->\n\t\t\t\t<!--</div>-->\n\t\t\t<!--</div>-->\n\t\t\t<!--<div class=\"row custom-top\">-->\n\t\t\t\t<!--<div class=\"col-lg-10 col-sm-10 col-xs-12 col-md-10 no-padding\">-->\n\t\t\t\t\t<!--<div class=\"enso-feature-subheading\"><b>{{summaryData[0].ensosubheading[0].text}}</b></div>-->\n\t\t\t\t\t<!--<div class=\"custom-top\">-->\n\t\t\t\t\t\t<!--<div class=\"row\">-->\n\t\t\t\t\t\t  <!--<div class=\"col-lg-2 col-sm-3 col-md-2 col-xs-2 no-padding summary-icon\">-->\n\t\t\t\t\t\t\t<!--<img alt={{summaryData[0].ensosubheadingicon1.alt}} src={{summaryData[0].ensosubheadingicon1.url}} class=\"img-responsive iconStyle\"/>-->\n\t\t\t\t\t\t  <!--</div>-->\n\t\t\t\t\t\t  <!--<div class=\"col-lg-10 col-md-10 col-sm-9 col-xs-10\">-->\n\t\t\t\t\t\t\t<!--<b>{{summaryData[0].ensosubheadingtitle1[0].text}}</b>-->\n\t\t\t\t\t\t\t<!--{{summaryData[0].ensosubheadingdata[0].text}}-->\n\t\t\t\t\t\t  <!--</div>-->\n\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--<div class=\"custom-top\">-->\n\t\t\t\t\t\t<!--<div class=\"row\">-->\n\t\t\t\t\t\t  <!--<div class=\"col-lg-2 col-sm-3 col-md-2 col-xs-2 no-padding summary-icon\">-->\n\t\t\t\t\t\t\t<!--<img alt={{summaryData[0].ensosubheadingicon2.alt}} src={{summaryData[0].ensosubheadingicon2.url}} class=\"img-responsive iconStyle\"/>-->\n\t\t\t\t\t\t  <!--</div>-->\n\t\t\t\t\t\t  <!--<div class=\"col-lg-10 col-md-10 col-sm-9 col-xs-10\">-->\n\t\t\t\t\t\t\t<!--<b>{{summaryData[0].ensosubheadingtitle2[0].text}}</b>-->\n\t\t\t\t\t\t\t<!--{{summaryData[0].ensosubheadingdata2[0].text}}-->\n\t\t\t\t\t\t  <!--</div>-->\n\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--<div class=\"custom-top\">-->\n\t\t\t\t\t\t<!--<div class=\"row\">-->\n\t\t\t\t\t\t  <!--<div class=\"col-lg-2 col-sm-3 col-md-2 col-xs-2 no-padding summary-icon\">-->\n\t\t\t\t\t\t\t<!--<img alt={{summaryData[0].ensosubheadingicon3.alt}} src={{summaryData[0].ensosubheadingicon3.url}} class=\"img-responsive iconStyle\"/>-->\n\t\t\t\t\t\t  <!--</div>-->\n\t\t\t\t\t\t  <!--<div class=\"col-lg-10 col-md-10 col-sm-9 col-xs-10\">-->\n\t\t\t\t\t\t\t<!--<b>{{summaryData[0].ensosubheadingtitle3[0].text}}</b>-->\n\t\t\t\t\t\t\t<!--{{summaryData[0].ensosubheadingdata3[0].text}}-->\n\t\t\t\t\t\t  <!--</div>-->\n\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--<div class=\"custom-top\">-->\n\t\t\t\t\t\t<!--<div class=\"row\">-->\n\t\t\t\t\t\t  <!--<div class=\"col-lg-2 col-sm-3 col-md-2 col-xs-2 no-padding summary-icon\">-->\n\t\t\t\t\t\t\t<!--<img alt={{summaryData[0].ensosubheadingicon4.alt}} src={{summaryData[0].ensosubheadingicon4.url}} class=\"img-responsive iconStyle\"/>-->\n\t\t\t\t\t\t  <!--</div>-->\n\t\t\t\t\t\t  <!--<div class=\"col-lg-10 col-md-10 col-sm-9 col-xs-10\">-->\n\t\t\t\t\t\t\t<!--<b>{{summaryData[0].ensosubheadingtitle4[0].text}}</b>-->\n\t\t\t\t\t\t\t<!--{{summaryData[0].ensosubheading4[0].text}}-->\n\t\t\t\t\t\t  <!--</div>-->\n\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--</div>-->\n\t\t\t\t<!--</div>-->\n\t\t\t<!--</div>-->\n\t\t<!--</div>-->\n\t<!--</div>-->\n\t<section class=\"intelligence\" style=\"margin-bottom:-130px;\">\n    <div class=\"container-fluid product-slider\">\n        <h3 class=\"headings\">{{featureShortData.fieldtitle[0].text}}</h3>\n        <div class=\"row features\">\n          <div class=\"col-lg-3 col-md-3 col-sm-6 feature-wrapper col-xs-12 \"  *ngFor=\"let data of featureShortData.platformintelligence\">\n            <div class=\"text-center\">\n              <img class=\"image-style wow fadeInUp \"  src={{data.logo.url}}/>\n            </div>\n            <br>\n            <h4 style=\"font-size:18px;\">\n              <b>{{data.header[0].text}}</b>\n            </h4>\n            <label style=\"margin-bottom:15px;height:35px;font-size:12px;\">\n              <b>{{data.subheader[0].text}}</b>\n            </label>\n            <br>\n            <span style=\"font-size:14px;color:#4a4a4a;text-align:left;\">{{data.descirption[0].text}}</span>\n          </div>\n        </div>\n        <!--<div class=\"row text-center sectionTop hidden-xs\" style=\"margin-bottom: 100px\">-->\n          <!--<div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12\">-->\n            <!--<button class=\"btn btn-custom\">-->\n              <!--{{summaryData[0].requestdemobtn[0].text}}-->\n              <!--&lt;!&ndash; <i class=\"fas fa-chevron-right\"></i> &ndash;&gt;-->\n              <!--<img src=\"../../../assets/images/right-arrow.png\" />-->\n            <!--</button>-->\n            <!--&lt;!&ndash; <button class=\"btn btn-enso\">-->\n                <!--{{summaryData[0].demobtn[0].text}}-->\n              <!--<i class=\"fas fa-chevron-right\"></i>-->\n            <!--</button> &ndash;&gt;-->\n          <!--</div>-->\n        <!--</div>-->\n    </div>\n\t</section>\n\t<div class=\"row caseStudy\" [ngStyle]=\"{'background-image': 'url(' + dataSheetData.downloadbg.url + ')'}\" style=\"padding-top:100px;\">\n\t\t<div class=\"col-lg-12 col-sm-12 col-xs-12 col-md-12 no-padding\">\n\t\t\t<!--<div class=\"hidden-xs ct-header ct-header-slider ct-slick-custom-dots customCaseStudy\" [ngStyle]=\"{'background-image': 'url(' + testimonialData.background.url + ')'}\">-->\n\t\t\t\t<!--<div class=\"ct-slick-homepage\">-->\n\t\t\t\t\t<!--<swiper [config]=\"config\">-->\n\t\t\t\t\t\t<!--<div class=\"swiper-wrapper\">-->\n\t\t\t\t\t\t\t<!--<div class=\"ct-header tablex item swiper-slide\" *ngFor=\"let list of testimonialData.hptestimonial\">-->\n\t\t\t\t\t\t\t\t<!--<div class=\"ct-u-display-tablex\">-->\n\t\t\t\t\t\t\t\t\t<!--<div class=\"inner\">-->\n\t\t\t\t\t\t\t\t\t\t<!--<div class=\"container\">-->\n\t\t\t\t\t\t\t\t\t\t\t<!--<div class=\"row row-eq-height\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t<!--<div class=\"col-md-8 col-lg-8 col-sm-8 col-xs-8 slider-inner wow fadeInRight\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<label class=\"\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!--{{list.carouselheading[0].text}}-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--</label>-->\n                          <!--<h2>-->\n                            <!--{{list.subheader[0].text}}-->\n                          <!--</h2>-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<p class=\"\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!--{{list.carouselsubheading[0].text}}-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--</p>-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<span class=\"\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<b>{{list.carouseldesignation[0].text}}</b>-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--</span>-->\n\t\t\t\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t\t\t\t\t<!--&lt;!&ndash;<div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4 wow fadeInLeft\" style=\"height: 410px\">&ndash;&gt;-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--&lt;!&ndash;<img alt={{list.carosuelimage.alt}} src={{list.carosuelimage.url}} class=\"testimonial-img\"/>&ndash;&gt;-->\n\t\t\t\t\t\t\t\t\t\t\t\t<!--&lt;!&ndash;</div>&ndash;&gt;-->\n\t\t\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t<!--<div class=\"swiper-pagination\"></div>-->\n\t\t\t\t\t<!--</swiper>-->\n\t\t\t\t<!--</div>-->\n\t\t\t<!--</div>-->\n\t\t\t<!--<div class=\"hidden-md hidden-lg hidden-sm ct-header ct-header-slider ct-slick-custom-dots customCaseStudy-mobile\" [ngStyle]=\"{'background-image': 'url(' + testimonialData.bgmobile.url + ')'}\">-->\n\t\t\t\t<!--<div class=\"ct-slick-homepage\">-->\n\t\t\t\t\t<!--<swiper [config]=\"config\">-->\n\t\t\t\t\t\t<!--<div class=\"swiper-wrapper\">-->\n\t\t\t\t\t\t\t<!--<div class=\"ct-header tablex item swiper-slide\" *ngFor=\"let list of testimonialData.hptestimonial\">-->\n\t\t\t\t\t\t\t\t<!--<div class=\"ct-u-display-tablex-mobile\">-->\n\t\t\t\t\t\t\t\t\t<!--<div class=\"inner\">-->\n\t\t\t\t\t\t\t\t\t\t<!--<div class=\"container no-padding\">-->\n\t\t\t\t\t\t\t\t\t\t\t<!--<div class=\"row slider-wrapper\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t<!--<div class=\"col-xs-8 slider-inner\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<h3 class=\"\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!--{{list.carouselheading[0].text}}-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--</h3>-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<p class=\"\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!--{{list.carouselsubheading[0].text}}-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--</p>-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<span class=\"\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<b>{{list.carouseldesignation[0].text}}</b>-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--</span>-->\n\t\t\t\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t\t\t\t\t<!--<div class=\"col-xs-4 no-padding img-testimonial-section\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<div class=\"img-wrapper\">-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!--<img alt={{list.carosuelimage.alt}} src={{list.carosuelimage.url}} class=\"testimonial-img\"/>-->\n\t\t\t\t\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t<!--<div class=\"swiper-pagination\"></div>-->\n\t\t\t\t\t<!--</swiper>-->\n\t\t\t\t<!--</div>-->\n\t\t\t<!--</div>-->\n      <div class=\"container\">\n        <h3 class=\"headings2\">{{industryData.industrytitle[0].text}}</h3>\n      </div>\n\n\t\t\t<div class=\"row wow fadeInUp\" style=\"padding:60px;\">\n\t\t\t\t<div class=\"col-lg-3 col-sm-6 col-md-3 col-xs-12\"  *ngFor=\"let list of industryData.industrylist\" style=\"height:100%;\">\n          <div class=\"overlay\">\n            <img src=\"{{list.imagelist.url}}\" class=\"img-responsive\" style=\"width:80%;margin:0 auto;\"/>\n            <div class=\"role\">\n              <label class=\"btn-value\">{{list.buttontext[0].text}}</label>\n              <p style=\"color:white;font-size:20px;\">{{list.title[0].text}}</p>\n            </div>\n          </div>\n          <!--<img src=\"{{list.imagelist.url}}\" class=\"img-responsive\"/>-->\n\t\t\t\t\t<!--<div class=\"product-slider\">-->\n\t\t\t\t\t\t<!--<ngu-carousel-->\n\t\t\t\t\t\t\t<!--[inputs]=\"dataSheetCarousel\">-->\n\t\t\t\t\t\t\t<!--<ngu-item NguCarouselItem *ngFor=\"let c1 of dataSheetData.datasheets1\">-->\n\t\t\t\t\t\t\t\t<!--<div class=\"carousel-section\">-->\n\t\t\t\t\t\t\t\t\t<!--<img alt={{c1.datasheetimage.alt}} src={{c1.datasheetimage.url}}/>-->\n\t\t\t\t\t\t\t\t\t<!--<p class=\"technology-subtitle\">{{c1.datasheettext[0].text}}</p>-->\n\t\t\t\t\t\t\t\t\t<!--<button class=\"btn btn-download\">{{dataSheetData.downloadlabel[0].text}}</button>-->\n\t\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t<!--</ngu-item>-->\n\t\t\t\t\t\t\t<!--<button NguCarouselPrev class='leftRs'>-->\n\t\t\t\t\t\t\t\t<!--<img src=\"../../assets/images/left-chevron.png\" class=\"image-style\"/>-->\n\t\t\t\t\t\t\t<!--</button>-->\n\t\t\t\t\t\t\t<!--<button NguCarouselNext class='rightRs'>-->\n\t\t\t\t\t\t\t\t<!--<img src=\"../../assets/images/right-chevron.png\" class=\"image-style\"/>-->\n\t\t\t\t\t\t\t<!--</button>-->\n\t\t\t\t\t\t<!--</ngu-carousel>-->\n\t\t\t\t\t<!--</div>-->\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n  <div class=\"row\">\n    <section class=\"enso\">\n      <div class=\"row\">\n        <div class=\"col-lg-6 col-md-6 col-sm-6 no-padding\" *ngFor=\"let tile of ensoData.ensotile\">\n          <div class=\"ef-bg\">\n            <div class=\"inner-bg\" [ngStyle]=\"{'background-image': 'url(' + tile.enso1.url + ')'}\">\n              <div class=\"container\">\n                <div class=\"row\">\n                  <div class=\"col-lg-9 col-lg-offset-1 inner-bottom\">\n                     <h2 class=\"enso-heading\">{{tile.title[0].text}}</h2>\n                     <p>{{tile.subheading[0].text}}</p>\n                  </div>\n                </div>\n              </div>\n\n            </div>\n          </div>\n        </div>\n      </div>\n    </section>\n  </div>\n\t<div class=\"row \">\n\t\t<section class=\"efficiency\" #demo>\n\t\t\t<div class=\"row wow fadeInUp\">\n\t\t\t\t<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12 no-padding text-center\">\n\t\t\t\t\t<h4 class=\"efficiencyText\">{{efficiencyData.maintitle[0].text}}</h4>\n          <br>\n\t\t\t\t\t<button class=\"btn btn-connect\" routerLink=\"/getstarted\" >{{efficiencyData.getstartedlabel[0].text}} <span class=\"fa fa-chevron-right\"></span></button>\n          <br>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\t</div>\n</div>\n"

/***/ }),

/***/ 820:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "enso-bg1.bbcdb5826dd375876e38.png";

/***/ }),

/***/ 821:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "homepageBackground.8eae1fcb7e335f97bbd3.png";

/***/ })

});
//# sourceMappingURL=0.chunk.js.map