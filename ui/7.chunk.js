webpackJsonp([7,13],{

/***/ 781:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__solutions_component__ = __webpack_require__(800);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__prismic_prismic_service__ = __webpack_require__(137);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SolutionsModule", function() { return SolutionsModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: "",
        component: __WEBPACK_IMPORTED_MODULE_3__solutions_component__["a" /* SolutionsComponent */]
    }
];
var SolutionsModule = (function () {
    function SolutionsModule() {
    }
    return SolutionsModule;
}());
SolutionsModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forChild(routes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__solutions_component__["a" /* SolutionsComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_4__prismic_prismic_service__["a" /* PrismicService */]]
    })
], SolutionsModule);

//# sourceMappingURL=solutions.module.js.map

/***/ }),

/***/ 800:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prismic_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_wow__ = __webpack_require__(302);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SolutionsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var SolutionsComponent = (function () {
    function SolutionsComponent(router, prismic, route, titleService, _headerService, meta, wowService) {
        this.router = router;
        this.prismic = prismic;
        this.route = route;
        this.titleService = titleService;
        this._headerService = _headerService;
        this.meta = meta;
        this.wowService = wowService;
        this.PrismicDOM = __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default.a;
        this.toolbar = false;
        this.wowService.init();
    }
    SolutionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeStream = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].fromPromise(this.prismic.buildContext())
            .subscribe(function (ctx) {
            _this.ctx = ctx;
            _this.fetchPage();
        });
        this.subscription = this._headerService.currentSection.subscribe(function (info) { return _this.sectionInfo = info; });
    };
    SolutionsComponent.prototype.ngOnDestroy = function () {
        this.routeStream.unsubscribe();
        this.subscription.unsubscribe();
    };
    SolutionsComponent.prototype.ngAfterViewChecked = function () {
        if (this.digitalization && this.digitalization.nativeElement && (this.sectionInfo.section !== '')) {
            this.scrollToSection(this.sectionInfo);
            this.sectionInfo = { section: '' };
        }
        else if (this.ctx && !this.toolbar) {
            // this.prismic.toolbar(this.ctx.api);
            this.toolbar = true;
            window.scrollTo(0, 0);
        }
    };
    SolutionsComponent.prototype.scrollOffsetForFixedHeader = function () {
        var scrolledY = window.scrollY;
        if (scrolledY) {
            window.scroll(0, scrolledY - 160);
        }
    };
    SolutionsComponent.prototype.scrollToSection = function (obj) {
        var _this = this;
        setImmediate(function () {
            if (obj.section === 'digitalization') {
                _this.digitalization.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            else if (obj.section === 'decisionautomation') {
                _this.decision.nativeElement.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'start' });
            }
            _this.scrollOffsetForFixedHeader();
        });
    };
    SolutionsComponent.prototype.fetchPage = function () {
        var _this = this;
        this.ctx.api.query([
            __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default.a.Predicates.any('document.type', ['solution']),
        ])
            .then(function (response) {
            _this.toolbar = false;
            _this.pageContent = response.results;
            var metaDescription = "";
            var metaKeyWords = [];
            _this.pageContent.map(function (prop) {
                if (prop.uid === 'digitization') {
                    _this.solutionsData = prop.data;
                    metaDescription = _this.solutionsData['meta-description'];
                    metaKeyWords.push(_this.solutionsData['meta-title']);
                }
            });
            _this.meta.updateTag({ name: 'description', content: metaDescription });
            _this.meta.updateTag({ name: 'author', content: 'Xpms' });
            _this.meta.updateTag({ name: 'keywords', content: metaKeyWords.toString() });
        })
            .catch(function (e) { return console.log(e); });
    };
    return SolutionsComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('digitalization'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], SolutionsComponent.prototype, "digitalization", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('decision'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], SolutionsComponent.prototype, "decision", void 0);
SolutionsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-solutions',
        template: __webpack_require__(817),
        styles: [__webpack_require__(809)]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__shared_services_header_service__["a" /* HeaderService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* Meta */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ngx_wow__["b" /* NgwWowService */]) === "function" && _j || Object])
], SolutionsComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j;
//# sourceMappingURL=solutions.component.js.map

/***/ }),

/***/ 809:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, ".contentMargin{\n  margin-top:100px;\n}\n\n.custom-top{\n    margin-top:25px;\n}\n\n\n.customEnsoList ul {\n    padding: 15px;\n    list-style-type:square;\n}\n.customEnsoList li{\n\tcolor:#f7d761;\n\tfont-size: 15px;\n}\n\n.customEnsoList li span{\n\tcolor:black;\n\tfont-size: 14px;\n}\n\n.heavyMargin{\n  margin-top:100px;\n  margin-bottom: 100px;\n}\n.row{\n\tmargin-left:0px;\n\tmargin-right:0px;\n}\n.sectionHeading-future {\n  color: #25aae1;\n  font-weight: 600;\n  padding-left: 10px;\n}\n\n.customEnsoList label {\n  text-transform: uppercase;\n}\n.process-label {\n  text-transform: uppercase;\n}\n@media screen and (min-width: 768px) {\n  .solutionHeading{\n    color: #25aae1;\n    font-weight: 600;\n  }\n}\n@media screen and (max-width: 767px) {\n  .img-future-mobile {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    padding-bottom: 20px;\n  }\n  .img-future-mobile img {\n    width: 50%;\n    height: 50%;\n  }\n  .solutionHeading{\n    text-align: center;\n    font-weight: 600;\n    text-transform: uppercase;\n    font-size: 20px;\n  }\n  .contentMargin {\n    margin-top: 50px;\n  }\n  .heavyMargin{\n    margin-top:50px;\n    margin-bottom: 50px;\n  }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 817:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!pageContent\" class=\"container content-spinner\">\n\t<div class=\"col-md-12 loader\">\n\t\t<img class=\"\" src=\"../../../assets/images/content_spinner.gif\"/>\n\t</div>\n</div>\n<div *ngIf=\"pageContent\" [attr.data-wio-id]=\"pageContent.id\">\n  <div class=\"container-fluid row contentMargin no-padding\">\n    <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding\">\n      <img alt={{solutionsData.solutionimage.alt}} src={{solutionsData.solutionimage.url}} height=\"350px\" width=\"75%\" class=\"img-responsive hidden-xs wow fadeInRight\"/>\n      <div class=\"img-future-mobile hidden-md hidden-sm hidden-lg\">\n          <img alt={{solutionsData.solutionimage.alt}} src={{solutionsData.solutionimage.url}} class=\"img-responsive\"/>\n          <h2 class=\"sectionHeading-future\">{{solutionsData.maintitle[0].text}}</h2>\n      </div>\n      </div>\n    <div class=\"col-lg-7 col-md-7 col-sm-7 col-xs-12 wow fadeInLeft\">\n      <section id=\"digitization\" #digitalization>\n        <h2 class=\"solutionHeading\">{{solutionsData.solutionfields[0].solutiontitle[0].text}}</h2>\n        <div class=\"custom-top\">\n            {{solutionsData.solutionfields[0].solutionsummary[0].text}}\n        </div>\n        <div class=\"row custom-top\">\n          <div  class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 customEnsoList no-padding\">\n            <label>{{solutionsData.solutiondata[0].solutiondatatitle[0].text}}</label>\n            <ul >\n              <li class=\"wow fadeInLeft\">\n                <span>{{solutionsData.solutiondata[0].solutiondatalist1[0].text}}</span>\n              </li>\n              <li class=\"wow fadeInLeft\" data-wow-delay=\"0.3s\">\n                <span>{{solutionsData.solutiondata[0].solutiondatalist2[0].text}}</span>\n              </li>\n              <li class=\"wow fadeInLeft\" data-wow-delay=\"0.6s\">\n                <span>{{solutionsData.solutiondata[0].solutiondatalist3[0].text}}</span>\n              </li>\n            </ul>\n          </div>\n          <div  class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 customEnsoList no-padding\">\n            <label>{{solutionsData.solutiondata[1].solutiondatatitle[0].text}}</label>\n            <ul  >\n              <li class=\"wow fadeInLeft\">\n                <span>{{solutionsData.solutiondata[1].solutiondatalist1[0].text}}</span>\n              </li>\n              <li class=\"wow fadeInLeft\" data-wow-delay=\"0.3s\">\n                <span>{{solutionsData.solutiondata[1].solutiondatalist2[0].text}}</span>\n              </li>\n              <li class=\"wow fadeInLeft\" data-wow-delay=\"0.6s\">\n                <span>{{solutionsData.solutiondata[1].solutiondatalist3[0].text}}</span>\n              </li>\n            </ul>\n          </div>\n        </div>\n        <br>\n        <label class=\"process-label\">{{solutionsData.processlabel[0].text}}</label>\n        <div class=\"custom-top\">\n          <img alt={{solutionsData.solutiondata[0].solutiondataimage.alt}} src={{solutionsData.solutiondata[0].solutiondataimage.url}} class=\"img-responsive\"/>\n        </div>\n      </section>\n      <section id=\"automation\" #decision class=\"heavyMargin\">\n        <h2 class=\"solutionHeading\">{{solutionsData.solutionfields[1].solutiontitle[0].text}}</h2>\n        <div class=\"custom-top\">\n            {{solutionsData.solutionfields[1].solutionsummary[0].text}}\n        </div>\n        <div class=\"row custom-top\">\n          <div  class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 customEnsoList no-padding\">\n              <label>{{solutionsData.solutiondata[0].solutiondatatitle[0].text}}</label>\n              <ul>\n                <li class=\"wow fadeInLeft\" >\n                  <span>{{solutionsData.solutiondata[0].solutiondatalist1[0].text}}</span>\n                </li>\n                <li class=\"wow fadeInLeft\" data-wow-delay=\"0.3s\">\n                  <span>{{solutionsData.solutiondata[0].solutiondatalist2[0].text}}</span>\n                </li>\n                <li class=\"wow fadeInLeft\" data-wow-delay=\"0.6s\">\n                  <span>{{solutionsData.solutiondata[0].solutiondatalist3[0].text}}</span>\n                </li>\n              </ul>\n          </div>\n          <div  class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12 customEnsoList no-padding\">\n              <label>{{solutionsData.solutiondata[1].solutiondatatitle[0].text}}</label>\n              <ul>\n                <li class=\"wow fadeInLeft\">\n                  <span>{{solutionsData.solutiondata[1].solutiondatalist1[0].text}}</span>\n                </li>\n                <li class=\"wow fadeInLeft\" data-wow-delay=\"0.3s\">\n                  <span>{{solutionsData.solutiondata[1].solutiondatalist2[0].text}}</span>\n                </li>\n                <li class=\"wow fadeInLeft\" data-wow-delay=\"0.6s\">\n                  <span>{{solutionsData.solutiondata[1].solutiondatalist3[0].text}}</span>\n                </li>\n              </ul>\n          </div>\n        </div>\n      </section>\n    </div>\n  </div>\n</div>\t\n\n"

/***/ })

});
//# sourceMappingURL=7.chunk.js.map