webpackJsonp([9,13],{

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__prismic_configuration__ = __webpack_require__(316);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrismicService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PrismicService = (function () {
    function PrismicService(http) {
        this.http = http;
    }
    PrismicService.prototype.buildContext = function () {
        var _this = this;
        var options = {};
        return __WEBPACK_IMPORTED_MODULE_2_prismic_javascript___default.a.api(__WEBPACK_IMPORTED_MODULE_3__prismic_configuration__["a" /* CONFIG */].apiEndpoint, { accessToken: __WEBPACK_IMPORTED_MODULE_3__prismic_configuration__["a" /* CONFIG */].accessToken })
            .then(function (api) {
            return {
                api: api,
                endpoint: __WEBPACK_IMPORTED_MODULE_3__prismic_configuration__["a" /* CONFIG */].apiEndpoint,
                accessToken: __WEBPACK_IMPORTED_MODULE_3__prismic_configuration__["a" /* CONFIG */].accessToken,
                linkResolver: __WEBPACK_IMPORTED_MODULE_3__prismic_configuration__["a" /* CONFIG */].linkResolver,
                toolbar: _this.toolbar,
            };
        })
            .catch(function (e) { return console.log("Error during connection to your Prismic API: " + e); });
    };
    PrismicService.prototype.validateOnboarding = function () {
        var infos = this.getRepositoryInfos();
        var headers = new Headers({ 'Content-Type': 'application/json' });
        if (infos.isConfigured) {
            this.http.post(infos.repoURL + "/app/settings/onboarding/run", null, headers)
                .subscribe(null, function (err) { return console.log("Cannot access your repository, check your api endpoint: " + err); });
        }
    };
    PrismicService.prototype.getRepositoryInfos = function () {
        var repoRegexp = /^(https?:\/\/([-\w]+)\.[a-z]+\.(io|dev))\/api(\/v2)?$/;
        var _a = __WEBPACK_IMPORTED_MODULE_3__prismic_configuration__["a" /* CONFIG */].apiEndpoint.match(repoRegexp), _ = _a[0], repoURL = _a[1], name = _a[2];
        var isConfigured = name !== 'your-repo-name';
        return { repoURL: repoURL, name: name, isConfigured: isConfigured };
    };
    PrismicService.prototype.toolbar = function (api) {
        var maybeCurrentExperiment = api.currentExperiment();
        if (maybeCurrentExperiment) {
            window['PrismicToolbar'].startExperiment(maybeCurrentExperiment.googleId());
        }
        window['PrismicToolbar'].setup(__WEBPACK_IMPORTED_MODULE_3__prismic_configuration__["a" /* CONFIG */].apiEndpoint);
    };
    PrismicService.prototype.preview = function (token) {
        return this.buildContext()
            .then(function (ctx) {
            return ctx.api.previewSession(token, ctx.linkResolver, '/').then(function (url) {
                return {
                    cookieName: __WEBPACK_IMPORTED_MODULE_2_prismic_javascript___default.a.previewCookie,
                    token: token,
                    redirectURL: url
                };
            });
        });
    };
    return PrismicService;
}());
PrismicService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], PrismicService);

var _a;
//# sourceMappingURL=prismic.service.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(7);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return WINDOW; });
/* unused harmony export WindowRef */
/* unused harmony export BrowserWindowRef */
/* unused harmony export windowFactory */
/* unused harmony export browserWindowProvider */
/* unused harmony export windowProvider */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WINDOW_PROVIDERS; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


/* Create a new injection token for injecting the window into a component. */
var WINDOW = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["InjectionToken"]('WindowToken');
/* Define abstract class for obtaining reference to the global window object. */
var WindowRef = (function () {
    function WindowRef() {
    }
    Object.defineProperty(WindowRef.prototype, "nativeWindow", {
        get: function () {
            throw new Error('Not implemented.');
        },
        enumerable: true,
        configurable: true
    });
    return WindowRef;
}());

/* Define class that implements the abstract class and returns the native window object. */
var BrowserWindowRef = (function (_super) {
    __extends(BrowserWindowRef, _super);
    function BrowserWindowRef() {
        return _super.call(this) || this;
    }
    Object.defineProperty(BrowserWindowRef.prototype, "nativeWindow", {
        get: function () {
            return window;
        },
        enumerable: true,
        configurable: true
    });
    return BrowserWindowRef;
}(WindowRef));

/* Create an factory function that returns the native window object. */
function windowFactory(browserWindowRef, platformId) {
    if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_common__["isPlatformBrowser"])(platformId)) {
        return browserWindowRef.nativeWindow;
    }
    return new Object();
}
/* Create a injectable provider for the WindowRef token that uses the BrowserWindowRef class. */
var browserWindowProvider = {
    provide: WindowRef,
    useClass: BrowserWindowRef
};
/* Create an injectable provider that uses the windowFactory function for returning the native window object. */
var windowProvider = {
    provide: WINDOW,
    useFactory: windowFactory,
    deps: [WindowRef, __WEBPACK_IMPORTED_MODULE_1__angular_core__["PLATFORM_ID"]]
};
/* Create an array of providers. */
var WINDOW_PROVIDERS = [
    browserWindowProvider,
    windowProvider
];
//# sourceMappingURL=window.service.js.map

/***/ }),

/***/ 295:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../app/modules/dashboard/dashboard.module": [
		776,
		0
	],
	"../app/modules/enso/enso.module": [
		777,
		1
	],
	"../app/modules/future/future.module": [
		778,
		4
	],
	"../app/modules/get-started/get-started.module": [
		779,
		6
	],
	"../app/modules/open-positions/open-positions.module": [
		780,
		3
	],
	"../app/modules/solutions/solutions.module": [
		781,
		7
	],
	"../app/modules/team/team.module": [
		782,
		2
	],
	"../app/modules/work/work.module": [
		783,
		5
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 295;


/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_hammerjs__ = __webpack_require__(475);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(315);





if (__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prismic_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismic_javascript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modules_shared_services_header_service__ = __webpack_require__(69);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AppComponent = (function () {
    function AppComponent(prismic, route, titleService) {
        this.prismic = prismic;
        this.route = route;
        this.titleService = titleService;
        this.PrismicDOM = __WEBPACK_IMPORTED_MODULE_4_prismic_dom___default.a;
        this.toolbar = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeStream = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].fromPromise(this.prismic.buildContext())
            .subscribe(function (ctx) {
            _this.ctx = ctx;
            _this.fetchPage();
        });
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.routeStream.unsubscribe();
    };
    AppComponent.prototype.ngAfterViewChecked = function () {
        if (this.ctx && !this.toolbar) {
            // this.prismic.toolbar(this.ctx.api);
            this.toolbar = true;
        }
    };
    AppComponent.prototype.fetchPage = function () {
        var _this = this;
        this.ctx.api.query([
            __WEBPACK_IMPORTED_MODULE_5_prismic_javascript___default.a.Predicates.any('document.type', ['subnavigation', 'footernav']),
        ])
            .then(function (response) {
            _this.toolbar = false;
            _this.pageContent = response.results;
            _this.pageContent.map(function (prop) {
                if (prop.uid === 'header') {
                    _this.headerInfo = prop.data;
                }
                else if (prop.uid === 'footernav') {
                    _this.footerData = prop.data;
                }
            });
        })
            .catch(function (e) { return console.log(e); });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(484),
        providers: [__WEBPACK_IMPORTED_MODULE_7__modules_shared_services_header_service__["a" /* HeaderService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__prismic_prismic_service__["a" /* PrismicService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["b" /* Title */]) === "function" && _c || Object])
], AppComponent);

var _a, _b, _c;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__prismic_prismic_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_cookie__ = __webpack_require__(476);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_wow__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__modules_shared_header_header_module__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__modules_shared_footer_footer_module__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__routes__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__modules_shared_services_header_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__modules_shared_services_window_service__ = __webpack_require__(141);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_8__angular_router__["a" /* RouterModule */],
            __WEBPACK_IMPORTED_MODULE_11__routes__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_5_ngx_cookie__["a" /* CookieModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_6_ngx_wow__["a" /* NgwWowModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_9__modules_shared_header_header_module__["a" /* HeaderModule */],
            __WEBPACK_IMPORTED_MODULE_10__modules_shared_footer_footer_module__["a" /* FooterModule */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_0__prismic_prismic_service__["a" /* PrismicService */], __WEBPACK_IMPORTED_MODULE_12__modules_shared_services_header_service__["a" /* HeaderService */], __WEBPACK_IMPORTED_MODULE_13__modules_shared_services_window_service__["a" /* WINDOW_PROVIDERS */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_footer_interface__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_footer_interface___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__models_footer_interface__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_header_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(50);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FooterComponent = (function () {
    function FooterComponent(_headerService, router) {
        this._headerService = _headerService;
        this.router = router;
    }
    FooterComponent.prototype.ngOnInit = function () {
        this.footerData = this.footerInfo;
    };
    FooterComponent.prototype.navigateTo = function (path, section) {
        this._headerService.setSection({ 'section': section });
        this.router.navigate([path]);
    };
    return FooterComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_footer_interface__["NavFooter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_footer_interface__["NavFooter"]) === "function" && _a || Object)
], FooterComponent.prototype, "footerInfo", void 0);
FooterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-footer',
        template: __webpack_require__(485),
        styles: [__webpack_require__(471)]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_header_service__["a" /* HeaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_header_service__["a" /* HeaderService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */]) === "function" && _c || Object])
], FooterComponent);

var _a, _b, _c;
//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__footer_component__ = __webpack_require__(308);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FooterModule = (function () {
    function FooterModule() {
    }
    return FooterModule;
}());
FooterModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__footer_component__["a" /* FooterComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_2__footer_component__["a" /* FooterComponent */]]
    })
], FooterModule);

//# sourceMappingURL=footer.module.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_header_interface__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_header_interface___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__models_header_interface__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_header_service__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_window_service__ = __webpack_require__(141);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var HeaderComponent = (function () {
    function HeaderComponent(router, _headerService, document, window) {
        this.router = router;
        this._headerService = _headerService;
        this.document = document;
        this.window = window;
        this.showWhyEnso = true;
        this.showDigitalization = true;
        this.showCaseStudies = true;
        this.showAboutus = true;
        this.showBlog = true;
        this.showTestimonials = false;
        this.showCustomers = false;
        this.showJoinus = false;
        this.showMeetTeam = false;
        this.showLeadership = false;
        this.showCulture = false;
        this.showMilestone = false;
        this.showContact = false;
        this.showAdvantage = false;
        this.showTechnology = false;
        this.showFeatures = false;
        this.showDatasheet = false;
        this.showDemo = false;
        this.showDecisionAutomation = false;
        this.showSolutionTechnology = false;
        this.showIndustries = false;
        this.showInLab = false;
        this.navIsFixed = false;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.headerData = this.headerInfo;
        this.routerObj = this.router;
        this.bindingVar = 'fadeIn';
    };
    HeaderComponent.prototype.onWindowScroll = function () {
        var number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
        if (number > 200) {
            this.navIsFixed = true;
        }
        else if (this.navIsFixed && number < 5) {
            this.navIsFixed = false;
        }
    };
    HeaderComponent.prototype.navigateTo = function (path, section) {
        this.deactivateOtherTabs();
        this._headerService.setSection({ 'section': section });
        this.router.navigate([path]);
        $('.nav>li.dropdown').removeClass('header-dropdown');
    };
    HeaderComponent.prototype.addHoverToNav = function () {
        if (!$('.nav>li.dropdown').hasClass('header-dropdown'))
            $('.nav>li.dropdown').addClass('header-dropdown');
    };
    HeaderComponent.prototype.deactivateOtherTabs = function () {
        this.showTestimonials = false;
        this.showCustomers = false;
        this.showContact = false;
        this.showJoinus = false;
        this.showMeetTeam = false;
        this.showLeadership = false;
        this.showCulture = false;
        this.showMilestone = false;
        this.showContact = false;
        this.showAdvantage = false;
        this.showTechnology = false;
        this.showFeatures = false;
        this.showDatasheet = false;
        this.showDemo = false;
        this.showDecisionAutomation = false;
        this.showSolutionTechnology = false;
        this.showIndustries = false;
        this.showInLab = false;
        // this.showWhyEnso = false;
    };
    HeaderComponent.prototype.hideDropdown = function () {
        $('.header-dropdown').hide();
    };
    return HeaderComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_header_interface__["NavHeader"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_header_interface__["NavHeader"]) === "function" && _a || Object)
], HeaderComponent.prototype, "headerInfo", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])("window:scroll", []),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], HeaderComponent.prototype, "onWindowScroll", null);
HeaderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-header',
        template: __webpack_require__(486),
        styles: [__webpack_require__(472)]
    }),
    __param(2, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["f" /* DOCUMENT */])),
    __param(3, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_5__services_window_service__["b" /* WINDOW */])),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_header_service__["a" /* HeaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_header_service__["a" /* HeaderService */]) === "function" && _c || Object, Object, Object])
], HeaderComponent);

var _a, _b, _c;
//# sourceMappingURL=header.component.js.map

/***/ }),

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__header_component__ = __webpack_require__(310);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HeaderModule = (function () {
    function HeaderModule() {
    }
    return HeaderModule;
}());
HeaderModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__header_component__["a" /* HeaderComponent */]],
        exports: [__WEBPACK_IMPORTED_MODULE_2__header_component__["a" /* HeaderComponent */]]
    })
], HeaderModule);

//# sourceMappingURL=header.module.js.map

/***/ }),

/***/ 312:
/***/ (function(module, exports) {

// footer.interface.ts
//# sourceMappingURL=footer.interface.js.map

/***/ }),

/***/ 313:
/***/ (function(module, exports) {

// header.interface.ts
//# sourceMappingURL=header.interface.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(50);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var routes = [
    { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
    {
        path: "enso",
        loadChildren: "../app/modules/enso/enso.module#EnsoModule"
    },
    {
        path: 'dashboard',
        loadChildren: "../app/modules/dashboard/dashboard.module#DashboardModule"
    },
    {
        path: 'solution',
        loadChildren: "../app/modules/solutions/solutions.module#SolutionsModule"
    },
    {
        path: "team",
        loadChildren: "../app/modules/team/team.module#TeamModule"
    },
    {
        path: "work",
        loadChildren: "../app/modules/work/work.module#WorkModule"
    },
    {
        path: "future",
        loadChildren: "../app/modules/future/future.module#FutureModule"
    },
    {
        path: "openpositions",
        loadChildren: "../app/modules/open-positions/open-positions.module#OpenPositionsModule"
    },
    {
        path: "getstarted",
        loadChildren: "../app/modules/get-started/get-started.module#GetStartedModule"
    }
];
// export const  AppRoutingModule = RouterModule.forRoot(routes,{useHash: true});
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forRoot(routes, { useHash: true })],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]],
        declarations: [],
        providers: []
    })
], AppRoutingModule);

//# sourceMappingURL=routes.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CONFIG; });
;
var CONFIG = {
    apiEndpoint: 'https://xpms.prismic.io/api/v2',
    linkResolver: function (doc) {
        if (doc.type === 'em')
            return "em/" + doc.uid;
        if (doc.type === 'enso')
            return "enso/" + doc.uid;
        if (doc.type === 'solution')
            return "solution/" + doc.uid;
        if (doc.type === 'work')
            return "work/" + doc.uid;
        if (doc.type === 'team')
            return "team/" + doc.uid;
        return '/';
    }
};
//# sourceMappingURL=prismic-configuration.js.map

/***/ }),

/***/ 471:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, "\nfooter{\n\tbackground: #4a4a4a;\n\tpadding-top: 40px;\n\tpadding-bottom: 40px;\n\tfont-family:'Raleway',sans-serif;\n}\n.widgetheading {\n\tfont-size: 12px;\n}\n\nfooter a {\n    color:#fff;\n    opacity: 0.5;\n\tfont-size: 12px;\n}\n\nfooter a:hover {\n\tcolor:#eee;\n}\n\nfooter h1, footer h2, footer h3, footer h4, footer h5, footer h6{\n    color:#fff;\n    text-transform: uppercase;\n}\n\nfooter address {\n\tline-height:1.6em;\n\tcolor: #797979;\n}\n\nfooter h5 a:hover, footer a:hover {\n\ttext-decoration:none;\n}\n.widget ul li {\n\tline-height: 1.5;\n}\nul.social-network {\n\tlist-style:none;\n\tmargin:0;\n}\n\nul.social-network li {\n\tmargin: 0 5px;\n    border: 1px solid #74767b;\n    border-radius: 50%;\n    padding: 5px 0 0;\n    width: 32px;\n    display: inline-block;\n\ttext-align: center;\n    height: 32px;\n    vertical-align: baseline;\n}\n\n#sub-footer{\n\ttext-shadow:none;\n\tcolor:#f5f5f5;\n\tpadding:0;\n\tpadding-top:30px;\n\tmargin:20px 0 0 0;\n\tbackground: #4a4a4a;\n}\n\n#sub-footer p{\n\tmargin:0;\n\tpadding:0;\n}\n\n#sub-footer span{\n\tcolor:#f5f5f5;\n}\n\n.copyright {\n\tfont-size:12px;\n\tpadding-top: 40px;\n}\n\n#sub-footer ul.social-network {\n\tmargin-top: 20px;\n}\n\n\n@media screen and (max-width: 767px) {\n\t.sectionTop ul {\n\t\tmin-height: 140px;\n\t}\n}\n.row{\n\tmargin-left:0px;\n\tmargin-right:0px;\n}\n.widgetheading {\n\tcursor: pointer;\n}\n.list-unstyled li a {\n\tcursor: pointer;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 472:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(51)();
// imports


// module
exports.push([module.i, ".headerLogo{\n    width: 200px;\n}\n.navbar {\n    margin-bottom: 0;\n    padding-bottom: 0;\n}\n.navbar .navbar-nav {\n    padding-top: 25px;\n}\n.navbar-brand {\n    padding-left: 50px;\n    padding-top: 25px;\n    padding-bottom: 0;\n    height: 75px;\n    cursor: pointer;\n}\n.navbar-fixed-top {\n    background: transparent;\n    background-color: #031218 !important;\n    transition-delay: 5s;\n    transition-timing-function: ease-in;\n    -webkit-transform: translateZ(0);\n            transform: translateZ(0);\n    -webkit-transition-timing-function: ease-in;\n    transition-duration: 20s;\n     box-shadow: 0 0 5px rgba(0,0,0,0.8);\n}\n.navbar-default{\n    background-color: transparent;\n    border:none;\n    box-shadow: none;\n}\n.navbar-right {\n    float: right!important;\n    margin-right: 15%;\n}\n\n\n.side-image {\n    position: absolute;\n    width: 25%;\n    right: 0;\n}\n.header-menu ul li{\n    margin-right: 20px;\n    z-index: 999;\n}\n.header-menu ul li a{\n    cursor: pointer;\n    font-size: 13px;\n    color:white;\n    font-weight: normal;\n    z-index: 999;\n    font-family:'Raleway', sans-serif;\n}\n.custom-tab ul {\n    background-color: #9b9b9b;\n    padding-right: 0;\n}\n.custom-tab ul li a{\n    font-size: 14px;\n    color: #fff;\n    font-weight: 600;\n    border-radius: 0;\n}\n.custom-tab li a:active {\n    background-color: rgb(84,84,84);\n}\n.header-menu ul li a:hover{\n    background-color: transparent;\n}\n.navbar-fixed-top .header-menu ul li.dropdown a.dropdown-toggle:hover{\n    background: transparent;\n}\n.custom-tab ul li a:hover{\n    background-color: rgb(75,75,75);\n}\n.nav .open>a, .nav .open>a:focus, .nav .open>a:hover{\n    background-color: rgba(255,255,255,0.9);\n}\n.icon-bar{\n    background-color: #000 !important;\n}\n.custom-tab{\n    margin-left: 0 !important;\n    margin-right: 0 !important;\n}\n.custom-tab ul li{\n    margin-right: 0 !important;\n    margin-top: 0;\n}\n.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover{\n    background-color: rgb(84,84,84);\n    color: #fff;\n}\n.nav-pills>li.active>a{\n    background-color: rgb(84,84,84);\n    color: #fff;\n}\n.jumbotron-cntr{\n    border-radius: 0 !important;\n    background-color: #b2b2b2;\n    margin-bottom: 0 !important\n}\n.nav-pills>li {\n    border-radius: 0 !important;\n    border-bottom: 1px solid #bdbdbd;\n    font-size: 14px;\n}\n\n.row-eq-height{\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.dropdown-menu>li {\n    display: table-cell;\n    height: 50px;\n    line-height: 50px;\n    vertical-align: middle;\n}\n\n.btn-learnmore {\n    text-transform: uppercase;\n    color: #4a4a4a;\n    font-weight: 600;\n    border-radius: 24px;\n    font-size: 12px;\n    background-color: rgb(245,226,87);\n    margin-top: 14px;\n    box-shadow: 0 1px 2px 0 #333;\n    border: 0px;\n}\n.header-tab-image {\n    padding:30px 15px 15px 0;\n}\n.header-tab-title {\n    color: #fff;\n    font-weight: 600;\n    margin-top: 30px;\n}\n.header-dropdown {\n    box-shadow: none;\n}\n.btn-started {\n    text-transform: uppercase;\n    color: #f7d761;\n    font-weight: normal;\n    border-radius: 24px;\n    font-size: 13px;\n    border: 1px solid #f7d761;\n    cursor: pointer;\n    background-color:transparent;\n    font-family:'Raleway',sans-serif;\n}\n.btn-started:active, .btn-started:focus {\n    outline: none;\n}\n@media screen and (max-width: 767px) {\n    .dropdown-menu>li {\n        display: block;\n        text-align: left;\n    }\n    .nav>li.dropdown.open {\n        position: static;\n        /* border-bottom: 6px solid rgb(252,218,83); */\n    }\n    .nav>li.dropdown.open .dropdown-menu {\n        display: table;\n        border-radius: 0px;\n        width: 40%;\n        text-align: left;\n        left: auto;\n        right: 10%;\n        border: 0;\n    }\n    .header-dropdown-mobile .nav-pills li a{\n        padding: 15px;\n    }\n    .navbar-nav {\n        margin: 0;\n    }\n    .header-menu {\n        margin: 0;\n        padding: 0;\n    }\n    .header-menu ul li {\n        margin-right: 0;\n    }\n    .navbar-header {\n        margin: 0;\n        position:absolute;\n        z-index:1000;\n    }\n    .navbar-toggle {\n        margin-right: 0;\n    }\n    .navbar-brand {\n        padding-left: 15px;\n    }\n    .search-icon-header {\n        position: relative;\n        float: right;\n        margin-top: 20px;\n        padding: 10px 0px;\n    }\n    .search-icon-header img {\n        height: 16px;\n        width: 16px;\n    }\n    .subHeading {\n        font-size: 14px;\n    }\n}\n@media (min-width: 768px) {\n    .navbar {\n        background: #fff;\n    }\n    .navbar-nav {\n        text-align: center;\n        float: right;\n    }\n    .navbar-nav > li {\n        float: none;\n        display: inline-block;\n    }\n    .navbar-nav > li.navbar-right {\n        float: right !important;\n    }\n    .nav>li.dropdown:hover {\n        position: static;\n        /* border-bottom: 6px solid rgb(252,218,83); */\n    }\n    .header-tab-content {\n        padding: 0 20px 30px 30px;\n    }\n    .custom-tab{\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: top;\n            -ms-flex-align: top;\n                align-items: top;\n        background: #b2b2b2;\n    }\n    .nav>li.dropdown.header-dropdown:hover .dropdown-menu {\n        display: table;\n        border-radius: 0px;\n        width: 40%;\n        text-align: left;\n        left: auto;\n        right: 10%;\n        box-shadow: none;\n        border: 0;\n    }\n    .nav>li.dropdown.header-dropdown:hover .dropdown-toggle:before {\n        position: absolute;\n        bottom: -4px;\n        left: 40%;\n        display: inline-block;\n        border-right: 7px solid transparent;\n        border-bottom: 7px solid #b2b2b2;\n        border-left: 7px solid transparent;\n        content: '';\n    }\n    \n    .nav>li.dropdown.header-dropdown:hover .dropdown-toggle:after {\n        position: absolute;\n        bottom: -3px;\n        left: 40%;\n        display: inline-block;\n        border-right: 6px solid transparent;\n        border-bottom: 6px solid #b2b2b2;\n        border-left: 6px solid transparent;\n        content: '';\n    }\n    \n    .navbar.navbar-fixed-top .navbar-nav{\n        padding-top: 15px;\n    }\n    .navbar-fixed-top .header-menu ul.navbar-nav li.dropdown .a-header{\n        padding-top: 0;\n    }\n    .navbar.navbar-fixed-top .navbar-brand{\n        padding-top: 10px;\n        height: 65px;\n    }\n    .navbar-header{\n      position:absolute;\n      z-index:10000;\n    }\n  }\n@media only screen and (min-width : 1024px) and (max-width : 1200px) {\n    .nav > li > a {\n        padding: 10px 5px;\n    }\n    .nav>li.dropdown.header-dropdown:hover .dropdown-toggle:before {\n        bottom: -9px;\n    }\n    .nav>li.dropdown.header-dropdown:hover .dropdown-toggle:after {\n        bottom: -9px;\n    }\n}\n\n@media screen and (min-width: 1200px){\n    .headerLogo{\n        width: 250px;\n    }\n}\n@media screen and (min-width: 992px){\n\n}\n@media only screen and (min-width : 768px) and (max-width : 991px) {\n    .navbar {\n        background: #fff;\n    }\n    .navbar-nav {\n        text-align: center;\n        float: right;\n    }\n    .headerLogo {\n        width: 150px;\n    }\n    .nav > li > a {\n        padding: 10px 5px;\n    }\n}\n@media screen and (max-width: 600px){\n    .nav>li.dropdown.open .dropdown-menu{\n        width: 100%;\n    }\n    .navbar-right{\n    margin-right:0%;\n    float:left !important;\n    }\n}\n  \n.row{\n\tmargin-left:0px;\n\tmargin-right:0px;\n}\n\n\n.navbar.navbar-fixed-top,\n.navbar-container{\n    transition: 0.8s;\n    -webkit-transition:  0.8s;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 484:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"headerInfo\">\n    <app-header [headerInfo]='headerInfo'></app-header>\n</div>\n<div class=\"main-router-outlet\">\n    <router-outlet></router-outlet>\n</div>\n<div *ngIf=\"footerData\">\n    <app-footer [footerInfo]='footerData'></app-footer>\n</div>\n  \n  "

/***/ }),

/***/ 485:
/***/ (function(module, exports) {

module.exports = "<footer>\n\t<div class=\"container\">\n\t\t<div class=\"row sectionTop\">\n\t\t\t<div class=\"col-sm-2 col-xs-6\">\n\t\t\t\t<div class=\"widget\">\n\t\t\t\t\t<h6 class=\"widgetheading\" (click)=\"navigateTo('enso','')\">{{footerData.footeritems1[0].maintitle}}</h6>\n\t\t\t\t\t<ul class=\"list-unstyled\">\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('enso','why')\">{{footerData.footeritems1[0].item1}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('enso','advantage')\">{{footerData.footeritems1[0].item2}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('enso','technology')\">{{footerData.footeritems1[0].item3}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('enso','features')\">{{footerData.footeritems1[0].item4}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('enso','datasheet')\">{{footerData.footeritems1[0].item5}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('enso','demo')\">{{footerData.footeritems1[0].item6}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-2 col-xs-6\">\n\t\t\t\t<div class=\"widget\">\n\t\t\t\t\t<h6 class=\"widgetheading\" (click)=\"navigateTo('solution','')\">{{footerData.footeritems2[0].maintitle}}</h6>\n\t\t\t\t\t<ul class=\"list-unstyled\">\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('solution','digitalization')\">{{footerData.footeritems2[0].item1}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('solution','decisionautomation')\">{{footerData.footeritems2[0].item2}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('solution','')\">{{footerData.footeritems2[0].item3}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('solution','')\">{{footerData.footeritems2[0].item4}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-2 col-xs-6\">\n\t\t\t\t<div class=\"widget\">\n\t\t\t\t\t<h6 class=\"widgetheading\" (click)=\"navigateTo('work','')\" >{{footerData.footeritems3[0].maintitle}}</h6>\n\t\t\t\t\t<ul class=\"list-unstyled\">\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('work','casestudies')\">{{footerData.footeritems3[0].item1}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('work','testimonials')\">{{footerData.footeritems3[0].item2}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('work','customers')\">{{footerData.footeritems3[0].item3}}</a>\n\t\t\t\t\t\t</li>\n\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-2 col-xs-6\">\n\t\t\t\t<div class=\"widget\">\n\t\t\t\t\t<h6 class=\"widgetheading\" (click)=\"navigateTo('team','')\">{{footerData.footeritems4[0].maintitle}}</h6>\n\t\t\t\t\t<ul class=\"list-unstyled\">\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('team','aboutus')\">{{footerData.footeritems4[0].item1}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('team','milestones')\">{{footerData.footeritems4[0].item2}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('team','culture')\">{{footerData.footeritems4[0].item3}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('team','leadership')\">{{footerData.footeritems4[0].item4}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('team','meet_team')\">{{footerData.footeritems4[0].item5}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('team','joinus')\">{{footerData.footeritems4[0].item6}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('team','contact')\">{{footerData.footeritems4[0].item7}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-2 col-xs-6\">\n\t\t\t\t<div class=\"widget\">\n\t\t\t\t\t<h6 class=\"widgetheading\" (click)=\"navigateTo('future','')\">{{footerData.footeritems5[0].maintitle}}</h6>\n\t\t\t\t\t<ul class=\"list-unstyled\">\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('future','blog')\">{{footerData.footeritems5[0].item1}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a routerLink=\"\" (click)=\"navigateTo('future','lab')\">{{footerData.footeritems5[0].item2}}</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-2 col-xs-4\">\n\t\t\t\t<div class=\"widget\">\n\t\t\t\t\t<h6 class=\"widgetheading\" (click)=\"navigateTo('getstarted','')\">{{footerData.footeritems6[0].maintitle}}</h6>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div id=\"sub-footer\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-lg-12 text-center\">\n\t\t\t\t\t<div class=\"\">{{footerData.followtext[0].text}}</div>\n\t\t\t\t\t<ul class=\"social-network\">\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a href=\"#\" data-placement=\"top\" title=\"\" data-original-title=\"Facebook\">\n\t\t\t\t\t\t\t\t<i class=\"fab fa-facebook-f\"></i>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a href=\"#\" data-placement=\"top\" title=\"\" data-original-title=\"Twitter\">\n\t\t\t\t\t\t\t\t<i class=\"fab fa-twitter\"></i>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a href=\"#\" data-placement=\"top\" title=\"\" data-original-title=\"Linkedin\">\n\t\t\t\t\t\t\t\t<i class=\"fab fa-linkedin\"></i>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a href=\"#\" data-placement=\"top\" title=\"\" data-original-title=\"Pinterest\">\n\t\t\t\t\t\t\t\t<i class=\"fab fa-pinterest\"></i>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t<a href=\"#\" data-placement=\"top\" title=\"\" data-original-title=\"Google plus\">\n\t\t\t\t\t\t\t\t<i class=\"fab fa-google-plus\"></i>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-12\">\n\t\t\t\t\t<div class=\"copyright text-center\">\n\t\t\t\t\t\t{{footerData.copyright[0].text}}\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</footer>\n"

/***/ }),

/***/ 486:
/***/ (function(module, exports) {

module.exports = "<div class=\"navbar navbar-container\" [ngClass] = \"navIsFixed || (routerObj.url != '/dashboard') ? 'navbar-fixed-top' : ''\" >\n    <div class=\"container-fluid\" style=\"padding: 0\">\n        <div class=\"navbar-header\">\n            <a routerLink=\"\" (click)=\"navigateTo('dashboard','')\" data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" class=\"navbar-brand\">\n                <img alt=\"{{headerData.subnavigation[0].logo.alt}}\" src=\"{{headerData.subnavigation[0].logo.url}}\" class=\"headerLogo img-responsive\" />\n            </a>\n            <button class=\"navbar-toggle\" style=\"margin-top: 22px;\" data-toggle=\"collapse\" data-target=\".btnCollapse\">\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n            </button>\n            <!-- <a class=\"hidden-lg hidden-md hidden-sm search-icon-header\" ><img src=\"../../../../assets/images/search.png\" style=\"vertical-align: middle;cursor: pointer;\" /></a> -->\n        </div>\n        <div class=\"collapse navbar-collapse btnCollapse header-menu\">\n            <ul class=\"nav navbar-nav hidden-xs\">\n                <li class=\"dropdown\" (mouseover)=\"addHoverToNav()\">\n                    <a class=\"dropdown-toggle a-header\" (click)=\"navigateTo('enso','')\" >{{headerData.subnavigation[0].navtab1}}</a>\n                    <ul class=\"dropdown-menu\" role=\"menu\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked col-md-3 col-lg-3 col-sm-3 col-xs-3\">\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('enso','why')\">{{headerData.navtab1data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('enso','advantage')\">{{headerData.navtab1data[0].item2}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('enso','technology')\">{{headerData.navtab1data[0].item3}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('enso','features')\">{{headerData.navtab1data[0].item4}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('enso','datasheet')\" >{{headerData.navtab1data[0].item5}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('enso','demo')\">{{headerData.navtab1data[0].item6}}</a>\n                                </li>\n                            </ul>\n                            <div class=\"tab-content col-md-9 col-lg-9 col-sm-9 col-xs-9\">\n                                <div class=\"tab-pane jumbotron-cntr active\" id=\"why\">\n                                    <div class=\"row\">\n                                        <div class=\"header-tab-content\" [ngClass]=\"headerData.navtab1data[0].image1.url ? 'col-md-9 col-sm-9' : 'col-md-12 col-sm-12'\">\n                                            <h3 class=\"header-tab-title\">{{headerData.navtab1data[0].data1[0].text}}</h3>\n                                            <small style=\"color: #fff;\">{{headerData.navtab1data[0].subdata1[0].text}}</small>\n                                            <br>\n                                            <button type=\"button\" class=\"btn btn-default navbar-btn btn-learnmore\">\n                                                {{headerData.navtab1data[0].button1[0].text}}\n                                            </button>\n                                        </div>\n                                        <div class=\"col-md-3 col-sm-3 header-tab-image\" *ngIf=\"headerData.navtab1data[0].image1.url\">\n                                            <img alt={{headerData.navtab1data[0].image1.alt}} src={{headerData.navtab1data[0].image1.url}} class=\"img-responsive\"/>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </ul>\n                </li>\n                <li class=\"dropdown\" (mouseover)=\"addHoverToNav()\">\n                    <a class=\"dropdown-toggle a-header\" (click)=\"navigateTo('solution','')\" id=\"mysol\">{{headerData.subnavigation[0].navtab2}}</a>\n                    <ul class=\"dropdown-menu\" role=\"menu\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked col-md-3 col-lg-3 col-sm-3 col-xs-3\" role=\"tablist\" data-tabs=\"tabs\">\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('solution','digitalization')\">{{headerData.navtab2data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('solution','decisionautomation')\">{{headerData.navtab2data[0].item2}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('solution','')\">{{headerData.navtab2data[0].item3}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('solution','')\">{{headerData.navtab2data[0].item4}}</a>\n                                </li>\n                            </ul>\n                            <div class=\"tab-content col-md-9 col-lg-9 col-sm-9 col-xs-9\">\n                                <div class=\"tab-pane jumbotron-cntr active\" id=\"digitalization\">\n                                    <div class=\"row\">\n                                        <div class=\"header-tab-content\" [ngClass]=\"headerData.navtab2data[0].image1.url ? 'col-md-9 col-sm-9' : 'col-md-12 col-sm-12'\">\n                                            <h3 class=\"header-tab-title\">{{headerData.navtab2data[0].data1[0].text}}</h3>\n                                            <small style=\"color: #fff;\">{{headerData.navtab2data[0].subdata1[0].text}}</small>\n                                            <br>\n                                            <button type=\"button\" class=\"btn btn-default navbar-btn btn-learnmore\">\n                                                {{headerData.navtab2data[0].button1[0].text}}\n                                            </button>\n                                        </div>\n                                        <div class=\"col-md-3 col-sm-3 header-tab-image\" *ngIf=\"headerData.navtab2data[0].image1.url\">\n                                            <img alt={{headerData.navtab2data[0].image1.alt}} src={{headerData.navtab2data[0].image1.url}} class=\"img-responsive\"/>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </ul>\n                </li>\n                <li class=\"dropdown\" (mouseover)=\"addHoverToNav()\">\n                    <a class=\"dropdown-toggle a-header\" (click)=\"navigateTo('work','')\" id=\"mywork\">{{headerData.subnavigation[0].navtab3}}</a>\n                    <ul class=\"dropdown-menu\" role=\"menu\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked col-md-3 col-lg-3 col-sm-3 col-xs-3\" role=\"tablist\" data-tabs=\"tabs\">\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('work','casestudies')\">{{headerData.navtab3data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('work','testimonials')\">{{headerData.navtab3data[0].item2}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('work','customers')\">{{headerData.navtab3data[0].item3}}</a>\n                                </li>\n                            </ul>\n                            <div class=\"tab-content col-md-9 col-lg-9 col-sm-9 col-xs-9\">\n                                <div class=\"tab-pane jumbotron-cntr active\" id=\"casestudies\">\n                                    <div class=\"row\">\n                                        <div class=\"header-tab-content\" [ngClass]=\"headerData.navtab3data[0].image1.url ? 'col-md-9 col-sm-9' : 'col-md-12 col-sm-12'\">\n                                            <h3 class=\"header-tab-title\">{{headerData.navtab3data[0].data1[0].text}}</h3>\n                                            <small style=\"color: #fff;\">{{headerData.navtab3data[0].subdata1[0].text}}</small>\n                                            <br>\n                                            <button type=\"button\" class=\"btn btn-default navbar-btn btn-learnmore\">\n                                                {{headerData.navtab3data[0].button1[0].text}}\n                                            </button>\n                                        </div>\n                                        <div class=\"col-md-3 col-sm-3 header-tab-image\" *ngIf=\"headerData.navtab3data[0].image1.url\">\n                                            <img alt={{headerData.navtab3data[0].image1.alt}} src={{headerData.navtab3data[0].image1.url}} class=\"img-responsive\"/>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </ul>\n                </li>\n                <li class=\"dropdown\" (mouseover)=\"addHoverToNav()\">\n                    <a class=\"dropdown-toggle a-header\" (click)=\"navigateTo('team','')\" id=\"myteam\">{{headerData.subnavigation[0].navtab4}}</a>\n                    <ul class=\"dropdown-menu\" role=\"menu\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked col-md-3 col-lg-3 col-sm-3 col-xs-3\" role=\"tablist\" data-tabs=\"tabs\">\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('team','aboutus')\">{{headerData.navtab4data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('team','milestones')\">{{headerData.navtab4data[0].item2}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('team','culture')\">{{headerData.navtab4data[0].item3}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('team','leadership')\">{{headerData.navtab4data[0].item4}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('team','meet_team')\">{{headerData.navtab4data[0].item5}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('team','joinus')\">{{headerData.navtab4data[0].item6}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('team','contact')\">{{headerData.navtab4data[0].item7}}</a>\n                                </li>\n                            </ul>\n                            <div class=\"tab-content col-md-9 col-lg-9 col-sm-9 col-xs-9\">\n                                <div class=\"tab-pane jumbotron-cntr active\" id=\"why\">\n                                    <div class=\"row\">\n                                        <div class=\"header-tab-content\" [ngClass]=\"headerData.navtab4data[0].image1.url ? 'col-md-9 col-sm-9' : 'col-md-12 col-sm-12'\">\n                                            <h3 class=\"header-tab-title\">{{headerData.navtab4data[0].data1[0].text}}</h3>\n                                            <small style=\"color: #fff;\">{{headerData.navtab4data[0].subdata1[0].text}}</small>\n                                            <br>\n                                            <button type=\"button\" class=\"btn btn-default navbar-btn btn-learnmore\">\n                                                {{headerData.navtab4data[0].button1[0].text}}\n                                            </button>\n                                        </div>\n                                        <div class=\"col-md-3 col-sm-3 header-tab-image\" *ngIf=\"headerData.navtab4data[0].image1.url\">\n                                            <img alt={{headerData.navtab4data[0].image1.alt}} src={{headerData.navtab4data[0].image1.url}} class=\"img-responsive\"/>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </ul>\n                </li>\n                <li class=\"dropdown\" (mouseover)=\"addHoverToNav()\">\n                    <a class=\"dropdown-toggle a-header\" (click)=\"navigateTo('future','')\" id=\"myfeature\">{{headerData.subnavigation[0].navtab5}}</a>\n                    <ul class=\"dropdown-menu\" role=\"menu\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked col-md-3 col-lg-3 col-sm-3 col-xs-3\" role=\"tablist\" data-tabs=\"tabs\">\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('future','blog')\">{{headerData.navtab5data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a routerLink=\"\" (click)=\"navigateTo('future','lab')\">{{headerData.navtab5data[0].item2}}</a>\n                                </li>\n                            </ul>\n                            <div class=\"tab-content col-md-9 col-lg-9 col-sm-9 col-xs-9\">\n                                <div class=\"tab-pane jumbotron-cntr active\" id=\"blogSection\">\n                                    <div class=\"row\">\n                                        <div class=\"header-tab-content\" [ngClass]=\"headerData.navtab5data[0].image1.url ? 'col-md-9 col-sm-9' : 'col-md-12 col-sm-12'\">\n                                            <h3 class=\"header-tab-title\">{{headerData.navtab5data[0].data1[0].text}}</h3>\n                                            <small style=\"color: #fff;\">{{headerData.navtab5data[0].subdata1[0].text}}</small>\n                                            <br>\n                                            <button type=\"button\" class=\"btn btn-default navbar-btn btn-learnmore\">\n                                                {{headerData.navtab5data[0].button1[0].text}}\n                                            </button>\n                                        </div>\n                                        <div class=\"col-md-3 col-sm-3 header-tab-image\" *ngIf=\"headerData.navtab5data[0].image1.url\">\n                                            <img alt={{headerData.navtab5data[0].image1.alt}} src={{headerData.navtab5data[0].image1.url}} class=\"img-responsive\"/>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </ul>\n                </li>\n                <li>\n                    <button type=\"button\" (click)=\"navigateTo('getstarted','')\" class=\"btn btn-default navbar-btn btn-started\" >{{headerData.subnavigation[0].navtab6}}</button>\n                </li>\n            </ul>\n            <ul class=\"nav navbar-nav hidden-md hidden-lg hidden-sm\">\n                <li class=\"dropdown\">\n                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">{{headerData.subnavigation[0].navtab1}}</a>\n                    <ul class=\"dropdown-menu header-dropdown-mobile\" role=\"menu\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked\">\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('enso','why')\">{{headerData.navtab1data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('enso','advantage')\">{{headerData.navtab1data[0].item2}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('enso','technology')\">{{headerData.navtab1data[0].item3}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('enso','features')\">{{headerData.navtab1data[0].item4}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('enso','datasheet')\" >{{headerData.navtab1data[0].item5}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('enso','demo')\">{{headerData.navtab1data[0].item6}}</a>\n                                </li>\n                            </ul>\n                        </div>\n                    </ul>\n                </li>\n                <li class=\"dropdown\">\n                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" id=\"mysol\">{{headerData.subnavigation[0].navtab2}}</a>\n                    <ul class=\"dropdown-menu header-dropdown-mobile\" role=\"menu\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked\" role=\"tablist\" data-tabs=\"tabs\">\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('solution','digitalization')\">{{headerData.navtab2data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('solution','decisionautomation')\">{{headerData.navtab2data[0].item2}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('solution','')\">{{headerData.navtab2data[0].item3}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('solution','')\">{{headerData.navtab2data[0].item4}}</a>\n                                </li>\n                            </ul>\n                        </div>\n                    </ul>\n                </li>\n                <li class=\"dropdown\">\n                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" id=\"mywork\">{{headerData.subnavigation[0].navtab3}}</a>\n                    <ul class=\"dropdown-menu header-dropdown-mobile\" role=\"menu\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked\" role=\"tablist\" data-tabs=\"tabs\">\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('work','casestudies')\">{{headerData.navtab3data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('work','testimonials')\">{{headerData.navtab3data[0].item2}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('work','customers')\">{{headerData.navtab3data[0].item3}}</a>\n                                </li>\n                            </ul>\n                        </div>\n                    </ul>\n                </li>\n                <li class=\"dropdown\">\n                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" id=\"myteam\">{{headerData.subnavigation[0].navtab4}}</a>\n                    <ul class=\"dropdown-menu header-dropdown-mobile\" role=\"menu\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked\" role=\"tablist\" data-tabs=\"tabs\">\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('team','aboutus')\">{{headerData.navtab4data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('team','milestones')\">{{headerData.navtab4data[0].item2}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('team','culture')\">{{headerData.navtab4data[0].item3}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('team','leadership')\">{{headerData.navtab4data[0].item4}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('team','meet_team')\">{{headerData.navtab4data[0].item5}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('team','joinus')\">{{headerData.navtab4data[0].item6}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('team','contact')\">{{headerData.navtab4data[0].item7}}</a>\n                                </li>\n                            </ul>\n                        </div>\n                    </ul>\n                </li>\n                <li class=\"dropdown\">\n                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" id=\"myfeature\">{{headerData.subnavigation[0].navtab5}}</a>\n                    <ul class=\"dropdown-menu header-dropdown-mobile\" role=\"menu\" (mouseleave)=\"showBlog=true;deactivateOtherTabs();\" style=\"padding-top: 0;padding-bottom: 0;\">\n                        <div class=\"row custom-tab\">\n                            <ul class=\"nav nav-pills nav-stacked\" role=\"tablist\" data-tabs=\"tabs\">\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('future','blog')\">{{headerData.navtab5data[0].item1}}</a>\n                                </li>\n                                <li>\n                                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" routerLink=\"\" (click)=\"navigateTo('future','lab')\">{{headerData.navtab5data[0].item2}}</a>\n                                </li>\n                            </ul>\n                        </div>\n                    </ul>\n                </li>\n                <li>\n                    <a data-toggle=\"collapse\" data-target=\".navbar-collapse.in\" (click)=\"navigateTo('getstarted','')\" class=\"dropdown-toggle\">{{headerData.subnavigation[0].navtab6}}</a>\n                </li>\n            </ul>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderService = (function () {
    function HeaderService() {
        this.sectionInfo = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"]({ section: '' });
        this.currentSection = this.sectionInfo.asObservable();
    }
    HeaderService.prototype.setSection = function (sectionObj) {
        this.sectionInfo.next(sectionObj);
    };
    return HeaderService;
}());
HeaderService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], HeaderService);

//# sourceMappingURL=header.service.js.map

/***/ }),

/***/ 772:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(296);


/***/ })

},[772]);
//# sourceMappingURL=main.bundle.js.map